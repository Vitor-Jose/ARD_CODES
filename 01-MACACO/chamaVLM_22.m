function [coeff] = chamaVLM_22(geo,flc,plotar,surfaceID)

%--------------------------------------------%
%     _    _ _____ ____ _____ ____ _____     %
%    | \  / |  _  |  __|  _  |  __|  _  |    %
%    |  \/  | [_] | (__| [_] | (__| (_) |    %
%    |_|  |_|_/ \_|____|_/ \_|____|_____|    %
% Multi-Analysis  Code for Airplane COncepts %
%                                            %
%--------------------------------------------%
%    Funcao-ponte entre MACACO e POMAER      %
%--------------------------------------------%

%Input Geometrico, fligth conditions e desempenho.
%Flags
%   -wing                  - Roda asa
%   -EH                    - Roda EH
%   -EV                    - Roda EV
%   -plotpolar             - Plota a polar da curva
%   -beta                  - Roda o VLM com beta
%   -PlotPolar             - Plotar a PlotPolar
% (nao recomenda rodar EV junto com wing ou EH).

PlotPolar = 0;
clear plane.LiftingSurfaces;
clear plane ;
clear fltcond; 
surfacenum = length(surfaceID);

%% Inicia com Plane
plane = Airplane;
if ~iscell(geo.s.perfil)
        for i = 1:surfacenum
            for j=1:geo.s.chordnum(i)    
                perfil = seleciona_perfil(geo.s.perfil(surfaceID(i),j));
                
                perf(i,j) = ImportObj(strcat('CURVAS/',perfil));
            end
        end
elseif iscell(geo.s.perfil)
            for i = 1:surfacenum
                for j=1:geo.s.chordnum(surfaceID(i))
                    perf(i,j) = ImportObj(strcat('CURVAS/',geo.s.perfil{surfaceID(i),j}));
                end
            end
end      
        for i = 1:surfacenum
            if i==1
                plane.LiftingSurfaces(i).ID = 1;
                plane.LiftingSurfaces(i).IsMain = 1;
                plane.LiftingSurfaces(i).Geo.pos.x      = -geo.s.pos(surfaceID(i),1);             %posicao das outras superficies em funcaoo do C/4 da asa principal
                plane.LiftingSurfaces(i).Geo.pos.z      = -geo.s.pos(surfaceID(i),3);             %posicao das outras superficies em funcao do C/4 da asa principal

            else
                plane.LiftingSurfaces(i) = LiftingSurface(strcat('wing',i));
                plane.LiftingSurfaces(i).Geo.pos.x      = -geo.s.pos(surfaceID(i),1);             %posicao das outras superficies em funcaoo do C/4 da asa principal
                plane.LiftingSurfaces(i).Geo.pos.z      = -geo.s.pos(surfaceID(i),3);             %posicao das outras superficies em funcao do C/4 da asa principal
            end
            plane.LiftingSurfaces(i).Geo.C              = geo.s.c(surfaceID(i),1:geo.s.chordnum(surfaceID(i)));               %corda secoes POMAER [m]
            plane.LiftingSurfaces(i).Geo.Yb             = geo.s.b(surfaceID(i),1:geo.s.chordnum(surfaceID(i)));               %Span-Wise secoes POMAER [m]
            plane.LiftingSurfaces(i).Geo.Dihedral       = geo.s.d(surfaceID(i),1:geo.s.chordnum(surfaceID(i))-1);               %Diedro das secoes POMAER [deg]
            plane.LiftingSurfaces(i).Geo.Sweep          = geo.s.e(surfaceID(i),1:geo.s.chordnum(surfaceID(i))-1);               %Enflechamento POMAER [deg]
            plane.LiftingSurfaces(i).Geo.Incidence      = geo.s.ai(surfaceID(i));                %Incidencia POMAER [deg]
            plane.LiftingSurfaces(i).Geo.TwistY         = geo.s.twist(surfaceID(i),1:geo.s.chordnum(surfaceID(i)));
            plane.LiftingSurfaces(i).Aero.Airfoils.Data = perf(i,:);                  %Perfis POMAER
%           plane.LiftingSurfaces(i).Aero.Airfoils.InterpType = [0 0 0 0];            %Interpolacao dos Perfis 

        end
        
        % =============== Mesh ===============
        for i=1:surfacenum
            aux_TypeY=ones(1,geo.s.chordnum(surfaceID(i))-1);
            aux_lengthaux=length(aux_TypeY);
            aux_TypeY(aux_lengthaux) = 2;
            plane.LiftingSurfaces(i).Aero.Mesh.TypeY    = aux_TypeY;                %Discretizacao da malha das secoes (1: Contant, 2: cossine, 3: right, 4: left)
            for j=1:(geo.s.chordnum(surfaceID(i))-1)
%                 if plane.LiftingSurfaces(i).Aero.Mesh.TypeY(j)==3    % Para onde houver discretizacao "a direita"
                if j == geo.s.chordnum(surfaceID(i))-1 %PONTA
%                     panel_dens = 12; %Densidade de Paineis
                    plane.LiftingSurfaces(i).Aero.Mesh.Ny(j)	= geo.o.panel;    %Num. de paineis
                elseif j == geo.s.chordnum(surfaceID(i))-2 %PENULTIMA SECAO
%                     panel_dens = 8; 
                    plane.LiftingSurfaces(i).Aero.Mesh.Ny(j)	= geo.o.panel;    %Num. de paineis
                else %RESTANTE DAS SECOES
%                     panel_dens = 9; 
                    plane.LiftingSurfaces(i).Aero.Mesh.Ny(j)	= geo.o.panel;    %Num. de paineis
               
                end
            end
        end

%% =============== Fligth Conditions ===============
plane.UpdateGeo;
plane.UpdateAeroMesh;

if plotar == 1
    plane.PlotMesh('-clf')
    pause (0.001)
elseif plotar == 2
    clf;
    plane.PlotGeo('-airfoils')
    view([-40,30])
    pause (0.001)
end

fltcond                                     = FlightConditions;         %Inicia flight conditions
fltcond.H                                   = flc.h;                    %Altitude de referencia [m]
fltcond.rho                                 = flc.rho;                  %rho ar POMAER

fltcond.Voo                                 = flc.Voo;                %velocidade do escoamento [m/s]
%     fltcond.UpdateRe;

    
%% ============================== VLM ==============================
for i=1:length(flc.aoa)
    fltcond.alpha                     = flc.aoa(i);          %vetor alpha [deg]
    fltcond.p                         = 0;
    fltcond.q                         = 0;
    fltcond.r                         = 0;
    fltcond.UpdateRe;
    % INTERPOLACAO    
    plane.Aero.Settings.NLVLM.Cl_from_adjusted_2DCurvesEq = 1;

     [results]                     = NL_VLM(plane,fltcond,'-append','-itermax',geo.o.itermax,'-dispstat');
end
%% ALOCACAO
for k=1:length(plane.LiftingSurfaces)
    for j=1:length(flc.aoa)
        coeff(surfaceID(k)).CL(j) = plane.LiftingSurfaces(1,k).Aero.Coeffs(1,j).CL;
        coeff(surfaceID(k)).CD(j) = plane.LiftingSurfaces(1,k).Aero.Coeffs(1,j).CD;
        coeff(surfaceID(k)).CDi(j) = plane.LiftingSurfaces(1,k).Aero.Coeffs(1,j).CDi;
        coeff(surfaceID(k)).Cm25(j) = plane.LiftingSurfaces(1,k).Aero.Coeffs(1,j).Cm25;
        coeff(surfaceID(k)).aoa(j) = flc.aoa(j);
    end
    coeff(surfaceID(k)).Bref = plane.Aero.Coeffs(1,1).Bref;
    coeff(surfaceID(k)).Cref = plane.Geo.MAC;
    coeff(surfaceID(k)).Sref = plane.Aero.Coeffs(1,1).Sref;
    coeff(surfaceID(k)).V = flc.Voo;
end

% if PlotPolar
% plane.PlotPolar;
% keyboard;
% end
    
end
