function coeffs = coeffs_22(geo,flc,plotVLM,surfaces,surfaceref)
% Funcao para simular condicoes e superficies

% === INPUT ===
% geo - Geometria
% flc - Condicoes de voo
% plotVLM - Opcoes de plot (0 - Desativado, 1 - Malha, 2 - Geometria)
% surfaces   - Conjunto de superficies
% surfaceref - Superficie de referencia

% === OUTPUT ===
% coeffs - Coeficientes 

callnum         = length(surfaces(:,1));    % Numero de simulacoes (conforme numero de linhas das superficies)
geo.s.chordnum  = geo.section_number+1;     % Numero de cordas

for aux_c = 1:callnum                       % Dentro de cada simulacao:
    surfaceID = nonzeros(surfaces(aux_c,:));% Superficies pedidas
    surfacenum = length(surfaceID);         % Numero de superficies
    flc.aoa = flc.case(aux_c).aoa;          % Condicao de voo
    C = chamaVLM_22(geo,flc,plotVLM,surfaceID);% Coeficientes agrupados
    
    for k=1:surfacenum
        coeffs(surfaceID(k)) = C(surfaceID(k)); % Coeficientes alocados conforme superficie especifica;
    end
end

if surfaceref ~= 0
    Sref = coeffs(surfaceref).Sref;
    Bref = coeffs(surfaceref).Bref;
    Cref = coeffs(surfaceref).Cref;
    for aux_c=1:length(coeffs)
        coeffs(aux_c).CL = coeffs(aux_c).CL*coeffs(aux_c).Sref/Sref;
        coeffs(aux_c).CD = coeffs(aux_c).CD*coeffs(aux_c).Sref/Sref;
        coeffs(aux_c).CDi = coeffs(aux_c).CDi*coeffs(aux_c).Sref/Sref;
        coeffs(aux_c).Cm25 = coeffs(aux_c).Cm25*(coeffs(aux_c).Sref*coeffs(aux_c).Cref)/(Sref*Cref);
        coeffs(aux_c).Sref = Sref;
        coeffs(aux_c).Cref = Cref;
        coeffs(aux_c).Bref = Bref;
    end
end 
end