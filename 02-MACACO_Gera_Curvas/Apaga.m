% Apaga ponto especifico 

n_pol = 14;
n_aoa = 18;

cd Perfis_salvos
load('E95origin.mat')

E95origin.pol(n_pol).alpha(n_aoa) = [];
E95origin.pol(n_pol).cl(n_aoa) = [];
E95origin.pol(n_pol).cdp(n_aoa) = [];
E95origin.pol(n_pol).cd(n_aoa) = [];
E95origin.pol(n_pol).cm(n_aoa) = [];
E95origin.pol(n_pol).Top_Xtr(n_aoa) = [];
E95origin.pol(n_pol).Bot_Xtr(n_aoa) = [];
E95origin.pol(n_pol).Cp(:,n_aoa) = [];
E95origin.pol(n_pol).x2(:,n_aoa) = [];
E95origin.pol(n_pol).y2(:,n_aoa) = [];
E95origin.pol(n_pol).x(:,n_aoa) = [];
E95origin.pol(n_pol).y(:,n_aoa) = [];
E95origin.pol(n_pol).Cf(:,n_aoa) = [];
E95origin.pol(n_pol).H(:,n_aoa) = [];

global limites_linear
limites_linear = [-1 6];
E95origin.UpdateCoeff;

save('E95origin.mat','E95origin')
cd ..