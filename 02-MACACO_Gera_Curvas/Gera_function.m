%% Fun��o para gerar as curvas e criar o .mat (atualizado para Cp e Cf)
% Vitor Jos� - 30/09/2021

function Gera_function(abrir_input,salvar_input,Re_input,aoa_input)
%% INPUTS
qual       = 1;              % Define o diret�rio do xfoil
abrir_dat  = abrir_input;    % Nome do arquivo a abrir
salvar_mat = salvar_input;   % Nome do arquivo a salvar
Re         = Re_input;       % Define o intervalo de Reynolds                       
aoa        = aoa_input;      % Define o intervalo de �ngulo de ataque

paineis    = 150;            % N�mero de pain�is
iteracoes  = 100;            % N�mero de itera��es m�ximas

Re_ncrit   = [1e5 2e5 3e5];  % Reynolds que separam os ncrit
ncrit      = [5.5 7 8.5 9];  % N�mero cr�tico para cada intervalo

% parametrizar = 0;
% if parametrizar 
%     esp_vertical    = par(1);
%     esp_horizontal  = par(2);
%     camb_vertical   = par(3);
%     camb_horizontal = par(4);
% end
%% OPCOES
global limites_linear
limites_linear = [0 6];

perfil = Airfoil;
perfil.name = salvar_mat;
perfil.xy = load(['Perfis_Interpolados\' abrir_dat]);

plotar_xfoil= 0;    % Ativa o plot ao vivo do xfoil

from_middle = 2;    % Op��o de come�ar o �ngulo de ataque pelo meio do vetor
                    % 0 - Desativado
                    % 1 - Ativado crescente do meio
                    % 2 - Ativado decrescente do meio
middle      = -3;    % Qual o primeiro valor, caso from_middle = 1 ou 2

if from_middle == 1
    initial_aoa_pos = find(aoa == middle);
    ordem_aoa = aoa(initial_aoa_pos:end);
    ordem_aoa(end+1:length(aoa)) = aoa(initial_aoa_pos-1:-1:1);
elseif from_middle == 2
    initial_aoa_pos = find(aoa == middle);
    ordem_aoa = aoa(initial_aoa_pos:-1:1);
    ordem_aoa(end+1:length(aoa)) = aoa(initial_aoa_pos+1:end);
else
    ordem_aoa = aoa;
end

n = 1;
penal = 0;
naofoi = 0;
caso_nao = [2e3 4e3 6e3];

while n <= length(Re)
    for k = 1:length(Re_ncrit)
        if Re(n) >= Re_ncrit(k)
            ncrit_escrito = ncrit(k+1);
        else
            ncrit_escrito = ncrit(k);
            break
        end
    end
    
    nome_ordem = ['X' num2str(qual) '/ordens' num2str(qual) '.txt'];
    comando = fopen(nome_ordem,'w');
    fprintf(comando,'\n');
    fprintf(comando,'load ../Perfis_Interpolados/%s\n',abrir_dat);
    fprintf(comando,'%s\n',erase(abrir_dat,'.dat'));
    if ~plotar_xfoil
        fprintf(comando,'plop\n');
        fprintf(comando,'g\n');
        fprintf(comando,'\n');
    end
%     if parametrizar
%     fprintf(comando,'gdes\n');
%     fprintf(comando,'tset\n');
%     fprintf(comando,'%f\n',esp_vertical);
%     fprintf(comando,'%f\n',camb_vertical);
%     fprintf(comando,'high\n');
%     fprintf(comando,'%f\n',esp_horizontal);
%     fprintf(comando,'%f\n',camb_horizontal);
%     fprintf(comando,'x\n');
%     fprintf(comando,'\n');
%     end
    fprintf(comando,'ppar\n');
    fprintf(comando,'n %d\n',paineis);
    fprintf(comando,'\n');
    fprintf(comando,'\n');

    fprintf(comando,'oper\n');
    fprintf(comando,'v\n');
    fprintf(comando,'%d\n',Re(n));
    fprintf(comando,'iter %d\n',iteracoes);
    fprintf(comando,'vpar\n');
    fprintf(comando,'n %f\n',ncrit_escrito);
    fprintf(comando,'\n');
    fprintf(comando,'p\n');
    fprintf(comando,'provisorio%d.nfo\n',qual);
    fprintf(comando,'\n');
    for i = 1:length(ordem_aoa)
        fprintf(comando,'a %g\n',ordem_aoa(i));
        fprintf(comando,'dump\n');
        fprintf(comando,['cf_' num2str(ordem_aoa(i)) '.nfo\n']);
        fprintf(comando,'cpwr\n');
        fprintf(comando,['cp_' num2str(ordem_aoa(i)) '.nfo\n']);
    end
    fprintf(comando,'p\n');
    fprintf(comando,'\n');
    fprintf(comando,'quit\n');
    
    switch qual
        case 1
            cd X1
            delete provisorio1.nfo
            system('start /min mataxfoil1.bat'); 
            !runner1.bat >NUL
            cd ..
        case 2
            cd X2
            delete provisorio2.nfo
            system('start /min mataxfoil2.bat'); 
            !runner2.bat >NUL
            cd ..   
    end
    
    perfil.pol(n-naofoi) = Airfoil_Polar;
    
    nome_provisorio = ['X' num2str(qual) '/provisorio' num2str(qual) '.nfo'];
    identidade = fopen(nome_provisorio);

    for i=1:7
        [~] = fgetl(identidade);
    end
    
    line = fgetl(identidade);
    info = strsplit(line);
    perfil.pol(n-naofoi).conf.xtrf_top = info(4);
    perfil.pol(n-naofoi).conf.xtrf_bot = info(6);

    line = fgetl(identidade);
    info = strsplit(line);
    perfil.pol(n-naofoi).Re = str2double([info{7:9}]);
    perfil.pol(n-naofoi).conf.mach = info(4);
    perfil.pol(n-naofoi).conf.ncrit = info(12);

    perfil.pol(n-naofoi).conf.xhinge = [];
    perfil.pol(n-naofoi).conf.yhinge = [];
    perfil.pol(n-naofoi).conf.defex_hinge = [];

    for i=10:12
        [~] = fgetl(identidade);
    end
    
    j=1;
    line    = fgetl(identidade);
    
    while line~=-1
        [data,~,~,~] = sscanf(line, '%f');

        reorg.alpha(j)	= data(1);
        reorg.cl(j)		= data(2);
        reorg.cd(j)		= data(3);
        reorg.cdp(j)	= data(4);
        reorg.cm(j)		= data(5);
        reorg.Top_Xtr(j)= data(6);
        reorg.Bot_Xtr(j)= data(7);

        j=j+1;
        line    = fgetl(identidade);
    end
    
    if from_middle == 1
        where_slip = find(reorg.alpha < reorg.alpha(1),1);

        perfil.pol(n-naofoi).alpha	= [reorg.alpha(end:-1:where_slip) reorg.alpha(1:where_slip-1)];
        perfil.pol(n-naofoi).cl		= [reorg.cl(end:-1:where_slip) reorg.cl(1:where_slip-1)];
        perfil.pol(n-naofoi).cd		= [reorg.cd(end:-1:where_slip) reorg.cd(1:where_slip-1)];
        perfil.pol(n-naofoi).cdp	= [reorg.cdp(end:-1:where_slip) reorg.cdp(1:where_slip-1)];
        perfil.pol(n-naofoi).cm		= [reorg.cm(end:-1:where_slip) reorg.cm(1:where_slip-1)];
        perfil.pol(n-naofoi).Top_Xtr= [reorg.Top_Xtr(end:-1:where_slip) reorg.Top_Xtr(1:where_slip-1)];
        perfil.pol(n-naofoi).Bot_Xtr= [reorg.Bot_Xtr(end:-1:where_slip) reorg.Bot_Xtr(1:where_slip-1)]; 
    elseif from_middle == 2
        where_slip = find(reorg.alpha > reorg.alpha(1),1);

        perfil.pol(n-naofoi).alpha	= [reorg.alpha(where_slip-1:-1:1) reorg.alpha(where_slip:end)];
        perfil.pol(n-naofoi).cl		= [reorg.cl(where_slip-1:-1:1) reorg.cl(where_slip:end)];
        perfil.pol(n-naofoi).cd		= [reorg.cd(where_slip-1:-1:1) reorg.cd(where_slip:end)];
        perfil.pol(n-naofoi).cdp	= [reorg.cdp(where_slip-1:-1:1) reorg.cdp(where_slip:end)];
        perfil.pol(n-naofoi).cm		= [reorg.cm(where_slip-1:-1:1) reorg.cm(where_slip:end)];
        perfil.pol(n-naofoi).Top_Xtr= [reorg.Top_Xtr(where_slip-1:-1:1) reorg.Top_Xtr(where_slip:end)];
        perfil.pol(n-naofoi).Bot_Xtr= [reorg.Bot_Xtr(where_slip-1:-1:1) reorg.Bot_Xtr(where_slip:end)]; 
    else
        perfil.pol(n-naofoi).alpha	= reorg.alpha;
        perfil.pol(n-naofoi).cl		= reorg.cl;
        perfil.pol(n-naofoi).cd		= reorg.cd;
        perfil.pol(n-naofoi).cdp	= reorg.cdp;
        perfil.pol(n-naofoi).cm		= reorg.cm;
        perfil.pol(n-naofoi).Top_Xtr= reorg.Top_Xtr;
        perfil.pol(n-naofoi).Bot_Xtr= reorg.Bot_Xtr; 
    end
  
    clear reorg
    
    fclose all;
    if length(perfil.pol(n-naofoi).cl) < 0.7*length(aoa)
        penal = penal + 1;
        if penal <= length(caso_nao)
            Re(n) = Re(n) + caso_nao(penal);
        else
            penal = 0;
            n = n+1;     
            naofoi = naofoi+1;
        end
    else 
        penal = 0;
        perfil.UpdateCoeff;
        
    %% Leitura de Cf
   
    for i = 1:length(perfil.pol(n-naofoi).alpha)
        j = 1;
        alpha = perfil.pol(n-naofoi).alpha(i);
        nome_cf = ['X' num2str(qual) '/cf_' num2str(alpha) '.nfo'];
        identidade2 = fopen(nome_cf);
        
        [~] = fgetl(identidade2); % Pula primeira linha
        %'#    s        x        y     Ue/Vinf    Dstar     Theta      Cf       H'
        
        line = fgetl(identidade2);
        while line~=-1
        [data,~,~,~] = sscanf(line, '%f');
        perfil.pol(n-naofoi).x(j,i) = data(2);
        perfil.pol(n-naofoi).y(j,i) = data(3);
        perfil.pol(n-naofoi).Cf(j,i) = data(7);
        perfil.pol(n-naofoi).H(j,i) = data(8);

        j=j+1;
        line    = fgetl(identidade2);
        end
        fclose all;     
    end

        %% Leitura de Cp
   
    for i = 1:length(perfil.pol(n-naofoi).alpha)
        j = 1;
        alpha = perfil.pol(n-naofoi).alpha(i);
        nome_cp = ['X' num2str(qual) '/cp_' num2str(alpha) '.nfo'];
        identidade2 = fopen(nome_cp);
        
        [~] = fgetl(identidade2); % Pula primeira linha
        [~] = fgetl(identidade2); % Pula segunda linha
        [~] = fgetl(identidade2); % Pula terceira linha
        %'#    x        y        Cp '
        
        line = fgetl(identidade2);
        while line~=-1
        [data,~,~,~] = sscanf(line, '%f');
        perfil.pol(n-naofoi).x2(j,i) = data(1);
        perfil.pol(n-naofoi).y2(j,i) = data(2);
        perfil.pol(n-naofoi).Cp(j,i) = data(3);

        j=j+1;
        line    = fgetl(identidade2);
        end
        fclose all; 
    end
    
    %% Proxima
    
        n = n+1;
    end
    delete X1/cp_*
    delete X1/cf_*
end

%% Salvar
% Caso queira salvar a cada itera��o, incluir no loop (descer end)
% mat_name_aux = matlab.lang.makeValidName(salvar_mat);
% eval([mat_name_aux  ' = perfil;'])
% save(['Perfis_salvos/' matlab.lang.makeValidName(salvar_mat)],mat_name_aux);
end
