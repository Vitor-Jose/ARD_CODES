%% C�digo para gerar curvas (atualizado para as distribui��es de Cp e Cf)
% Vitor Jos� - 30/09/2021

clear; close all; clc;

% Os perfis .dat devem ser colocados na pasta "Perfis"
% Insira o nome dos arquivos sem ".dat":
perfis = {'dae11_h2022_4'}; 

% Insira o vetor com intervalo de Reynolds:
% Re = [2e4:2e4:2.1e5 2.3e5:2.5e4:5.80e5];  
Re = [400000];

% Insira o vetor com o intervalo de �ngulo de ataque:
aoa =  -5:0.5:16;

%% Loop
for i = 1:length(perfis)
    perfis_dat = [perfis{i} '.dat'];      % Nome do perfil somado ao ".dat"
    perfis_mat = [perfis{i}];             % Nome em que ser� salvo o .mat
    Gera_function2(perfis_dat,perfis_mat,Re,aoa);
    
    fprintf('%d/%d - %s\n',i,length(perfis),perfis{i});
end

load('handel.mat')
sound(y)
