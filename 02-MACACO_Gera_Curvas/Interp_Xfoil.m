% Código responsavel por chaamar o Xfoil e realizar a interpolação dos
% perfis
function nome =  Interp_Xfoil(perfil1,perfil2,ponderamento)
%% inicialização
                                                  % ponderamento da interpolação em relação ao perfil1        

paineis = 180;



%% criando comandos 
nome_novo = [erase(perfil1,'.dat') '_' erase(perfil2,'.dat')];

comando = fopen('Interpolador_Perfis/comandos_xfoil.txt','w');                          % Abre o arquivo de comandos

fprintf(comando,'\n');    
fprintf(comando,'inte \n');                                                % Comando de interpolação no Xfoil
fprintf(comando,'f \n');                                                   % Avisa que o perfil ta no diretorio
fprintf(comando,'../Perfis/%s \n',perfil1);                                % Chama o perfil 1
fprintf(comando,'f \n');                                                    
fprintf(comando,'../Perfis/%s \n',perfil2);                                % Chama o perfil 2
fprintf(comando,'%3.2f \n',ponderamento);                                  % Define o ponderamento da interpolação
fprintf(comando,'%s \n',nome_novo);                                        % Salva o nome do novo perfil como NomePerfil1_NomePerfil2

    fprintf(comando,'ppar\n');
    fprintf(comando,'n %d\n',paineis);
    fprintf(comando,'\n');
    fprintf(comando,'\n');
    fprintf(comando,'save ../Perfis_Interpolados/%s.dat \n',nome_novo);
    fprintf(comando,'\n');
    fprintf(comando,'\n');


    fprintf(comando,'quit\n');
    

%% SOLVER Xfoil

 cd Interpolador_Perfis

   system('start /min mataxfoil2.bat'); 
   !runner2.bat >NUL

 cd ..

%% Concerta .dat

cd Perfis_Interpolados

pontos = importdata([nome_novo '.dat']);                                   % apaga a primeira linha contendo o nome do perfil interpolado
xy = pontos.data;
dlmwrite([nome_novo '.dat'], xy, 'delimiter', ' ', 'precision', 6);        % salva .dat na forma que o gera curvas consegue ler

cd .. 
nome = [nome_novo '.dat'];
end
