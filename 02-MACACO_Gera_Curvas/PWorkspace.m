function [pontuacao] = PWorkspace(x)
% x = [97.7601    9.9478    0.4835]
disp(x);

% MODELO DE OTIMIZACAO DE PERFIS
% TUCANO 2023 - CLASSE REGULAR
% Autores: Gabriel G. Thales F. Jose P.

tic;
penal = 0;

Re = [400000];
aoa = [-5:1:16];
perfil1 = seleciona_perfil(floor(x(1)));

perfil2 = seleciona_perfil(floor(x(2)));

ponderamento = x(3);

nome = Interp_Xfoil(perfil1,perfil2,ponderamento);
    
perfil = Gera_function2(nome,nome,Re,aoa);

% estol
[clmax,idx] = max(perfil.pol.cl);
estol = perfil.pol.alpha(idx);

% clalpha

dcl = perfil.pol.cl(perfil.pol.alpha == 7) - perfil.pol.cl(perfil.pol.alpha == 0);
clalpha = dcl/7;
clalpha = clalpha * 180/pi;

% cm m�dio
dcm = (perfil.pol.cm(perfil.pol.alpha == 10) - perfil.pol.cl(perfil.pol.alpha == 0))/2;

if estol < 10 || clalpha < 6
    penal = 1;
end

if penal
    pontuacao = [100 100];
else
    pontuacao = [-10*clmax -10*dcm];
end
tempo = toc;

%% -------------------------------- DISPLAY -------------------------------
disp('==================================================================');
fprintf('Clmax: %3.2f \n',clmax);
fprintf('Cm: %3.2f \n',dcm);
disp('Vetor x: '); disp(mat2str(x));
fprintf('TEMPO = %.3f\n',tempo);
disp('==================================================================');

end