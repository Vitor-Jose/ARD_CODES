% Tucano Genetic Algorithm
%% Tucano Regular 2023
clear; close all; clc;
tempoi = clock;

% VTR		"Value To Reach" (stop when std(function) < VTR)
VTR = 1.e-2;


% XVmin,XVmax   vector of lower and bounds of initial population
%    		the algorithm seems to work well only if [XVmin,XVmax]
%    		covers the region where the global minimum is expected
%               *** note: these are o bound constraints!! ***

% ======================== LIMITES DAS VARIAVEIS ==========================
%     	_________________________

OB = [
   
    0 124.9   % perfil 1 
    0 124.9   % perfil 2 
    0 1     % ponderamento
    ];
% =================================================================================

XVmin = OB(:,1)';
XVmax = OB(:,2)';


% D		number of parameters of the objective function
D = length(XVmin);

% y		problem data vector (remains fixed during optimization)
y=[];

% NP    number of population members
NP = 10*D;
NPin = 5;

% itermax       maximum number of iterations (generations)
itermax = 50;

% F     DE-stepsize F ex [0, 2]
F = 0.8;

nvezes = 1;

    options = optimoptions('gamultiobj','PopulationSize',NP,'PlotFcn',@gaplotpareto,'MaxGenerations',itermax);
    [x,fval,exitflag,output,population,scores] = gamultiobj(@PWorkspace,D,[],[],[],[],XVmin,XVmax,[],options);

tempof = clock;
tempo = tempof - tempoi;
save('OtimizacaoGA.mat');

% atentar-se a mudancas de mes
tempo = tempo(3)*86400 + tempo(4)*3600 + tempo(5)*60 + tempo(6);

horas = fix(tempo/3600);
minutos = fix((tempo/3600 - horas)*60);
segundos = fix(((tempo/3600 - horas)*60 - minutos)*60);
fprintf('Tempo de otimizacao: %dh:%dmin:%ds.\n\n', horas, minutos, segundos)


% !fim.mp3
