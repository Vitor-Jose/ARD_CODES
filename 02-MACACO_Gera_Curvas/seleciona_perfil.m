function [perfil] = seleciona_perfil(choice_perf)
if  choice_perf == 0
    perfil = 'ah79100a.dat';
elseif choice_perf == 1
    perfil ='BA1-WT-01.dat';
elseif choice_perf == 2
    perfil ='BA2-WT-01.dat';
elseif choice_perf == 3
    perfil ='BA7_s1223.dat';
elseif choice_perf == 4
    perfil ='BA7_top10.dat';
elseif choice_perf == 5
    perfil ='NACA-0012.dat';
elseif choice_perf == 6
    perfil ='NACA-0012.dat';
elseif choice_perf == 7
    perfil ='NACA-0012.dat';
elseif choice_perf == 8
    perfil ='NACA-0012.dat';
elseif choice_perf == 9
    perfil ='NACA-0012.dat';
elseif choice_perf == 10
    perfil ='NACA-0012.dat';
elseif choice_perf == 11
    perfil ='BA7_top10_WTU_01.dat';
elseif choice_perf == 12
    perfil ='BELUGA_05.dat';
elseif choice_perf == 13
    perfil ='BELUGA_MH150.dat';
elseif choice_perf == 14
    perfil ='BT825.dat';
elseif choice_perf == 15
    perfil ='NACA-2412.dat';
elseif choice_perf == 16
    perfil ='NACA-2412.dat';
elseif choice_perf == 17
    perfil ='NACA-2412.dat';
elseif choice_perf == 18
    perfil ='NACA-2412.dat';
elseif choice_perf == 19
    perfil ='BTorigin.dat';
elseif choice_perf == 20
    perfil ='ch10 (smoothed).dat';
elseif choice_perf == 21
    perfil ='CHP01_342_50.dat';
elseif choice_perf == 22
    perfil ='CHP_01.dat';
elseif choice_perf ==23
    perfil ='CLARK Y.dat';
elseif choice_perf == 24
    perfil ='dae11.dat';
elseif choice_perf == 25
    perfil ='e344.dat';
elseif choice_perf == 26
    perfil ='e423.dat';
elseif choice_perf == 27
    perfil ='e591.dat';
elseif choice_perf == 28
    perfil ='E95origin.dat';
elseif choice_perf == 29
    perfil ='NACA-2412.dat';
elseif choice_perf == 30
    perfil ='NACA-2412.dat';
elseif choice_perf == 31
    perfil ='NACA-2412.dat';
elseif choice_perf == 32
    perfil ='NACA-2412.dat';
elseif choice_perf == 33
    perfil ='EPPLER_174.dat';
elseif choice_perf == 34
    perfil ='EPPLER_197.dat';
elseif choice_perf == 35
    perfil ='EPPLER_205.dat';
elseif choice_perf == 36
    perfil ='EPPLER_334.dat';
elseif choice_perf == 37
    perfil ='EPPLER_342.dat';
elseif choice_perf == 38
    perfil ='EPPLER_421.dat';
elseif choice_perf == 39
    perfil ='ETW_02.dat';
elseif choice_perf == 40
    perfil ='ETW_02_9_5.dat';
elseif choice_perf == 41
    perfil ='ETW_05.dat';
elseif choice_perf == 42
    perfil ='ET_04.dat';
elseif choice_perf == 43
    perfil ='ET_M2007.dat';
elseif choice_perf == 44
    perfil ='FLUEGEL.dat';
elseif choice_perf == 45
    perfil ='fx60126.dat';
elseif choice_perf == 46
    perfil ='fx74cl5140.dat';
elseif choice_perf == 47
    perfil ='fx74modsm.dat';
elseif choice_perf == 48
    perfil ='fx76mp120.dat';
elseif choice_perf == 49
    perfil ='FX_63_137.dat';
elseif choice_perf == 50
    perfil ='GOE448.dat';
elseif choice_perf == 51
    perfil ='GOE652.dat';
elseif choice_perf == 52
    perfil ='h2022_1.dat';
elseif choice_perf == 53
    perfil ='h2022_2.dat';
elseif choice_perf == 54
    perfil ='h2022_3.dat';
elseif choice_perf == 55
    perfil ='h2022_4.dat';
elseif choice_perf == 56
    perfil ='h2022_5.dat';
elseif choice_perf == 57
    perfil ='h2022_6.dat';
elseif choice_perf == 58
    perfil ='h2022_7.dat';
elseif choice_perf == 59
    perfil ='HS522.dat';
elseif choice_perf == 60
    perfil ='JV.dat';
elseif choice_perf == 61
    perfil ='KC_135_winglet.dat';
elseif choice_perf == 62
    perfil ='LNV109A.dat';
elseif choice_perf == 63
    perfil ='L_MOMENT 6-H-10.dat';
elseif choice_perf == 64
    perfil ='MARSKE_XM_1D.dat';
elseif choice_perf == 65
    perfil ='mh112 16,2_ .dat';
elseif choice_perf == 66
    perfil ='MH45.dat';
elseif choice_perf == 67
    perfil ='MH6012.dat';
elseif choice_perf == 68
    perfil ='MH70.dat';
elseif choice_perf == 69
    perfil ='MH78.dat';
elseif choice_perf == 70
    perfil ='MH80.dat';
elseif choice_perf == 71
    perfil ='MH81.dat';
elseif choice_perf == 72
    perfil ='MH82.dat';
elseif choice_perf == 73
    perfil ='MH82_5.dat';
elseif choice_perf == 74
    perfil ='MH83.dat';
elseif choice_perf == 75
    perfil ='MH84.dat';
elseif choice_perf == 76
    perfil ='MHL90.dat';
elseif choice_perf == 77
    perfil ='NACA-0012.dat';
elseif choice_perf == 78
    perfil ='NACA-1412.dat';
elseif choice_perf == 79
    perfil ='NACA-2412.dat';
elseif choice_perf == 80
    perfil ='NACA-3412.dat';
elseif choice_perf == 81
    perfil ='NACA-4412.dat';
elseif choice_perf == 82
    perfil ='NACA0012.dat';
elseif choice_perf == 83
    perfil ='NACA4415.dat';
elseif choice_perf == 84
    perfil ='NACA_0012.dat';
elseif choice_perf == 85
    perfil ='NACA_1412.dat';
elseif choice_perf == 86
    perfil ='NACA_2412.dat';
elseif choice_perf == 87
    perfil ='NACA_3412.dat';
elseif choice_perf == 88
    perfil ='NACA_4412.dat';
elseif choice_perf == 89
    perfil ='N_24.dat';
elseif choice_perf == 90
    perfil ='s1210 12_.dat';
elseif choice_perf == 91
    perfil ='s1223.dat';
elseif choice_perf == 92
    perfil = 'NACA-0012.dat';
elseif choice_perf == 93
    perfil ='NACA-0012.dat';
elseif choice_perf == 94
    perfil ='NACA-0012.dat';
elseif choice_perf == 95
    perfil ='NACA-0012.dat';
elseif choice_perf == 96
    perfil ='NACA-0012.dat';
elseif choice_perf == 97
    perfil ='NACA-0012.dat';
elseif choice_perf == 98
    perfil ='s2027.dat';
elseif choice_perf == 99
    perfil ='S804.dat';
elseif choice_perf == 100
    perfil ='S835.dat';
elseif choice_perf == 101
    perfil ='SD7003.dat';
elseif choice_perf == 102
    perfil ='SG6043.dat';
elseif choice_perf == 103
    perfil ='SPICA_11p73.dat';
elseif choice_perf == 104
    perfil ='TL55.dat';
elseif choice_perf == 105
    perfil ='TL56.dat';
elseif choice_perf == 106
    perfil ='TOP10.dat';
elseif choice_perf == 107
    perfil ='TsAGI R-3a (12_).dat';
elseif choice_perf == 108
    perfil ='usnps4 (smoothed).dat';
elseif choice_perf == 109
    perfil ='wasp (smoothed).dat';
elseif choice_perf == 110
    perfil ='WTU_01.dat';
elseif choice_perf == 111
    perfil ='WT_01.dat';
elseif choice_perf == 112
    perfil ='x2017_01.dat';
elseif choice_perf == 113
    perfil ='x2020.dat';
elseif choice_perf == 114
    perfil ='x2022_1.dat';
elseif choice_perf == 115
    perfil ='x2022_10.dat';
elseif choice_perf == 116
    perfil ='x2022_2.dat';
elseif choice_perf == 117
    perfil ='x2022_3.dat';
elseif choice_perf == 118
    perfil ='x2022_4.dat';
elseif choice_perf == 119
    perfil ='x2022_5.dat';
elseif choice_perf == 120
    perfil ='x2022_6.dat';
elseif choice_perf == 121
    perfil ='x2022_7.dat';
elseif choice_perf == 122
    perfil ='x2022_8.dat';
elseif choice_perf == 123
    perfil ='x2022_9.dat';
elseif choice_perf == 124
    perfil = 'ys930.dat';
end
end