function Cm_plot = Cm_alpha(CG,nome,num)
%% ==================== Prealocacaoo e Inicialização =======================
%CG = CG;  

load(nome);
load([nome '_' num2str(num)]);

for aux_plane = 1:length(conjunto.Airplane)
plane = conjunto.Airplane(aux_plane);

geo.surfacenum = length(plane.LiftingSurfaces);
geo.motor.pos  = [0 0 -0.01];
geo.o.panel     = 15;
geo.o.itermax   = 30;
geo.tp.pos      = [-0.02 0 0.25];

for aux = 1:length(plane.LiftingSurfaces)
    geo.section_number(aux) = length(plane.LiftingSurfaces(aux).Geo.Yb)-1;
    for aux_p = 1:length(plane.LiftingSurfaces(aux).Geo.Yb)
    geo.s.b(aux,aux_p) = plane.LiftingSurfaces(aux).Geo.Yb(aux_p);
    geo.s.c(aux,aux_p) = plane.LiftingSurfaces(aux).Geo.C(aux_p);
    geo.s.twist(aux,aux_p) = plane.LiftingSurfaces(aux).Geo.TwistY(aux_p);
    geo.s.ai(aux) = plane.LiftingSurfaces(aux).Geo.Incidence;
    geo.s.eo(aux) = 0;
    geo.s.neta(aux) = 1;
    geo.s.deda(aux) = 0;
    geo.s.pos(aux,:) = [-plane.LiftingSurfaces(aux).Geo.pos.x  plane.LiftingSurfaces(aux).Geo.pos.y -plane.LiftingSurfaces(aux).Geo.pos.z]+CG;
    
    geo.s.perfil(aux,aux_p) = {plane.LiftingSurfaces(aux).Aero.Airfoils.Data(aux_p).name};
    end
    
    for aux_p = 1:length(plane.LiftingSurfaces(aux).Geo.Yb)-1
        geo.s.d(aux,aux_p) = plane.LiftingSurfaces(aux).Geo.Dihedral(aux_p);
        geo.s.e(aux,aux_p) = plane.LiftingSurfaces(aux).Geo.Sweep(aux_p);
    end
end

% save('geo_teste.mat','geo')

%% ========================= JA VEIO COM NOIS VLM =============================
aoa = eval(cond.aoa);
for j=1:geo.surfacenum
    coef(j) = x(aux_plane).coefs(j);
    coeff.fit(j).CL = fit(aoa',coef(j).CL','linearinterp');         % Nesse loop e feito o fit dos coeficientes (apenas para simplificar o acesso aos coeficientes)
    coeff.fit(j).CD = fit(aoa',coef(j).CD','linearinterp');
    coeff.fit(j).Cm25 = fit(aoa',coef(j).CM','linearinterp');
end
coef(1).Cref = plane.Geo.MAC;
%% ========================= Cálculo do Cm ================================
cont = 1;
for aoa2 = aoa
    for j=1:geo.surfacenum
    M = geo.s.neta(j)*[0 -geo.s.pos(j,3) geo.s.pos(j,2); geo.s.pos(j,3) 0 -geo.s.pos(j,1); -geo.s.pos(j,2) geo.s.pos(j,1) 0]*([cosd(aoa2) 0 -sind(aoa2);0 1 0;sind(aoa2) 0 cosd(aoa2)]*[-coeff.fit(j).CD(aoa2-geo.s.deda(j)*aoa2-geo.s.eo(j));0;-coeff.fit(j).CL(aoa2-geo.s.deda(j)*aoa2-geo.s.eo(j))]);
    Cm(j) = M(2)/coef(1).Cref + geo.s.neta(j)*coeff.fit(j).Cm25(aoa2 - geo.s.deda(j)*aoa2 - geo.s.eo(j));
    end
    Cm_aeronave = sum(Cm);
    Cm_plot(aux_plane,cont) = Cm_aeronave;
    cont = cont+1;
end 
clearvars coef
end

% openfig('cm_vsp');
% hold on
% plot(alpha_plot,Cm_plot)
% grid on
% xlabel('Alpha (Graus)')
% ylabel('Cm Total')
% title('Cm x alpha')
% hold on




        





