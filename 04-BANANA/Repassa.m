%% Programa para passar um Airplane com par�metros para o BANANA
clear; close all; clc;

nome_entrada = '../03-MACACO_Aeronaves/plane_2022_2.mat';    %Nome do arquivo de entrada
nome_saida   = '2022_2.mat';    %Nome do arquivo de saida

%% Carregando e igualando
load(nome_entrada);

conjunto = Mission;
conjunto.Airplane = obj; 

nLS = length(conjunto.Airplane.LiftingSurfaces);
%% Aloca��o
m1 = {1 erase(nome_entrada,'.mat') nLS 'ruim'};
m2 = zeros(10,5,1,nLS);
m3 = {'sem_nome'};

k = 1;
for i = 1:length(conjunto.Airplane.LiftingSurfaces)
    ate = length(conjunto.Airplane.LiftingSurfaces(i).Geo.Yb);
    m2(1,1:ate,1,i)    = conjunto.Airplane.LiftingSurfaces(i).Geo.Yb;
    m2(2,1:ate,1,i)    = conjunto.Airplane.LiftingSurfaces(i).Geo.C;
    m2(4,1:ate,1,i)    = conjunto.Airplane.LiftingSurfaces(i).Geo.TwistY;        
    m2(5,1:ate-1,1,i)  = conjunto.Airplane.LiftingSurfaces(i).Geo.Dihedral;      
    m2(6,1:ate-1,1,i)  = conjunto.Airplane.LiftingSurfaces(i).Geo.Sweep;         
    m2(7,1,1,i)        = conjunto.Airplane.LiftingSurfaces(i).Geo.Incidence;     
    m2(8,1,1,i)        = conjunto.Airplane.LiftingSurfaces(i).Geo.pos.x;    
    m2(8,2,1,i)        = conjunto.Airplane.LiftingSurfaces(i).Geo.pos.y;        
    m2(8,3,1,i)        = conjunto.Airplane.LiftingSurfaces(i).Geo.pos.z; 
    m2(9,1:ate-1,1,i)  = conjunto.Airplane(1).LiftingSurfaces(i).Aero.Mesh.TypeY;   
    m2(10,1:ate-1,1,i) = conjunto.Airplane(1).LiftingSurfaces(i).Aero.Mesh.Ny;
    for contaperfil = 1:ate
        saida = (find(contains(m3,conjunto.Airplane.LiftingSurfaces(i).Aero.Airfoils.Data(contaperfil).name)));
        if isempty(saida)
            m3{k,1} = [conjunto.Airplane.LiftingSurfaces(i).Aero.Airfoils.Data(contaperfil).name '.mat']; 
            m2(3,contaperfil,1,i) = k;   
            k = k + 1;
        else
            m2(3,contaperfil,1,i) = saida;   
        end
    end
end
conjunto.Airplane.UpdateGeo;
conjunto.Airplane.UpdateAeroMesh;

save(nome_saida,'m1','m2','m3','conjunto');