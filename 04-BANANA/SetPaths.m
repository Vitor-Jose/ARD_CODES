disp('%--------------------------------------------%')
disp('%     _    _ _____ ____ _____ ____ _____     %')
disp('%    | \  / |  _  |  __|  _  |  __|  _  |    %')
disp('%    |  \/  | [_] | (__| [_] | (__| (_) |    %')
disp('%    |_|  |_|_/ \_|____|_/ \_|____|_____|    %')
disp('% Multi-Analysis  Code for Airplane COncepts %')
disp('%                                            %')
disp('%    UNIVERSIDADE FEDERAL DE UBERLANDIA      %')
disp('%     Andre Rezende Dessimoni Carvalho       %')
disp('%                                            %')
disp('% Special thanks to:                         %')
disp('% ASES - Athena Society of Eng. and Science  %')
disp('% TUCANO AeroDesign                          %')
disp('%                                            %')
disp('%--------------------------------------------%')

cd ..
cd 01-MACACO
folders = {	'00-classes'
			'00-common'
			'00-others'
            '00-db_components'
			'01-geometric'
			'02-aerodynamics'
            'CURVAS'
			};
		
for i=1:length(folders)
	f = folders{i};
		cd(f)
		addpath(genpath(pwd))
		cd ..
end
cd ..
cd 04-BANANA
clear f folders i

