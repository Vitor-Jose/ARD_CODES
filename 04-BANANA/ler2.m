function [x,cond] =ler2(nome,n)
    nome = erase(nome,'.mat');
    nn = num2str(n);
    nome = [nome '_' nn '.mat'];
    load(nome)   
end