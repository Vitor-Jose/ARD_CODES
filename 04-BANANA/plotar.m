function plotar(cond,m2,m3)
%% Qual o plot?
plotar_geo = 1;   % 0 - N�o plota
                  % 1 - Plota com perfis
                  % 2 - Plota sem perfis
                  
plotar_mesh = 1;  % 0 - N�o plota
                  % 1 - Plota malha
                  
area = 1;

%% Perfis
for i = 1:length(m3)
    perfil(i)=ImportObj(m3{i}); %Para perfil fora da pasta
end

%% Defini��o das aeronaves
for j = 1:cond.n 
    plane=Airplane;
    if cond.s(1) == 0  
        for i = 1:cond.sup(j)
            at = find(m2(1,2:5,j,i) == 0);
            if isempty(at)
                at = length(m2(1,:,j,1));
            end
             ate(i) = at(1);
             plane.LiftingSurfaces(i)=LiftingSurface('Asa Principal'); %Opcional para asa 1
             plane.LiftingSurfaces(i).Geo.Yb=m2(1,1:ate(i),j,i);
             plane.LiftingSurfaces(i).Geo.C=m2(2,1:ate(i),j,i);
             plane.LiftingSurfaces(i).Geo.Sweep=m2(6,1:ate(i)-1,j,i);
             plane.LiftingSurfaces(i).Geo.Dihedral= m2(5,1:ate(i)-1,j,i);
             plane.LiftingSurfaces(i).Geo.Incidence= m2(7,1,j,i);
             plane.LiftingSurfaces(i).Geo.TwistY=m2(4,1:ate(i),j,i);
             plane.LiftingSurfaces(i).IsMain= 0;
             if i == 1
                plane.LiftingSurfaces(i).IsMain= 1;
             end
             for ii = 1:ate(i)
                 p(ii) = perfil(m2(3,ii,j,i));
             end
             plane.LiftingSurfaces(i).Aero.Airfoils.Data = p(1:ate(i));
             plane.LiftingSurfaces(i).Geo.pos.x = m2(8,1,j,i);
             plane.LiftingSurfaces(i).Geo.pos.y = m2(8,2,j,i);
             plane.LiftingSurfaces(i).Geo.pos.z = m2(8,3,j,i);
             plane.LiftingSurfaces(i).Aero.Mesh.Ny = m2(10,1:ate(i)-1,j,i);
             plane.LiftingSurfaces(i).Aero.Mesh.TypeY = m2(9,1:ate(i)-1,j,i); 
        end
        plane.UpdateGeo;
        plane.PlotGeo('-airfoils','-nimage',j);
    else
        for i = 1:cond.sup(cond.s(j))
            at = find(m2(1,2:5,cond.s(j),i) == 0);
            if isempty(at)
                at = length(m2(1,:,cond.s(j),1));
            end
            ate(i) = at(1);
            plane.LiftingSurfaces(i)=LiftingSurface('Asa Principal'); %Opcional para asa 1
            plane.LiftingSurfaces(i).Geo.Yb=m2(1,1:ate(i),cond.s(j),i);
            plane.LiftingSurfaces(i).Geo.C=m2(2,1:ate(i),cond.s(j),i);
            plane.LiftingSurfaces(i).Geo.Sweep=m2(6,1:ate(i)-1,cond.s(j),i);
            plane.LiftingSurfaces(i).Geo.Dihedral= m2(5,1:ate(i)-1,cond.s(j),i);
            plane.LiftingSurfaces(i).Geo.Incidence= m2(7,1,cond.s(j),i);
            plane.LiftingSurfaces(i).Geo.TwistY=m2(4,1:ate(i),cond.s(j),i);
            plane.LiftingSurfaces(i).IsMain= 0;
            if i == 1
                plane.LiftingSurfaces(i).IsMain= 1;
            end
            for ii = 1:ate(i)
                p(ii) = perfil(m2(3,ii,cond.s(j),i));
            end
            plane.LiftingSurfaces(i).Aero.Airfoils.Data = p(1:ate(i));
            plane.LiftingSurfaces(i).Geo.pos.x = m2(8,1,cond.s(j),i);
            plane.LiftingSurfaces(i).Geo.pos.y = m2(8,2,cond.s(j),i);
            plane.LiftingSurfaces(i).Geo.pos.z = m2(8,3,cond.s(j),i);
            plane.LiftingSurfaces(i).Aero.Mesh.Ny = m2(10,1:ate(i)-1,cond.s(j),i);
            plane.LiftingSurfaces(i).Aero.Mesh.TypeY = m2(9,1:ate(i)-1,cond.s(j),i);            
        end
        plane.UpdateGeo;
        plane.UpdateAeroMesh;  
        
        if area
            fprintf('Area: %6.4f \n',plane.LiftingSurfaces(1).Geo.Area.Sz);
        end
            
        
%% Plots
        if plotar_geo == 1
            plane.PlotGeo('-airfoils','-nimage',cond.s(j));
            view([-40,30])
        elseif plotar_geo == 2
            plane.PlotGeo('-nimage',cond.s(j));
            view([-40,30])            
        end
        if plotar_mesh
            plane.PlotMesh('-nimage',cond.s(j)+100);
        end   
    end
end
end