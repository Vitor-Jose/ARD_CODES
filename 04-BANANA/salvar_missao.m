function salvar_missao(nome,m1,m2,m3)
%% Missao 
conjunto = Mission;

%% Definindo perfis   
 for p = 1:length(m3)
    perfil(p)=ImportObj(m3{p}); 
 end
 
%% Definindo as aeronaves
 for j = 1:length(m1(:,1))
     for i = 1:m1{j,3}
         at = find(m2(1,2:5,j,i) == 0);
         if isempty(at)
             at = length(m2(1,:,j,i));
         end
         ate = at(1);
         conjunto.Airplane(j).LiftingSurfaces(i)=LiftingSurface(join(['Aero' num2str(j) 'Sup' num2str(i)])); %Opcional para asa 1

         conjunto.Airplane(j).LiftingSurfaces(i).Geo.Yb=m2(1,1:ate,j,i);           %1
         conjunto.Airplane(j).LiftingSurfaces(i).Geo.C=m2(2,1:ate,j,i);
         for ii = 1:ate
             pp(ii) = perfil(m2(3,ii,j,i));
         end
         conjunto.Airplane(j).LiftingSurfaces(i).Aero.Airfoils.Data = pp(1:ate);         
         conjunto.Airplane(j).LiftingSurfaces(i).Geo.TwistY=m2(4,1:ate,j,i);
         conjunto.Airplane(j).LiftingSurfaces(i).Geo.Dihedral= m2(5,1:ate-1,j,i);
         conjunto.Airplane(j).LiftingSurfaces(i).Geo.Sweep=m2(6,1:ate-1,j,i);
         conjunto.Airplane(j).LiftingSurfaces(i).Geo.Incidence= m2(7,1,j,i);
         conjunto.Airplane(j).LiftingSurfaces(i).Geo.pos.x = m2(8,1,j,i);
         conjunto.Airplane(j).LiftingSurfaces(i).Geo.pos.y = m2(8,2,j,i);
         conjunto.Airplane(j).LiftingSurfaces(i).Geo.pos.z = m2(8,3,j,i);         
         if i == 1
            conjunto.Airplane(j).LiftingSurfaces(i).IsMain= 1;
         else
            conjunto.Airplane(j).LiftingSurfaces(i).IsMain= 0;
         end
         clear pp
         
         %% Malha
         conjunto.Airplane(j).LiftingSurfaces(i).Aero.Mesh.Ny = m2(10,1:ate-1,j,i);
         conjunto.Airplane(j).LiftingSurfaces(i).Aero.Mesh.TypeY = m2(9,1:ate-1,j,i);  
     end
     
     conjunto.Airplane(j).UpdateGeo;
     conjunto.Airplane(j).UpdateAeroMesh;
     
 end
 save(nome,'m1','m2','m3','conjunto');
 end

