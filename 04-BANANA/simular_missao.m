function simular_missao(cond,nome)
% coefs  - Coeficientes tridimensionais das superf�cies
% coefs0 - Coeficientes tridimensionais da aeronave
% graf   - Distribui��es dos coeficientes bidimensionais e maalha
fprintf('================================= \n')
fprintf('In�cio da simula��o das aeronaves \n')
%% Carregando aeronaves
load(nome)

%% Atribuindo valores 
aoa= eval(cond.aoa);
v = eval(cond.v);
beta = cond.beta;

%% Condi��es de voo
conjunto.FltCond(cond.n) = FlightConditions;
conjunto.FltCond(cond.n).rho = cond.rho; 

%% Loops 
for n = 1:length(cond.nplane)
    tic
    k = 1; %Onde est�o alocados os coeficientes no 'append'
    for i=1:length(v)
        for j=1:length(aoa)
            conjunto.FltCond(cond.n).alpha = aoa(j);
            conjunto.FltCond(cond.n).beta = beta;
            conjunto.FltCond(cond.n).Voo = v(i);
            conjunto.Airplane(n).Aero.Settings.NLVLM.Cl_from_adjusted_2DCurvesEq = 1;
            
            results = NL_VLM(conjunto.Airplane(n),conjunto.FltCond(cond.n),'-append','-itermax',30,'-dispstat');

            for s = 1:length(conjunto.Airplane(n).LiftingSurfaces)
                x(n).coefs(s).CL(i,j) = conjunto.Airplane(n).LiftingSurfaces(s).Aero.Coeffs(k).CL;
                x(n).coefs(s).CD0(i,j) = conjunto.Airplane(n).LiftingSurfaces(s).Aero.Coeffs(k).CD0;
                x(n).coefs(s).CDi(i,j) = conjunto.Airplane(n).LiftingSurfaces(s).Aero.Coeffs(k).CDi;
                x(n).coefs(s).CD(i,j) = conjunto.Airplane(n).LiftingSurfaces(s).Aero.Coeffs(k).CD;
                x(n).coefs(s).CM(i,j) = conjunto.Airplane(n).LiftingSurfaces(s).Aero.Coeffs(k).Cm25;
                x(n).coefs(s).Cn(i,j) = conjunto.Airplane(n).LiftingSurfaces(s).Aero.Coeffs(k).Cn;
                x(n).coefs(s).Cl_rol(i,j) = conjunto.Airplane(n).LiftingSurfaces(s).Aero.Coeffs(k).Cl_rol;
                x(n).coefs(s).ar =  conjunto.Airplane(n).LiftingSurfaces(s).Geo.AR;
                x(n).coefs(s).r = (x(n).coefs(s).CL)./(x(n).coefs(s).CD);
                x(n).coefs(s).e = ((x(n).coefs(s).CL).^2)./(x(n).coefs(s).CDi.*pi.*x(n).coefs(s).ar);
                x(n).coefs(s).alpha(i,j) = aoa(j);
                
                x(n).graf(s).dist = conjunto.Airplane(n).LiftingSurfaces(s).Aero.Mesh.Yb;
                x(n).graf(s).cl(i,j,:) = conjunto.Airplane(n).LiftingSurfaces(s).Aero.Loads(k).cl;
                x(n).graf(s).cd(i,j,:) = conjunto.Airplane(n).LiftingSurfaces(s).Aero.Loads(k).cd0;
                x(n).graf(s).cm(i,j,:) = conjunto.Airplane(n).LiftingSurfaces(s).Aero.Loads(k).cm25;
                x(n).graf(s).alpha_ind(i,j,:) = conjunto.Airplane(n).LiftingSurfaces(s).Aero.Loads(k).alpha_ind;
                x(n).graf(s).alpha_eff(i,j,:) = -conjunto.Airplane(n).LiftingSurfaces(s).Aero.Loads(k).alpha_ind + aoa(j);
                x(n).graf(s).Re_pan(i,j,:) = conjunto.Airplane(n).LiftingSurfaces(s).Aero.Loads(k).Re_pan;
            end
        
        x(n).coefs0.CL(i,j) = conjunto.Airplane(n).Aero.Coeffs(k).CL;
        x(n).coefs0.CD(i,j) = conjunto.Airplane(n).Aero.Coeffs(k).CD;
        x(n).coefs0.Cn(i,j) = conjunto.Airplane(n).Aero.Coeffs(k).Cn;
        x(n).coefs0.Cl_rol(i,j) = conjunto.Airplane(n).Aero.Coeffs(k).Cl_rol;
        k = k+1;
        end
    end 
    x(n).coefs0.r = (x(n).coefs0.CL)./(x(n).coefs0.CD);
    tempo = toc;
    fprintf('%d de %d completos|%6.3fs|%s \n',n,length(cond.nplane),tempo,m1{cond.nplane(n),2});
end
%% M�dulo de salvamento de dados
nomef = erase(nome,'.mat');
nn = num2str(cond.n);
nomef = [nomef '_' nn '.mat'];
save(nomef,'x','cond')
fprintf('Simula��o finalizada e salvas em "%s" \n', nomef)
end