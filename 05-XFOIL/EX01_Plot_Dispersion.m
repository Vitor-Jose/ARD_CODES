%% Exemplo de c�digo para plotar a dispers�o de perfis (Relatorio de 2021)
% Vitor Jos�

%% LIMPEZA
clear; close all; clc;

%% FILA
% Insere-se o nome dos arquivos .dat a serem analisados

% Asa inferior
analises1 = {'L1_3c' 'L1_original' 'L1_1_20' 'L1_1_40' 'L1_1_75' 'L1_1'};  % Grupo 1 de perfis obtido por otimiza��o
analises2 = {'L2_original' 'L2_1_20' 'L2_1_40' 'L2_L1_3c_50'};             % Grupo 2 de perfis obtido por otimiza��o
analises_i = {'Li1' 'Li2' 'Li3' 'Li4' 'Li5' 'Li6' 'Li7' 'Li8' 'Li10' ...   % Indiv�duos criados para povoar o plot
    'Li11'}; 

% Asa superior
analises3 = {'M1_3e3' 'M1_3c' 'M1_2e' 'M1_original' 'M1_3e'  'M1_2d' ...   % Grupo 3 de perfis obtido por otimiza��o
    'M1_3d' 'M1_1_20' 'M1_1_40'};                                          
analises4 = {'M2_3_50' 'M2_3'  'M2_original' 'M2_1'};                      % Grupo 4 de perfis obtido por otimiza��o
analises_j = {'Mi1' 'Mi2' 'Mi3' 'Mi4' 'Mi5' 'Mi6' 'Mi7' 'Mi8' 'Mi9'};      % Indiv�duos criados para povoar o plot

%% SELECIONA
% Une as c�lulas e marca individuos (Comentar ou descomentar grupos)

% Asa inferior
analises = [analises1 analises2 analises_i];
m = [2 7]; % Perfis iniciais
n = 1;     % Perfis finais

% Asa superior
% analises = [analises3 analises4 analises_j];
% m = [4 12];    % Perfis iniciais
% n = [1 2 10];  % Perfis finais

%% CONDI��ES
AoA = 10;
Re = 258000;
Path = 'Perfis/OTM_Regular_2021/';

%% LOOP
n_perfis = length(analises);

% Pr�-aloca��o
Cl = zeros(1,n_perfis);
Cd = zeros(1,n_perfis);
Cm = zeros(1,n_perfis);

for i = 1:n_perfis
    nome_dat = [Path analises{i} '.dat'];
    [Dados] = Pega_function(nome_dat,Re,AoA);     % Fun��o que gera curvas
    fprintf('%d/%d - %s\n',i,length(analises),analises{i});
    Cl(i) = Dados.cl;
    Cd(i) = Dados.cd;
    Cm(i) = Dados.cm;  
end

f = figure(1);


subplot(1,2,1)
plot(Cm,Cl,'ok'); hold on;
plot(Cm(n),Cl(n),'or');
plot(Cm(m),Cl(m),'og');
xlabel('C_m'); ylabel('C_l');
grid on;

subplot(1,2,2)
plot(Cl./Cd,Cl,'ok'); hold on;
plot(Cl(n)./Cd(n),Cl(n),'or');
plot(Cl(m)./Cd(m),Cl(m),'og');
xlabel('C_l/C_d'); ylabel('C_l')
grid on;

% f2 = figure(2);
% 
% plot3(Cm,Cl./Cd,Cl,'o'); hold on;
% plot3(Cm(n),Cl(n)./Cd(n),Cl(n),'or'); hold on;
