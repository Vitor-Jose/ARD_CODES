function [Dados] = Pega_function(abrir_dat_input,Re_input,aoa_input,varargin)
%% INPUTS
qual    = 1;            % Define o diret�rio do xfoil
Re      = Re_input;     % Define o intervalo de Reynolds                      
aoa     = aoa_input;    % Define o intervalo de �ngulo de ataque
abrir_dat = abrir_dat_input; % Nome do arquivo a abrir

% Bandeiras
par_logico      = false; % [Espessura; Pos. Espessura; Cambra; Pos. Cambra]

% Bandeiras do usu�rio
i = 1;
while i <= length(varargin)
	switch varargin{i}
		case 'Parametros'
			par_logico = true;
            par = varargin{i+1};
			i=i+2;
		otherwise
			warning(['Op��o desconhecida: ' varargin{i}])
			i=i+1;
	end
end

if par_logico
    esp_vertical    = par(1);
    esp_horizontal  = par(2);
    camb_vertical   = par(3);
    camb_horizontal = par(4);
end


paineis = 140;
iteracoes = 100;

Re_ncrit = [1e5 2e5 3e5];
ncrit = [5.5 7 8.5 9];

%% OPCOES
plotar_xfoil= 1;    % Ativa o plot ao vivo do xfoil

from_middle = 0;    % Op��o de come�ar o �ngulo de ataque pelo meio do vetor
                    % 0 - Desativado
                    % 1 - Ativado crescente do meio
                    % 2 - Ativado decrescente do meio
middle      = 0;    % Qual o primeiro valor, caso from_middle = 1 ou 2

if from_middle == 1
    initial_aoa_pos = find(aoa == middle);
    ordem_aoa = aoa(initial_aoa_pos:end);
    ordem_aoa(end+1:length(aoa)) = aoa(initial_aoa_pos-1:-1:1);
elseif from_middle == 2
    initial_aoa_pos = find(aoa == middle);
    ordem_aoa = aoa(initial_aoa_pos:-1:1);
    ordem_aoa(end+1:length(aoa)) = aoa(initial_aoa_pos+1:end);
else
    ordem_aoa = aoa;
end

n = 1;

while n <= length(Re)
    for k = 1:length(Re_ncrit)
        if Re(n) >= Re_ncrit(k)
            ncrit_escrito = ncrit(k+1);
        else
            ncrit_escrito = ncrit(k);
            break
        end
    end
    
    nome_ordem = ['X' num2str(qual) '/ordens' num2str(qual) '.txt'];
    comando = fopen(nome_ordem,'w');
    fprintf(comando,'\n');
    fprintf(comando,'load ../%s\n',abrir_dat);
    fprintf(comando,'%s\n',erase(abrir_dat,'.dat'));
    
    if ~plotar_xfoil
        fprintf(comando,'plop\n');
        fprintf(comando,'g\n');
        fprintf(comando,'\n');
    end
    
    if par_logico
        fprintf(comando,'gdes\n');
        fprintf(comando,'tset\n');
        fprintf(comando,'%f\n',esp_vertical);
        fprintf(comando,'%f\n',camb_vertical);
        fprintf(comando,'high\n');
        fprintf(comando,'%f\n',esp_horizontal);
        fprintf(comando,'%f\n',camb_horizontal);
        fprintf(comando,'x\n');
        fprintf(comando,'\n');
    end
    
    fprintf(comando,'ppar\n');
    fprintf(comando,'n %d\n',paineis);
    fprintf(comando,'\n');
    fprintf(comando,'\n');

    fprintf(comando,'oper\n');
    fprintf(comando,'v\n');
    fprintf(comando,'%d\n',Re(n));
    fprintf(comando,'iter %d\n',iteracoes);
    fprintf(comando,'vpar\n');
    fprintf(comando,'n %f\n',ncrit_escrito);
    fprintf(comando,'\n');
    fprintf(comando,'p\n');
    fprintf(comando,'provisorio%d.nfo\n',qual);
    fprintf(comando,'\n');
    
    for i = 1:length(ordem_aoa)
        fprintf(comando,'a %g\n',ordem_aoa(i));
        fprintf(comando,'dump\n');
        fprintf(comando,['cf_' num2str(ordem_aoa(i)) '.nfo\n']);
    end
    fprintf(comando,'p\n');
    fprintf(comando,'\n');
    fprintf(comando,'quit\n');
    
    switch qual
        case 1
            cd X1
            delete provisorio1.nfo
            system('start /min mataxfoil1.bat'); 
            !runner1.bat >NUL
            cd ..
        case 2
            cd X2
            delete provisorio2.nfo
            system('start /min mataxfoil2.bat'); 
            !runner2.bat >NUL
            cd ..   
    end

    nome_provisorio = ['X' num2str(qual) '/provisorio' num2str(qual) '.nfo'];
    identidade = fopen(nome_provisorio);

    for i=1:7
        [~] = fgetl(identidade);
    end

    line = fgetl(identidade);
    info = strsplit(line);
    conf.xtrf_top = info(4);
    conf.xtrf_bot = info(6);

    line = fgetl(identidade);
    info = strsplit(line);
    Re2 = str2double([info{7:9}]);
    conf.mach = info(4);
    conf.ncrit = info(12);

    conf.xhinge = [];
    conf.yhinge = [];
    conf.defex_hinge = [];

    for i=10:12
        [~] = fgetl(identidade);
    end
    
    j=1;
    line    = fgetl(identidade);
    
    while line~=-1
        [data,~,~,~] = sscanf(line, '%f');
        
        Dados(n).Re         = Re2;
        Dados(n).alpha(j)	= data(1);
        Dados(n).cl(j)		= data(2);
        Dados(n).cd(j)		= data(3);
        Dados(n).cdp(j) 	= data(4);
        Dados(n).cm(j)		= data(5);
        Dados(n).Top_Xtr(j) = data(6);
        Dados(n).Bot_Xtr(j) = data(7);

        j=j+1;
        line    = fgetl(identidade);
    end
    
    fclose all;  
    n = n+1;
end

end