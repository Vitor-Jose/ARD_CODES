%% C�digo para pegar dados diretamente do XFOIL
% Vitor Jos�

%% LIMPEZA
clear; close all; clc;

%% FILA
% Insere-se o nome dos arquivos .dat a serem analisados

analises = {'NACA0012' 'naca0009'};       % Grupo de perfis

%% CONDI��ES
AoA = [0:0.5:15];
Re = [3e5];
Path = 'Perfis/';
%% LOOP
n_perfis = length(analises);

% Pr�-aloca��o
Cl = zeros(1,n_perfis);
Cd = zeros(1,n_perfis);
Cm = zeros(1,n_perfis);

for i = 1:n_perfis
    nome_dat = [Path analises{i} '.dat'];
    Dados(i,:) = Pega_function(nome_dat,Re,AoA);     % Fun��o que gera curvas
    fprintf('%d/%d - %s\n',i,n_perfis,analises{i});
    Cl(:) = Dados(i,1).cl(1);
    Cd(:) = Dados(i,1).cd(1);
    Cm(:) = Dados(i,1).cm(1);  
end

f = figure(1);

plot([-flip(Dados(1,1).alpha) Dados(1,1).alpha],[-flip(Dados(1,1).cl) Dados(1,1).cl],'LineWidth',2); hold on
plot([-flip(Dados(2,1).alpha) Dados(2,1).alpha],[-flip(Dados(2,1).cl) Dados(2,1).cl],'LineWidth',2); hold on
xlabel('\alpha [�]'); ylabel('C_l');
title('Re=3e5')
% subplot(1,2,1)
% plot(Cm,Cl,'ok'); hold on;
% xlabel('C_m'); ylabel('C_l');
% grid on;
% 
% subplot(1,2,2)
% plot(Cl./Cd,Cl,'ok'); hold on;
% xlabel('C_l/C_d'); ylabel('C_l')
% grid on;


