%% C�digo para pegar dados diretamente do XFOIL
% Vitor Jos�

%% LIMPEZA
clear; close all; clc;

%% FILA
% Insere-se o nome dos arquivos .dat a serem analisados

analises = {'CHP01_342_50' 'BA7_top10_WTU_01'};       % Grupo de perfis

%% CONDI��ES
AoA = [-3:0.5:18];
Re = [3e5];
Path = 'Perfis/';
%% LOOP
n_perfis = length(analises);

% Pr�-aloca��o
Cl = zeros(1,n_perfis);
Cd = zeros(1,n_perfis);
Cm = zeros(1,n_perfis);

for i = 1:n_perfis
    nome_dat = [Path analises{i} '.dat'];
    Dados(i,:) = Pega_function(nome_dat,Re,AoA);     % Fun��o que gera curvas
    fprintf('%d/%d - %s\n',i,n_perfis,analises{i});
    Cl(:) = Dados(i,1).cl(1);
    Cd(:) = Dados(i,1).cd(1);
    Cm(:) = Dados(i,1).cm(1);  
end

f = figure(1);

subplot(2,2,[1 3])
plot(Dados(1,1).alpha, Dados(1,1).cl,'--k','LineWidth',1); hold on
plot(Dados(2,1).alpha, Dados(2,1).cl,'-b','LineWidth',1.5); hold on
xlabel('\alpha [�]'); ylabel('c_l');
title('c_l'); grid on;
legend('Pr�vio','Otimizado')

subplot(2,2,2)
plot(Dados(1,1).alpha, Dados(1,1).cd,'--k','LineWidth',1); hold on
plot(Dados(2,1).alpha, Dados(2,1).cd,'-b','LineWidth',1.5); hold on
xlabel('\alpha [�]'); ylabel('c_d');
title('c_d');  grid on;

subplot(2,2,4)
plot(Dados(1,1).alpha, Dados(1,1).cm,'--k','LineWidth',1); hold on
plot(Dados(2,1).alpha, Dados(2,1).cm,'-b','LineWidth',1.5); hold on
xlabel('\alpha [�]'); ylabel('c_m');  grid on;
title('c_m');

% subplot(1,2,1)
% plot(Cm,Cl,'ok'); hold on;
% xlabel('C_m'); ylabel('C_l');
% grid on;
% 
% subplot(1,2,2)
% plot(Cl./Cd,Cl,'ok'); hold on;
% xlabel('C_l/C_d'); ylabel('C_l')
% grid on;


