%% C�digo para pegar dados diretamente do XFOIL
% Vitor Jos�

%% LIMPEZA
clear; close all; clc;

%% FILA
% Insere-se o nome dos arquivos .dat a serem analisados

analises = {'M1_original' 'M2_original'};       % Grupo de perfis

%% CONDI��ES
AoA = [0:16];
Re = [300000 350000];
Path = 'Perfis/OTM_Regular_2021/';

%% LOOP
n_perfis = length(analises);

% Pr�-aloca��o
% Cl = zeros(1,n_perfis);
% Cd = zeros(1,n_perfis);
% Cm = zeros(1,n_perfis);

for i = 1:n_perfis
    nome_dat = [Path analises{i} '.dat'];
    Dados(i,:) = Pega_function(nome_dat,Re,AoA);     % Fun��o que gera curvas
    fprintf('%d/%d - %s\n',i,n_perfis,analises{i});
end

% f = figure(1);
% 
% subplot(1,2,1)
% plot(Cm,Cl,'ok'); hold on;
% xlabel('C_m'); ylabel('C_l');
% grid on;
% 
% subplot(1,2,2)
% plot(Cl./Cd,Cl,'ok'); hold on;
% xlabel('C_l/C_d'); ylabel('C_l')
% grid on;
