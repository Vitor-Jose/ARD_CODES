%% Junta os dados
% Jan /2022

% Formato do output
% dados(perfil,aoa,Re)

%% LIMPEZA
clear; close all; clc;

%% INPUT
Re = 2e5;
AoA = 5;

%% ARQUIVOS
Path = 'Perfis_relatorio';

Files = dir(Path);
Names = {Files.name};
Names(1:2) = [];

File_data = 'bancoh.mat';        % Arquivo a ser carregado
if exist(File_data,'file') == 2
    load(File_data)
else
    dados.ID = [];
    dados.nome = ".";
end
dados.Re = Re;
dados.AoA = AoA;
%% LOOP
f = figure;
cont = length(dados.ID);            % Contador do numero de perfis ja existentes no arquivo
for i = 1:length(Names) 
    cd(Path)
    perfil_aux = ImportObj(Names{i});
    cd ..
    
    if sum(dados.nome == perfil_aux.name) == 0  % Caso perfil n�o cadastrado
        dados.ID(cont+1) = cont+1;
        dados.nome(cont+1) = string(perfil_aux.name);
        dados.filename(cont+1) = string(Names{i});
        
        % Coeficientes
        for j = 1:length(Re)
            ind_Re = find([perfil_aux.coef.Re]>=Re(j),1,'first');
            
            dados.cl(cont+1,:,j) = perfil_aux.cl(AoA,Re(j));                        % Calculado pelas equa��es
            dados.clmax(cont+1,1,j) = perfil_aux.coef(ind_Re).clmax;
            dados.clmin(cont+1,1,j) = perfil_aux.coef(ind_Re).clmin;
            dados.cla(cont+1,1,j) = perfil_aux.coef(ind_Re).cla;
            dados.cl0(cont+1,1,j) = perfil_aux.coef(ind_Re).cl0;
            dados.a_clmax(cont+1,1,j) = perfil_aux.coef(ind_Re).a_clmax;
            dados.a_clmin(cont+1,1,j) = perfil_aux.coef(ind_Re).a_clmin;
            dados.a0(cont+1,1,j) = perfil_aux.coef(ind_Re).a0;
            
            for k = 1:length(AoA)
                dados.cl_interp(cont+1,k,j) = perfil_aux.cl(AoA(k),Re(j),'interp'); % Calculado por interpola��o
                [dados.cd(cont+1,k,j),dados.cm(cont+1,k,j)] = perfil_aux.cdcm(AoA(k),Re(j));
            end
        end

        fprintf('Novo: %s \n',perfil_aux.name)
        cont = cont+1;
    else
        fprintf('Perfil j� cadastrado: %s \n',perfil_aux.name)
    end
    
% ===========================================================
    
    plot(perfil_aux.xy(:,1),perfil_aux.xy(:,2),'k','LineWidth',1);
    title(Names{i})
    axis equal
    axis([0 1 -0.05 0.25]);
    xticks(0:0.2:1);
    f.Position = [403 508 438 158];
    
    % saveas(f,['Fotos/' erase(Names{i},'.mat') '.png'])
    % saveas(f,['Fotos/' erase(Names{i},'.mat') '.fig'])
    % saveas(f,['Fotos/' erase(Names{i},'.mat') '.eps'])
    %saveas(f,'Batata.png')
%    saveas(f,['Plots/Airfoils1/' Names{i} '.eps'])  
end

File_data2 = 'bancoh.mat';                % Arquivo a ser salvo
save(File_data2,'dados')
