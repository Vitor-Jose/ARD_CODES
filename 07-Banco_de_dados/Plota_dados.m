%% Junta os dados
% Jan /2022

% Formato do output
% dados(perfil,aoa,Re)

%% LIMPEZA
clear; close all; clc;

%% INPUT
AoA = 5; Re = 4e5;

%% ARQUIVOS
Path = 'Perfis_salvos';

File_data = 'bancohiper.mat';
load(File_data)

%% Exemplo: Encontrar cl crescentes
aux_AoA = dados.AoA == AoA;
aux_Re = dados.Re == Re;

% [cl_cres,pos_cres] = sort(dados.cl(:,aux_AoA,aux_Re));
% nome_cres = dados.nome(pos_cres);
% fprintf('Perfis organizados em ordem crescente de cl:\n');
% disp([nome_cres' dados.cl(pos_cres,aux_AoA,aux_Re)]);

f = figure;
f.Position = [100 100 1000 480];
nomes = cellstr(dados.filename);
subplot(1,3,1)
plot(dados.cd(:,aux_AoA,aux_Re),dados.cl(:,aux_AoA,aux_Re),'or');
% text(dados.cd(:,aux_AoA,aux_Re),dados.cl(:,aux_AoA,aux_Re),nomes,'FontSize',8);
xlabel('Cd'); ylabel('Cl'); grid on;
subplot(1,3,2)
plot(dados.cl(:,aux_AoA,aux_Re)./dados.cd(:,aux_AoA,aux_Re),dados.cl(:,aux_AoA,aux_Re),'or')
% text(dados.cl(:,aux_AoA,aux_Re)./dados.cd(:,aux_AoA,aux_Re),dados.cl(:,aux_AoA,aux_Re),nomes,'FontSize',8)
xlabel('Cl/Cd'); ylabel('Cl'); grid on;
subplot(1,3,3)
plot(dados.cm(:,aux_AoA,aux_Re),dados.cl(:,aux_AoA,aux_Re),'or')
text(dados.cm(:,aux_AoA,aux_Re),dados.cl(:,aux_AoA,aux_Re),nomes,'FontSize',8)
xlabel('Cm'); ylabel('Cl'); grid on;