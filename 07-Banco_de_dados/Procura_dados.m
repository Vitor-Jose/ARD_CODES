%% Junta os dados
% Jan /2022

% Formato do output
% dados(perfil,aoa,Re)

%% LIMPEZA
clear; close all; clc;

%% INPUT
AoA = 10; Re = 4e5;

%% ARQUIVOS
Path = 'Perfis_salvos';

File_data = 'bancodedados.mat';
load(File_data)

%% Exemplo: Encontrar cl crescentes
aux_AoA = dados.AoA == AoA;
aux_Re = dados.Re == Re;

[cl_cres,pos_cres] = sort(dados.cl(:,aux_AoA,aux_Re));
nome_cres = dados.nome(pos_cres);
fprintf('Perfis organizados em ordem crescente de cl:\n');
disp([nome_cres' dados.cl(pos_cres,aux_AoA,aux_Re)]);


%% Exemplo: Encontrar cl mais pr�ximo
cl_find = 1.8;
aux_AoA = dados.AoA == AoA;
aux_Re = dados.Re == Re;

[cl_prox,pos_prox] = sort(abs(dados.cl(:,aux_AoA,aux_Re)-cl_find));
nome_prox = dados.nome(pos_prox);
fprintf('Perfis organizados por proximidade de cl a %4.2f:\n',cl_find);
disp([nome_prox' dados.cl(pos_prox,aux_AoA,aux_Re)]);

%% Exemplo: Encontrar clmax mais pr�ximo
clmax_find = 3;
aux_Re = dados.Re == Re;

[clmax_prox,posmax_prox] = sort(abs(dados.clmax(:,1,aux_Re)-clmax_find));
nomemax_prox = dados.nome(posmax_prox);
fprintf('Perfis organizados por proximidade de clmax a %4.2f:\n',clmax_find);
disp([nomemax_prox' dados.clmax(posmax_prox,1,aux_Re) dados.a_clmax(posmax_prox,1,aux_Re)]);

% Sugest�o, fazer criar planilha no excel
%  Salva dados 
NomePerfil = nome_cres';
Coef_cl = dados.cl(pos_cres,aux_AoA,aux_Re);
Coef_cd = dados.cd(pos_cres,aux_AoA,aux_Re);
Coef_cm = dados.cm(pos_cres,aux_AoA,aux_Re);
Coef_clmax = dados.clmax(pos_cres,aux_Re);
Alpha_clmax = dados.a_clmax(pos_cres,aux_Re);
Coef_clmin = dados.clmin(pos_cres,aux_Re);
Alpha_clmin = dados.a_clmin(pos_cres,aux_Re);
Coef_cla = dados.cla(pos_cres,aux_Re);

T = table(NomePerfil,Coef_cl,Coef_cd,Coef_cm,Coef_clmax,Alpha_clmax,Coef_clmin,Alpha_clmin,Coef_cla);          % Forma tabela com os resultados (Re espec�fico)
filename = 'Banco.xlsx';                      % Nome do arquivo em Excel que ser� salvo
writetable(T,filename,'Sheet',1,'Range','B2')   % Escreve arquivo do Excel

%% Sugest�es
% 1 - Salvar uma planilha no excel
    % Criar primeiro a tabela organizada dentro do matlab usado os
    % vetores,e depois transformar essa tabela pra .xls
% 2 - Rodarem os novos perfis no gera curvas e conferir os existentes