function [nomes] = Procura_function(AoA,Re,File_data)
%% Junta os dados
% Fev/2022

%% ARQUIVOS
load(File_data)

%% Exemplo: Encontrar cl crescentes
aux_AoA = dados.AoA == AoA;
aux_Re = dados.Re == Re;

% [AoA2,aux_AoA]=min(abs(dados.AoA-AoA));
% [Re2,aux_Re]=min(abs(dados.Re-Re));
% 
% [cl_cres,pos_cres] = sort(dados.cl(:,aux_AoA,aux_Re));
% nome_cres = dados.nome(pos_cres);
% fprintf('Perfis organizados em ordem crescente de cl:\n');
% disp([nome_cres' dados.cl(pos_cres,aux_AoA,aux_Re)]);
% nomes = dados.filename(pos_cres);

%% CLmax
[clmax_prox,posmax_prox] = sort(dados.clmax(:,1,aux_Re));
nomemax_prox = dados.nome(posmax_prox);
fprintf('Perfis organizados por proximidade de clmax:');
disp([nomemax_prox' dados.clmax(posmax_prox,1,aux_Re) dados.a_clmax(posmax_prox,1,aux_Re)]);
nomes = dados.filename(posmax_prox);