%% Atividade 05 - Exerc�cio 1
clear; close all; clc;

% SetPaths

Missao = Mission;

%% Airplane 1
Missao.Airplane(1).LiftingSurfaces.IsMain = 1;
Missao.Airplane(1).LiftingSurfaces.label = 'Asa Principal';
Missao.Airplane(1).LiftingSurfaces.Geo.Yb = [0 0.8 1.25];
Missao.Airplane(1).LiftingSurfaces.Geo.C = [0.36 0.36 0.22];
Missao.Airplane(1).LiftingSurfaces.Geo.TwistY = [0 0 0];
Missao.Airplane(1).LiftingSurfaces(1).Geo.Incidence = 0;
perfil(1) = ImportObj('BA7_top10.mat');
perfil(2) = ImportObj('WTU_01.mat');
Missao.Airplane(1).LiftingSurfaces(1).Aero.Airfoils.Data = [perfil(1) perfil(1) perfil(2)];

Missao.Airplane(1).LiftingSurfaces(2) = LiftingSurface('EH');
Missao.Airplane(1).LiftingSurfaces(2).IsMain = 0;
Missao.Airplane(1).LiftingSurfaces(2).Geo.Yb = [0 0.4];
Missao.Airplane(1).LiftingSurfaces(2).Geo.C = [0.19 0.19];
Missao.Airplane(1).LiftingSurfaces(2).Geo.Incidence = 0;
Missao.Airplane(1).LiftingSurfaces(2).Geo.pos.x = 0.8;
Missao.Airplane(1).LiftingSurfaces(2).Geo.pos.y = 0;
Missao.Airplane(1).LiftingSurfaces(2).Geo.pos.z = 0.4;
perfil(3) = ImportObj('NACA0012.mat');
Missao.Airplane(1).LiftingSurfaces(2).Aero.Airfoils.Data = [perfil(3) perfil(3)];

Missao.Airplane(1).UpdateGeo;
% Missao.Airplane(1).PlotGeo('-airfoils');

%% Malha

Missao.Airplane(1).LiftingSurfaces(1).Aero.Mesh.Ny = [12 12];
Missao.Airplane(1).LiftingSurfaces(1).Aero.Mesh.TypeY = [1 3];

Missao.Airplane(1).LiftingSurfaces(2).Aero.Mesh.Ny = [10];
Missao.Airplane(1).LiftingSurfaces(2).Aero.Mesh.TypeY = [3];

Missao.Airplane(1).UpdateAeroMesh;
% Missao.Airplane(1).PlotMesh;

%% Airplane 2
Missao.Airplane(2).LiftingSurfaces.IsMain = 1;
Missao.Airplane(2).LiftingSurfaces.label = 'Asa Principal';
Missao.Airplane(2).LiftingSurfaces.Geo.Yb = [0 0.8 1.25];
Missao.Airplane(2).LiftingSurfaces.Geo.C = [0.36 0.36 0.22];
Missao.Airplane(2).LiftingSurfaces.Geo.TwistY = [0 -1 -3];
Missao.Airplane(2).LiftingSurfaces(1).Geo.Incidence = 1;
perfil(4) = ImportObj('CLARKY.mat');
Missao.Airplane(2).LiftingSurfaces(1).Aero.Airfoils.Data = [perfil(1) perfil(1) perfil(4)];

Missao.Airplane(2).LiftingSurfaces(2) = LiftingSurface('EH');
Missao.Airplane(2).LiftingSurfaces(2).IsMain = 0;
Missao.Airplane(2).LiftingSurfaces(2).Geo.Yb = [0 0.4];
Missao.Airplane(2).LiftingSurfaces(2).Geo.C = [0.19 0.19];
Missao.Airplane(2).LiftingSurfaces(2).Geo.Incidence = 0;
Missao.Airplane(2).LiftingSurfaces(2).Geo.pos.x = 0.8;
Missao.Airplane(2).LiftingSurfaces(2).Geo.pos.y = 0;
Missao.Airplane(2).LiftingSurfaces(2).Geo.pos.z = 0.4;
Missao.Airplane(2).LiftingSurfaces(2).Aero.Airfoils.Data = [perfil(3) perfil(3)];

Missao.Airplane(2).UpdateGeo;
% Missao.Airplane(2).PlotGeo('-airfoils');

%% Malha

Missao.Airplane(2).LiftingSurfaces(1).Aero.Mesh.Ny = [12 12];
Missao.Airplane(2).LiftingSurfaces(1).Aero.Mesh.TypeY = [1 3];

Missao.Airplane(2).LiftingSurfaces(2).Aero.Mesh.Ny = [10];
Missao.Airplane(2).LiftingSurfaces(2).Aero.Mesh.TypeY = [3];

Missao.Airplane(2).UpdateAeroMesh;
% Missao.Airplane(2).PlotMesh;

%% Condi��es de Voo

Missao.FltCond(1).Voo = 12;
Missao.FltCond(1).rho = 1.225;
Missao.FltCond(1).alpha = 15;

Missao.FltCond(2).Voo = 16;
Missao.FltCond(2).rho = 1.225;
Missao.FltCond(2).alpha = 5;

%% SALVAR
save('Missao.mat','Missao')