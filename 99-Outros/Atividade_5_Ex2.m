%% Atividade 05 - Exerc�cio 2
clear; close all; clc;

load('Missao.mat')

NL_VLM(Missao.Airplane(1), Missao.FltCond(1),'-append');
NL_VLM(Missao.Airplane(1), Missao.FltCond(2),'-append');

NL_VLM(Missao.Airplane(2), Missao.FltCond(1),'-append');
NL_VLM(Missao.Airplane(2), Missao.FltCond(2),'-append');

figure(1); hold on;
plot(Missao.Airplane(1).LiftingSurfaces(1).Aero.Mesh.Yb,Missao.Airplane(1).LiftingSurfaces(1).Aero.Loads(1).cl); 
plot(Missao.Airplane(2).LiftingSurfaces(1).Aero.Mesh.Yb,Missao.Airplane(2).LiftingSurfaces(1).Aero.Loads(1).cl);
title('Condicao 1')
legend('Aeronave 1','Aeronave 2')

figure(2); hold on;
plot(Missao.Airplane(1).LiftingSurfaces(1).Aero.Mesh.Yb,Missao.Airplane(1).LiftingSurfaces(1).Aero.Loads(2).cl); 
plot(Missao.Airplane(2).LiftingSurfaces(1).Aero.Mesh.Yb,Missao.Airplane(2).LiftingSurfaces(1).Aero.Loads(2).cl);
title('Condicao 2')
legend('Aeronave 1','Aeronave 2')
 
