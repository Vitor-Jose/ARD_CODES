%% Atividade 05 - Exerc�cio 3
clear; close all; clc;

load('Missao.mat')

iteracoes = [5 10 25];

for i = 1:length(iteracoes)
    fprintf('%d itera��es m�ximas:\n',iteracoes(i))
    NL_VLM(Missao.Airplane(1), Missao.FltCond(1),'-dispstat','-itermax',iteracoes(i));
    disp(Missao.Airplane(1).Aero.Coeffs.CL)
end

 
