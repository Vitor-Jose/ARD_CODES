%% 2
% clear;close all; clc

% SetPaths;

% load('Mission');

% mis.Airplane(1).LiftingSurfaces(1).Geo.Yb;;
% mis.Airplane(2).LiftingSurfaces(1).Geo.Yb;;
% 
% 
% mis.Airplane(1).LiftingSurfaces(1).Aero.Loads(1).cl;
% mis.Airplane(1).LiftingSurfaces(1).Aero.Loads(2).cl;
% 
% 
% mis.Airplane(2).LiftingSurfaces(1).Aero.Loads(1).cl;
% mis.Airplane(2).LiftingSurfaces(1).Aero.Loads(2).cl;

figure(1)
plot(mis.Airplane(1).LiftingSurfaces(1).Aero.Mesh.Yb,mis.Airplane(1).LiftingSurfaces(1).Aero.Loads(1).cl)
hold on
plot(mis.Airplane(1).LiftingSurfaces(1).Aero.Mesh.Yb,mis.Airplane(1).LiftingSurfaces(1).Aero.Loads(2).cl)
grid on
xlabel('Envergadura')
ylabel('Cl')
legend('Distribuicao para condicao 1','Distribuicao para condicao 2','location','south')
title('Distribuicao Cl por Yb para a aeronave 1')

figure(2)
plot(mis.Airplane(2).LiftingSurfaces(1).Aero.Mesh.Yb,mis.Airplane(2).LiftingSurfaces(1).Aero.Loads(1).cl)
hold on
plot(mis.Airplane(2).LiftingSurfaces(1).Aero.Mesh.Yb,mis.Airplane(2).LiftingSurfaces(1).Aero.Loads(2).cl)
grid on
xlabel('Envergadura')
ylabel('Cl')
legend('Distribuicao para condicao 1','Distribuicao para condicao 2','location','south')
title('Distribuicao Cl por Yb para a aeronave 2')

figure(3)
plot(mis.Airplane(1).LiftingSurfaces(1).Aero.Mesh.Yb,mis.Airplane(1).LiftingSurfaces(1).Aero.Loads(1).cl)
hold on
plot(mis.Airplane(1).LiftingSurfaces(1).Aero.Mesh.Yb,mis.Airplane(1).LiftingSurfaces(1).Aero.Loads(2).cl)

plot(mis.Airplane(2).LiftingSurfaces(1).Aero.Mesh.Yb,mis.Airplane(2).LiftingSurfaces(1).Aero.Loads(1).cl,':','LineWidth',1.5)
plot(mis.Airplane(2).LiftingSurfaces(1).Aero.Mesh.Yb,mis.Airplane(2).LiftingSurfaces(1).Aero.Loads(2).cl,':','LineWidth',1.5)
grid on
xlabel('Envergadura')
ylabel('Cl')
legend('Distribuicao para condicao 1 aero 1','Distribuicao para condicao 2 aero 1','Distribuicao para condicao 1 aero 2','Distribuicao para condicao 2 aero 2','location','south')
title('Comparativo entre as duas aeronaves')

%% 3

% clear;close all; clc

% SetPaths;

% load('Mission');

analise1 = NL_VLM(mis.Airplane(1),mis.FltCond(1),'-dispstat','-itermax',1);
CL1 = mis.Airplane(1).Aero.Coeffs.CL

analise2 = NL_VLM(mis.Airplane(1),mis.FltCond(1),'-dispstat','-itermax',5);
CL2 = mis.Airplane(1).Aero.Coeffs.CL

analise3 = NL_VLM(mis.Airplane(1),mis.FltCond(1),'-dispstat','-itermax',10);
CL3 = mis.Airplane(1).Aero.Coeffs.CL

analise4 = NL_VLM(mis.Airplane(1),mis.FltCond(1),'-dispstat','-itermax',25);
CL4 = mis.Airplane(1).Aero.Coeffs.CL

%%

