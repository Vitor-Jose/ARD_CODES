clear; close all; clc;
load('mission.mat');


SetPaths
funci = Airplane; 

funci.LiftingSurfaces.IsMain=1;
funci.LiftingSurfaces.label='asa'
funci.LiftingSurfaces.Geo.Yb=[0 0.8 1.25];
funci.LiftingSurfaces.Geo.C=[0.36 0.36 0.28];
perfil(10)=ImportObj('BA7_top10.mat');
perfil(11)=ImportObj('WTU_01.mat');
funci.LiftingSurfaces(1).Geo.TwistY=[0 0 0];
funci.LiftingSurfaces(1).Aero.Airfoils.Data=[perfil(10) perfil(10) perfil(11)];


funci.LiftingSurfaces(2) = LiftingSurface('Emp Horizontal');
funci.LiftingSurfaces(2).IsMain=0;
funci.LiftingSurfaces(2).IsMain = 0;
funci.LiftingSurfaces(2).Geo.Yb = [0 0.4];
funci.LiftingSurfaces(2).Geo.C = [0.19 0.19];
perfil(9)=ImportObj('NACA0012.mat');
funci.LiftingSurfaces(2).Geo.pos.x = 0.8; 
funci.LiftingSurfaces(2).Geo.pos.y = 0;
funci.LiftingSurfaces(2).Geo.pos.z = 0.4;
funci.LiftingSurfaces(2).Aero.Airfoils.Data=[perfil(9) perfil(9)];

funci.LiftingSurfaces(1).Aero.Mesh.Ny = [10 8 7];
funci.LiftingSurfaces(1).Aero.Mesh.TypeY = [1 2 3]; 
funci.LiftingSurfaces(2).Aero.Mesh.Ny = [8];
funci.LiftingSurfaces(2).Aero.Mesh.TypeY = [2];










%segunda aeronave
mission2 =Airplane;

mission2.LiftingSurfaces.IsMain=1;
mission2.LiftingSurfaces.label='asa 02'
mission2.LiftingSurfaces.Geo.Yb=[0 0.8 1.25];
mission2.LiftingSurfaces.Geo.C=[0.36 0.36 0.28];
perfil(7)=ImportObj('BA7_top10.mat');
perfil(9)=ImportObj('CLARKY.mat');
mission2.LiftingSurfaces(1).Geo.TwistY=[0 -1 -3]; 
mission2.LiftingSurfaces(1).Aero.Airfoils.Data=[perfil(7) perfil(7) perfil(9)];


mission2.LiftingSurfaces(2) = LiftingSurface('Emp Horizontal');
mission2.LiftingSurfaces(2).IsMain=0;
mission2.LiftingSurfaces(2).IsMain = 0;
mission2.LiftingSurfaces(2).Geo.Yb = [0 0.4];
mission2.LiftingSurfaces(2).Geo.C = [0.19 0.19];
perfil(9)=ImportObj('NACA0012.mat');
mission2.LiftingSurfaces(2).Geo.pos.x = 0.8;
mission2.LiftingSurfaces(2).Geo.pos.y = 0;
mission2.LiftingSurfaces(2).Geo.pos.z = 0.4;
mission2.LiftingSurfaces(2).Aero.Airfoils.Data=[perfil(9) perfil(9)];

funci.UpdateGeo;
mission2.UpdateGeo;
% funci.LiftingSurfaces(1).Aero.Mesh.Ny = [10 8];
% funci.LiftingSurfaces(1).Aero.Mesh.TypeY = [1 2];
% 
% funci.LiftingSurfaces(2).Aero.Mesh.Ny=[5];
% funci.LiftingSurfaces(2).Aero.Mesh.TypeY=[2];
% 
% funci.LiftingSurfaces(1).Aero.Mesh.Ny = [10 8 ];
% funci.LiftingSurfaces(1).Aero.Mesh.TypeY = [1 2 ];
% 
% funci.LiftingSurfaces(2).Aero.Mesh.Ny=[5];
% funci.LiftingSurfaces(2).Aero.Mesh.TypeY=[2];
% %--------------------------------------------------
% mission2.LiftingSurfaces(1).Aero.Mesh.Ny = [10 8];
% mission2.LiftingSurfaces(1).Aero.Mesh.TypeY = [1 2];
% 
% mission2.LiftingSurfaces(2).Aero.Mesh.Ny=[5];
% mission2.LiftingSurfaces(2).Aero.Mesh.TypeY=[2];
% 
% mission2.LiftingSurfaces(1).Aero.Mesh.Ny = [10 8 ];
% mission2.LiftingSurfaces(1).Aero.Mesh.TypeY = [1 2 ];
% 
% mission2.LiftingSurfaces(2).Aero.Mesh.Ny=[5];
% mission2.LiftingSurfaces(2).Aero.Mesh.TypeY=[2];



%funci.UpdateAeroMesh;
%funci.PlotMesh;




%mission2.UpdateAeroMesh;
%mission2.PlotMesh;



%condiceos de voo
condicao2 = FlightConditions;
condicao2.Voo = 16;
condicao2.rho = 1.225;
condicao2.alpha=5;
condicao1=FlightConditions;
condicao1.Voo = 12;
condicao1.rho = 1.225;
condicao1.alpha=15;





RESULT1 = NL_VLM(mission.Airplane(1), mission.fltcond(1));

figure(1)
plot(mission.Airplane(1).LiftingSurfaces(1).Aero.Mesh.Yb,mission.Airplane(1).LiftingSurfaces(1).Aero.Loads.cl)
hold on




RESULT2 = NL_VLM(mission.Airplane(1), mission.FltCond(2));

plot(mission.Airplane(1).LiftingSurfaces(1).Aero.Mesh.Yb,mission.Airplane(1).LiftingSurfaces(1).Aero.Loads.cl);





 












