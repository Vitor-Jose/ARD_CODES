% Corrije
clear; close all; clc;

read_file = 'Amostra2.mat';
save_file = 'Amostra4.mat';

load(read_file);

for aux_S = 1:length(prm.S)
    for aux_P = 1:length(prm.perfil)
        aux_err = dados(aux_S,aux_P).CL > 2.5;
        dados(aux_S,aux_P).CL(aux_err) = NaN;
        dados(aux_S,aux_P).CD(aux_err) = NaN;
        dados(aux_S,aux_P).CM(aux_err) = NaN;  
    end
end

save(save_file,'dados','prm')