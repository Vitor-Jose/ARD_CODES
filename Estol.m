%% C�lculo do estol por meio do m�todo da se��o cr�tica

%% LIMPEZA
clear; close all; clc;

%% INICIALIZACAO
SetPaths
load('03-MACACO_Aeronaves/plane_2022_2n.mat');
plane = obj;

% Caso queira mudar alguma caracter�stica da aeronave (geometria ou malha)

% plane.LiftingSurfaces(3).Geo.Incidence = 0;
% plane.UpdateGeo;
% plane.UpdateAeroMesh;

%% INPUT
correcao = -0.005;        % Valor de Cl como margem de seguran�a (associado a incerteza do XFOIL)
superficies = [1];    % Quais as Lifting Surfaces analisadas no estol
n_super = length(superficies);

caso = 'min';           % Encontrar o 'max' ou o 'min'
                        % Caso max, passo e correcao deve ser positivo
                        % Caso min, passo e correcao deve ser negativo
                        
AoA.inicial = 0;
AoA.final   = -10;
AoA.passo   = -1;
AoA.fila    = AoA.inicial:AoA.passo:AoA.final;

%% CONDI��ES DE VOO
cond = FlightConditions;
cond.Voo= 12;
cond.rho= 1.1161;

%% Cl_max de cada perfil
n_Re = zeros(n_super,1);
Re   = zeros(n_super,3);
pos_Re = zeros(n_super,3);
clmax  = zeros(n_super,3);
clmin  = zeros(n_super,3);
n_limite =  zeros(n_super,3);
limite = zeros(n_super,60);

for i = 1:n_super
    n_Re(i) = length(plane.LiftingSurfaces(superficies(i)).Geo.C);
    Re(i,1:n_Re(i)) = (cond.Voo.*cond.rho.*plane.LiftingSurfaces(superficies(i)).Geo.C)./cond.mi;
    for j = 1:n_Re(i)
        pos_Re(i,j) = find([plane.LiftingSurfaces(superficies(i)).Aero.Airfoils.Data(j).coef.Re]>=Re(i,j),1,'first');
        clmax(i,j)  = plane.LiftingSurfaces(superficies(i)).Aero.Airfoils.Data(j).coef(pos_Re(i,j)).clmax;
        clmax(i,j)  = clmax(i,j) - correcao;
        
        clmin(i,j)  = plane.LiftingSurfaces(superficies(i)).Aero.Airfoils.Data(j).coef(pos_Re(i,j)).clmin;
        clmin(i,j)  = clmin(i,j) - correcao;      
    end
    
    n_limite(i) = length(plane.LiftingSurfaces(i).Aero.Mesh.Yb);
    switch caso
        case 'max'
            limite(i,.5*n_limite(i)+1:n_limite(i)) = interp1(plane.LiftingSurfaces(i).Geo.Yb,clmax(i,1:n_Re(i)),plane.LiftingSurfaces(i).Aero.Mesh.Yb(0.5*end+1:end));
        case 'min'
            limite(i,.5*n_limite(i)+1:n_limite(i)) = interp1(plane.LiftingSurfaces(i).Geo.Yb,clmin(i,1:n_Re(i)),plane.LiftingSurfaces(i).Aero.Mesh.Yb(0.5*end+1:end));
    end
    limite(i,1:.5*n_limite(i)) = flip(limite(i,.5*n_limite(i)+1:n_limite(i)));
    
    subplot(n_super,1,i)
    plot(plane.LiftingSurfaces(i).Aero.Mesh.Yb,limite(i,1:n_limite(i)));
    grid on; hold on;
end

%% LOOP
estolou(1:n_super) = false;
alfa_estol(1:n_super) = AoA.final;

continuar = true; k = 1;
while continuar 
     cond.alpha = AoA.fila(k);
    
%     cond.alpha = 10;
%     plane.LiftingSurfaces(3).Geo.Incidence = AoA.fila(k);
%     plane.UpdateGeo;
%     plane.UpdateAeroMesh;

    plane.Aero.Settings.NLVLM.Cl_from_adjusted_2DCurvesEq = 1;
    NL_VLM(plane, cond,'-dispstat','-itermax',35);

    
    for i = 1:n_super
        diff = limite(i,1:n_limite(i)) - plane.LiftingSurfaces(i).Aero.Loads.cl;
        switch caso
            case 'max'
                if min(diff)<=0 && ~estolou(i)
                    estolou(i) = true; 
                    alfa_estol(i) = cond.alpha;
                    fprintf('Estol na asa %d com %4.1f\n',i,AoA.fila(k))

                    % Caso queira parar no primeiro estol, n�o continuar
                    % continuar = false
                    
                    % PLOT
                    subplot(n_super,1,i)
                    plot(plane.LiftingSurfaces(i).Aero.Mesh.Yb,plane.LiftingSurfaces(i).Aero.Loads.cl);
                end  
                
            case 'min'
                if max(diff)>=0 && ~estolou(i)
                    estolou(i) = true; 
                    alfa_estol(i) = cond.alpha;
                    fprintf('Estol na asa %d com %4.1f\n',i,AoA.fila(k))
                    
                    % Caso queira parar no primeiro estol, n�o continuar
                    % continuar = false
                    
                    % PLOT
                    subplot(n_super,1,i)
                    plot(plane.LiftingSurfaces(i).Aero.Mesh.Yb,plane.LiftingSurfaces(i).Aero.Loads.cl);
                end  
        end
        clearvars diff
    end
    
    k = k+1;
    
    if isempty(find(estolou == 0,1)) || k > length(AoA.fila) 
        continuar = false;
    end
end

%% teste para upar no git