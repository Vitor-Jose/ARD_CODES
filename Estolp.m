function [ard] = estolp(geo, flc, sim)
%ESTOL Summary of this function goes here
%   Calculo do alha estol (ard.alpha_estol), a deflexao maxima do profundor
%   (ard.deltae) e alguns parametros aerodinamicos da geometria

% INICIALIZACAO
sim.dist = 1; % Carrega distribui��o de alpha efetivo no VLManda
correct  = 2.75;  % Define quantos graus abaixo do estol ser� procurado
aoa  = 16:0.3:19.5;
flc.Voo  = 11.1;
a_estol = 20;

% Leva
aoa_len = length(aoa);
di = fix(aoa_len/sim.npro);
dr = rem(aoa_len,sim.npro);
loop1 = di;
loop2(1:di) = sim.npro;
if dr ~= 0
    loop1 = loop1+1;
    loop2(end+1) = dr;
end

% Atribui os perfis e carrega-os
perfil_1 = seleciona_perfil(geo.LiftingSurface.perfil(1,1));
perfil_2 = seleciona_perfil(geo.LiftingSurface.perfil(2,1));
p1 = ImportObj(perfil_1);
p2 = ImportObj(perfil_2);

Re1 = flc.Voo*geo.LiftingSurface.c(1,1)*flc.rho/1.812e-5;
Re2 = flc.Voo*geo.LiftingSurface.c(2,1)*flc.rho/1.812e-5;

pos1 = find([p1.coef.Re]>=Re1,1,'first');
pos2 = find([p2.coef.Re]>=Re2,1,'first');

alpha_clmax = [p1.coef(pos1).a_clmax p2.coef(pos2).a_clmax];
alpha_consi = alpha_clmax - correct;

parar = 0;

for j = 1:loop1
    for i = 1:loop2(j)
        flc.aoa(i) = aoa(sim.npro*(j-1)+i);
    end    
    valores = VLMandap(geo,flc,sim,0,'-LiftingSurfaces');    
    for i = 1:loop2(j)
        achou1 = find(valores(1).Loads(i).alpha_eff > alpha_consi(1),1);
        achou2 = find(valores(2).Loads(i).alpha_eff > alpha_consi(2),1);
        if ~isempty(achou1)
            a_estol = aoa(sim.npro*(j-1)+i);
            fprintf('Estol na asa inferior com %4.1f: %s \n',a_estol,perfil_1)
            parar = 1;
            break 
        elseif ~isempty(achou2)
            a_estol = aoa(sim.npro*(j-1)+i);
            fprintf('Estol na asa superior com %4.1f : %s \n',a_estol,perfil_2)
            parar = 1;
            break        
        end
    end
    if parar 
        break
    end
    clear flc.aoa;
end
ard.alpha_estol = a_estol;
sim.dist = 0;
end
