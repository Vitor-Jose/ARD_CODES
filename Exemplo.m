clear; close all; clc;

load('plane_2022_com_eh.mat')
aviao = obj;

perfil(1) = ImportObj('BT.mat');
aviao.LiftingSurfaces(1).Aero.Airfoils.Data = [perfil(1) perfil(1) perfil(1)];

aviao.UpdateGeo;
aviao.PlotGeo('-airfoils','-nimage',5);

%% - Malha

aviao.LiftingSurfaces(1).Aero.Mesh.Ny = [7 6];
aviao.LiftingSurfaces(1).Aero.Mesh.TypeY = [3 2];

aviao.LiftingSurfaces(2).Aero.Mesh.Ny = [4];
aviao.LiftingSurfaces(2).Aero.Mesh.TypeY = [2];

aviao.UpdateAeroMesh;
%aviao.PlotMesh;

%% - Condi��es de voo

condicao = FlightConditions;
condicao.Voo = 12;
aoa = -5:20;

%% - Solver

for j = 1:length(aoa)
    condicao.alpha = aoa(j);
    NL_VLM(aviao, condicao,'-append');

    CLplane(1,j) = aviao.Aero.Coeffs(j).CL;
    CDplane(1,j) = aviao.Aero.Coeffs(j).CD;

    for i = 1:2
        CDsurf(i,j) = aviao.LiftingSurfaces(i).Aero.Coeffs(j).CD;
        CLsurf(i,j) = aviao.LiftingSurfaces(i).Aero.Coeffs(j).CL;
    end
end

f1 = figure;
plot(aoa,CLsurf(1,:))
title('CL x \alpha (BT)')
xlabel('aoa [�]')
ylabel('CLsurf')
grid minor

f2 = figure;
plot(aoa,CDsurf(1,:))
title('CD x \alpha (BT)')
xlabel('\alpha [�]')
ylabel('CDsurf')
grid minor

f3 = figure;
plot(CDsurf(1,:),CLsurf(1,:))
title('CL x CD (BT)')
xlabel('CDsurf')
ylabel('CLsurf')
grid minor





