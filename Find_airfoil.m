%% Encontra perfil para asa
% Vitor Maffei - Fevereiro de 2022
clear; close all; clc; 
SetPaths

AoA         = [0:4:16];
V           = 10;
file_search = 'banco20222.mat';
S           = 0.8;

%% Carrega selecao
cd 07-Banco_de_dados
    [perfil_nome] = Procura_function(10,4e5,file_search);
cd ..

CL = zeros(length(perfil_nome),length(AoA));
CD = zeros(length(perfil_nome),length(AoA));
CM = zeros(length(perfil_nome),length(AoA));
for aa = 1:length(AoA)
    %% Condicoes
    condicao        = FlightConditions;
    condicao.alpha  = AoA(aa);
    condicao.Voo    = V;
    condicao.rho    = 1.1161;

    %% Geometria
    aviao = Airplane;
    aviao.LiftingSurfaces.IsMain		= 1;
    aviao.LiftingSurfaces.label 		= 'Asa principal';
    aviao.LiftingSurfaces.Geo.Yb 		= [0 1];
    aviao.LiftingSurfaces.Geo.C 		= [S/2 S/2];
    aviao.LiftingSurfaces.Geo.Dihedral  = [0 0];
    aviao.LiftingSurfaces.Geo.Sweep 	= [0 0];
    aviao.LiftingSurfaces.Geo.TwistY 	= [0 0 0];
    aviao.LiftingSurfaces.Geo.Incidence = 0;
    aviao.LiftingSurfaces(1).Geo.pos.x  = 0;
    aviao.LiftingSurfaces(1).Geo.pos.y  = 0;
    aviao.LiftingSurfaces(1).Geo.pos.z  = 0;

    perfil(1) = ImportObj(perfil_nome(1));
    aviao.LiftingSurfaces(1).Aero.Airfoils.Data = [perfil(1) perfil(1)];

    aviao.LiftingSurfaces(1).Aero.Mesh.Ny = 30;
    aviao.LiftingSurfaces(1).Aero.Mesh.TypeY = 3;

    aviao.UpdateGeo;
    aviao.UpdateAeroMesh;
    %% Loop
    cont = 1;

    for i = 1:length(perfil_nome)
        perfil(1) = ImportObj(perfil_nome(i));
        aviao.LiftingSurfaces(1).Aero.Airfoils.Data = [perfil(1) perfil(1)];
        NL_VLM(aviao, condicao);%,'-dispstat','-itermax',35);
        CL(i,aa) = aviao.Aero.Coeffs.CL;
        CD(i,aa) = aviao.Aero.Coeffs.CD;
        CM(i,aa) = aviao.Aero.Coeffs.Cm25;

        fprintf('%d: %14s - CL verdadeiro: %4.2f\n',cont,perfil_nome(i),CL(i,aa));
        cont = cont+1;
    end
end
y = AoA; x = 1:length(perfil_nome);
%[~,pn] = sort(CL(:,1)+CL(:,2)+CL(:,3)+CL(:,4)+CL(:,end));
[~,pn] = sort(CL(:,end));
CLnovo = CL(pn,:);
CDnovo = CD(pn,:);
CMnovo = CM(pn,:);
perfil_novo = perfil_nome(pn);
disp([x' perfil_novo'])
%% Plots - Bidimensional
% plot(CD,CL,'o'); grid on;
% text(CD,CL,cellstr(perfil_nome));

%% Plots - Tridimensional
[X,Y] = meshgrid(1:length(perfil_nome),AoA);
% mesh(X,Y,CL','FaceColor','interp')
% figure
% mesh(X,Y,CD','FaceColor','interp')
% figure
% mesh(X,Y,CL'./CD','FaceColor','interp')

% === CLnovo ===
figure; surf(X,Y,CLnovo','FaceColor','interp');
zlabel('C_L'); xlabel('Perfis'); ylabel('AoA'); hold on;
text(1:length(perfil_nome),AoA(1)*ones(1,length(perfil_nome)),CLnovo(:,1),cellstr(perfil_nome(pn)))

% Teste ajuste

 p00 =      0.4985;  %(0.4193, 0.5777)
 p10 =    0.007595;  %(-0.0002652, 0.01546)
 p01 =     0.09071;  %(0.07782, 0.1036)
 p20 =   0.0001556;  %(-4.573e-05, 0.000357)
 p11 =    0.000547;  %(0.000222, 0.000872)
 p02 =   -0.001784;  %(-0.002518, -0.00105)
% Ajuste = p00 + p10.*X + p01.*Y + p20.*X.^2 + p11.*X.*Y + p02.*Y.^2;
% surf(X,Y,Ajuste,'FaceColor','interp')
%  CLp00 =  p00;
%  CLp10 =  p10;
%  CLp01 =  p01;
%  CLp20 =  p20;
%  CLp11 =  p11;
%  CLp02 =  p02;

 
 % colormap(hsv);
 % figure
 % surf(X,Y,Ajuste,'FaceColor','interp')
 
% === CDnovo ===
figure; surf(X,Y,CDnovo','FaceColor','interp');
zlabel('C_D'); xlabel('Perfis'); ylabel('AoA'); hold on;
text(1:length(perfil_nome),AoA(1)*ones(1,length(perfil_nome)),CDnovo(:,1),cellstr(perfil_nome(pn)))

p00 =      0.0326;
p10 =   -0.000597;
p01 =    0.004338;
p20 =   7.748e-05;
p11 =   0.0001083;
p02 =   0.0002707;

% CDp00 =  p00;
% CDp10 =  p10;
% CDp01 =  p01;
% CDp20 =  p20;
% CDp11 =  p11;
% CDp02 =  p02;
%Ajuste2 = p00 + p10.*X + p01.*Y + p20.*X.^2 + p11.*X.*Y + p02.*Y.^2;
%surf(X,Y,Ajuste2,'FaceColor','interp')

% figure
% surf(X,Y,Ajuste2,'FaceColor','interp')

% === CL/CDnovo ===
figure; surf(X,Y,CLnovo'./CDnovo(:,1)','FaceColor','interp');
zlabel('C_L/C_D'); xlabel('Perfis'); ylabel('AoA'); hold on;
text(1:length(perfil_nome),AoA(1)*ones(1,length(perfil_nome)),CLnovo(:,1)./CDnovo(:,1),cellstr(perfil_nome(pn)))
% AA = Ajuste./Ajuste2;
% figure; surf(X,Y,AA,'FaceColor','interp');

% === CMnovo ===
figure; mesh(X,Y,CMnovo','FaceColor','interp');
text(1:length(perfil_nome),AoA(1)*ones(1,length(perfil_nome)),CMnovo(:,1),cellstr(perfil_nome(pn)))

save(['S_' num2str(S) '_V_' num2str(V)])