%% Encontra perfil para asa
% Vitor Maffei - Fevereiro de 2022
clear; close all; clc; 
SetPaths

AoA         = [-2:4:8 10:2:20];
V           = 10:2.5:15;
file_search = 'banco20222.mat';
S           = 0.6:0.1:1.4;

%% Carrega selecao
cd 07-Banco_de_dados
    [perfil_nome] = Procura_function(10,4e5,file_search);
cd ..

for aux_S = 1:length(S)
    
    % Geometria
    aviao = Airplane;
    aviao.LiftingSurfaces.IsMain		= 1;
    aviao.LiftingSurfaces.label 		= 'Asa principal';
    aviao.LiftingSurfaces.Geo.Yb 		= [0 0.95];
    aviao.LiftingSurfaces.Geo.C 		= [S(aux_S)/2 S(aux_S)/2];
    aviao.LiftingSurfaces.Geo.Dihedral  = [0 0];
    aviao.LiftingSurfaces.Geo.Sweep 	= [0 0];
    aviao.LiftingSurfaces.Geo.TwistY 	= [0 0 0];
    aviao.LiftingSurfaces.Geo.Incidence = 0;
    aviao.LiftingSurfaces(1).Geo.pos.x  = 0;
    aviao.LiftingSurfaces(1).Geo.pos.y  = 0;
    aviao.LiftingSurfaces(1).Geo.pos.z  = 0;
    aviao.UpdateGeo;
    
    AR = aviao.LiftingSurfaces(1).Geo.AR;
    % Malha
    aviao.LiftingSurfaces(1).Aero.Mesh.Ny = 30;
    aviao.LiftingSurfaces(1).Aero.Mesh.TypeY = 3;
    aviao.UpdateAeroMesh;
    
    for aux_P = 1:length(perfil_nome)
        dados(aux_S,aux_P).CL = zeros(length(V),length(AoA));
        dados(aux_S,aux_P).CD = zeros(length(V),length(AoA));
        dados(aux_S,aux_P).CM = zeros(length(V),length(AoA));
        dados(aux_S,aux_P).CDi = zeros(length(V),length(AoA));
        perfil = ImportObj(perfil_nome(aux_P));
        aviao.LiftingSurfaces(1).Aero.Airfoils.Data = [perfil perfil];
        
        for aux_V = 1:length(V)
            for aux_A = 1:length(AoA)
                % Condicoes
                condicao        = FlightConditions;
                condicao.alpha  = AoA(aux_A);
                condicao.Voo    = V(aux_V);
                condicao.rho    = 1.1161;

                NL_VLM(aviao, condicao,'-dispstat','-itermax',35);
                
                dados(aux_S,aux_P).CL(aux_V,aux_A) = aviao.Aero.Coeffs.CL;
                dados(aux_S,aux_P).CD(aux_V,aux_A) = aviao.Aero.Coeffs.CD;
                dados(aux_S,aux_P).CDi(aux_V,aux_A) = aviao.Aero.Coeffs.CDi;
                dados(aux_S,aux_P).e(aux_V,aux_A) = (aviao.Aero.Coeffs.CL^2)/(aviao.Aero.Coeffs.CDi*pi*AR);  
                dados(aux_S,aux_P).CM(aux_V,aux_A) = aviao.Aero.Coeffs.Cm25;

                fprintf('%3.1f: %14s - CL: %4.2f\n',AoA(aux_A),perfil_nome(aux_P),dados(aux_S,aux_P).CL(aux_V,aux_A));
            end
        end
    end
end

prm.AoA = AoA;
prm.V = V;
prm.perfil = perfil_nome;
prm.S = S;

save('Amostra2.mat','dados','prm')
