%% Plota as distribui��es nas asa
% Vitor Jos� - nov. 2021

%% LIMPEZA
clear; clc;
SetPaths

%% PAR�METROS INICIAS
% load('03-MACACO_Aeronaves/plane_2022_aileron.mat');    % Carrega a aeronave
load('03-MACACO_Aeronaves/plane_2022.mat');    % Carrega a aeronave
aviao = obj;        % Realoca para avi�o

% p2 = ImportObj('BT825_15_minus15.mat');
% p1 = ImportObj('E95_35_minus15.mat');
% aviao.LiftingSurfaces(1).Aero.Airfoils.Data(4) = p2;
% aviao.LiftingSurfaces(1).Aero.Airfoils.Data(3) = p1;
aviao.Aero.Settings.NLVLM.Cl_from_adjusted_2DCurvesEq = 1; 

AoA =  [0:2:10];
V = [12];
    
for conta_V = 1:length(AoA)
    %% MODIFICAR GEOMETRIA
    % aviao.UpdateGeo;
    % aviao.UpdateAeroMesh;

    %% CONDI��ES DE VOO
    condicao = FlightConditions;
    condicao.alpha = AoA(conta_V);
    condicao.Voo = V;
    condicao.rho = 1.1161;


    %% SOLVER
    NL_VLM(aviao, condicao,'-dispstat','-itermax',30);
    CLplane(conta_V) = aviao.Aero.Coeffs.CL;
    CDplane(conta_V) = aviao.Aero.Coeffs.CD;
    CD0plane(conta_V) = aviao.Aero.Coeffs.CD0;
    CDiplane(conta_V) = aviao.Aero.Coeffs.CDi;
    Cmasa(conta_V)   = aviao.LiftingSurfaces(1).Aero.Coeffs.Cm25;

    CDsurf(conta_V) = aviao.LiftingSurfaces(1).Aero.Coeffs.CD;
    CLsurf(conta_V) = aviao.LiftingSurfaces(1).Aero.Coeffs.CL;
    Cmsurf(conta_V) = aviao.LiftingSurfaces(1).Aero.Coeffs.Cm25;
end

load('solo')
dCL = CLsurf1-CLsurf';
dCD = CDsurf1-CDsurf';
dCm = Cmsurf1-Cmsurf';
altura = 0.3:0.2:1.5;
[X,Y] = meshgrid(altura,aoa);
L = interp2(X,Y,dCL,1,10) %altura, aoa
D = interp2(X,Y,dCD,1,10) %altura, aoa
M = interp2(X,Y,dCm,1,10) %altura, aoa

plot(aoa,CLsurf,aoa,CLsurf1(:,1),aoa,CLsurf1(:,2),aoa,CLsurf1(:,3),aoa,CLsurf1(:,4),aoa,CLsurf1(:,5))
% figure(1)
% plot(V,CLplane)
% xlabel('V')
% ylabel('CL')
% hold on
% figure(2)
% plot(V,CDplane)
% xlabel('V')
% ylabel('CD')
% hold on
% figure(3)
% plot(V,CD0plane)
% xlabel('V')
% ylabel('CD0')
% hold on
% figure(4)
% plot(V,CDiplane)
% xlabel('V')
% ylabel('CDi')
% hold on