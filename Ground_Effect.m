clear; close all; clc

j =0;
solo = [3:23];
for c = solo
load('asaefeitosolo.mat') 
aviao = obj; 

% aviao.LiftingSurfaces.IsMain = 1;
% aviao.LiftingSurfaces.label = 'Asa Principal';
% aviao.LiftingSurfaces.Geo.Yb = geo.LiftingSurface(1).b(1,:);
aviao.LiftingSurfaces(1).Geo.Incidence = c;
% aviao.LiftingSurfaces.Geo.C = geo.LiftingSurface.c(1,:);
% perfil(1) = ImportObj('BA7_top10_WTU_01b.mat');
% perfil(2) = ImportObj('ETW_02_9_5b.mat');
% % perfil(3) = ImportObj('MHL90.mat');
% % perfil(4) = ImportObj('mh84.mat');
% aviao.LiftingSurfaces(1).Aero.Airfoils.Data = [perfil(2) perfil(2) perfil(1)];

% aviao.LiftingSurfaces(2) = LiftingSurface('EH');
% aviao.LiftingSurfaces(2).IsMain = 0;
% aviao.LiftingSurfaces(2).Geo.Yb = geo.LiftingSurface.b(2,:);
% aviao.LiftingSurfaces(2).Geo.C = geo.LiftingSurface.c(2,:);
aviao.LiftingSurfaces(2).Geo.Incidence = -c;
% aviao.LiftingSurfaces(2).Geo.pos.x = geo.LiftingSurface.pos(2,1);
% aviao.LiftingSurfaces(2).Geo.pos.y = geo.LiftingSurface.pos(2,2);
% aviao.LiftingSurfaces(2).Geo.pos.z = geo.LiftingSurface.pos(2,3);
% perfil(5) = ImportObj('prof2010.mat');
% aviao.LiftingSurfaces(2).Aero.Airfoils.Data = [perfil(5) perfil(5)];

aviao.UpdateGeo;
% aviao.PlotGeo('-airfoils');
% aviao.PlotWake('-airfoils');

%% Malha

aviao.LiftingSurfaces(1).Aero.Mesh.Ny = [10 8];
aviao.LiftingSurfaces(1).Aero.Mesh.TypeY = [1 3]; % uniforme na se��o mais interior.
                                                    % cosseno nas se��es intermedi�rias para prever os efeitos da jun��o de se��es com perfis diferentes
                                                    % e direita na se��o mais externas em busca de dados mais detalhados por conta dos v�rtices de ponta de asa

aviao.LiftingSurfaces(2).Aero.Mesh.Ny = [10 8];
aviao.LiftingSurfaces(2).Aero.Mesh.TypeY = [1 3];

aviao.UpdateAeroMesh;
% aviao.PlotMesh;

%% Condi��es de Voo

condicao = FlightConditions;
condicao.Voo = 12;
condicao.rho = 1.185;
aoa = [0];

%% Solver
    j = j+1;
    condicao.alpha = aoa;
    NL_VLM(aviao, condicao,'-append','-dispstat','-itermax',30);

    CLplane(j) = aviao.Aero.Coeffs.CL;
    CDplane(j) = aviao.Aero.Coeffs.CD;

    for i = 1:2
        CDsurf(i,j) = aviao.LiftingSurfaces(i).Aero.Coeffs.CD;
        CLsurf(i,j) = aviao.LiftingSurfaces(i).Aero.Coeffs.CL;
    end

disp(c) 
end 
figure(1)
% plot(aoa,CLplane)
%plot(solo-3,CLplane,'--',solo-3,CLsurf(1,:),'--',solo-3,CLsurf(2,:),'--') 
plot(solo-3,CLsurf(1,:),'--')
title('Coeficientes de sustenta��o')
xlabel('alpha [�]')
ylabel('CL')
legend('CL asa solo','Location','northwest')
% legend('CL Global (emp -8)','CL asa (emp -8)','CL empenagem (emp -8)','Location','northwest')
grid minor
hold on

% figure(2)
% % plot(aoa,CDplane)
% plot(aoa,CDplane,'--',aoa,CDsurf(1,:),'--',aoa,CDsurf(2,:),'--')
% title('Coeficientes de arrasto')
% xlabel('alpha [�]')
% ylabel('CD')
% legend('CD Global','CD asa','CD eh','Location','northwest')
% % legend('CD Global (emp -8)','CD asa (emp -8)','CD empenagem (emp -8)','Location','northwest')
% grid minor
% hold on 
% %% SUSTENTA��O E ARRASTO 
% % L = CLplane(1)*condicao.rho*Cond.Voo^2*S;
% % D = CDplane(1)*condicao.rho*Cond.VooV^2*S;
% 
% Lasa = CLsurf(1,:).*condicao.rho.*condicao.Voo.^2*aviao.LiftingSurfaces(1).Geo.Area.Sref;
% Lemp = CLsurf(2,:).*condicao.rho.*condicao.Voo.^2*aviao.LiftingSurfaces(2).Geo.Area.Sref;
% Lglobal = Lasa + Lemp;
% 
% figure(3)
% plot(aoa,Lglobal,aoa,Lasa,aoa,Lemp)
% title('For�a de sustenta��o')
% xlabel('alpha [�]')
% ylabel('Lift [N]')
% legend('L global','L asa','L eh','Location','northwest')
% grid minor
% 
% Dasa = CDsurf(1,:).*condicao.rho.*condicao.Voo.^2*aviao.LiftingSurfaces(1).Geo.Area.Sref;
% Demp = CDsurf(2,:).*condicao.rho.*condicao.Voo.^2*aviao.LiftingSurfaces(2).Geo.Area.Sref;
% Dglobal = Dasa + Demp;
% 
% figure(4)
% plot(aoa,Dglobal,aoa,Dasa,aoa,Demp)
% title('For�a de arrasto')
% xlabel('alpha [�]')
% ylabel('Drag [N]')
% legend('D Global','D asa','D emp','Location','northwest')
% grid minor


