function Mest_aoa(geo,flc,varargin)

V = 10;
aoamax = 16;
aoamin = -5;
flc.Voo = V;                                                               % Declarando a velocidade para o VLM
flc.case(1).aoa = (aoamin:aoamax);                                              % Range de angulos que serao calculados para as asas
flc.case(2).aoa = (10:-1:-15);                                      % Range de angulos que serao calculados para a empenagem
coef = coeffs(geo,flc,0,[1 2 3],1);                                         % Chamando VLM
for j=1:geo.surfacenum
    coeff.fit(j).CL = fit(coef(j).aoa',coef(j).CL','linearinterp');    % Nesse loop e feito o fit dos coeficientes (apenas para simplificar o acesso aos coeficientes)
    coeff.fit(j).CD = fit(coef(j).aoa',coef(j).CD','linearinterp');
    coeff.fit(j).Cm25 = fit(coef(j).aoa',coef(j).Cm25','linearinterp');
end
aoa_mest = flc.case(1).aoa(1):0.1:flc.case(1).aoa(end);
for k=1:length(aoa_mest)
    x1 = 0.01;
    [y,erro] = fminsearch(@mest_fun,x1);
    mest(k) = y/coef(1).Cref;
    erromest(k) = erro;
end
figure
plot(aoa_mest,mest);
grid on
xlabel('Angulo de ataque [graus]')
ylabel('Margem Estatica [ ]')
title(['Margem Estatica em Funca do aoa, Velocidade de analise = ', num2str(flc.Voo),' [m/s]'])
figure
plot(aoa_mest,erromest);
grid on
xlabel('Angulo de ataque [graus]')
ylabel('Erro da funcao de mest [ ]')
%% ==================== Funcao de otimizacao MEST =========================
function Erromest = mest_fun(y)                                                 % Funcao para otimizar
deltaCG = y;                                                                % O codigo recebe uma posicao deltaX do CG
inc = 0.1;                                                                  % Passo de aoa que o codigo buscara Cmalfa = 0
aoa = [aoa_mest(k) aoa_mest(k)+inc];                                        % Valores de aoa que serao analisados
Cmt = zeros(1,length(aoa));                                                 % Prealocando essa variavel
Cm = zeros(geo.surfacenum,1);
for t=1:length(aoa)
    for j=1:geo.surfacenum
        M = geo.s.neta(j)*[0 -geo.s.pos(j,3) geo.s.pos(j,2); geo.s.pos(j,3) 0 -(geo.s.pos(j,1)+deltaCG); -geo.s.pos(j,2) (geo.s.pos(j,1)+deltaCG) 0]*([cosd(aoa(t)) 0 -sind(aoa(t));0 1 0;sind(aoa(t)) 0 cosd(aoa(t))]*[-coeff.fit(j).CD(aoa(t)-geo.s.deda(j)*aoa(t)-geo.s.eo(j));0;-coeff.fit(j).CL(aoa(t)-geo.s.deda(j)*aoa(t)-geo.s.eo(j))]);
        Cm(j) = M(2)/coef(1).Cref + geo.s.neta(j)*coeff.fit(j).Cm25(aoa(t) - geo.s.deda(j)*aoa(t) - geo.s.eo(j));
    end
    Cmt(t) = sum(Cm);
end
Erromest = abs(diff(Cmt))*100;
end
end