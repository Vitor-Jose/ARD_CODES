% Organiza dados retirados pelo Find_airfoil
clear; close all; clc;

read_file = 'Amostra3.mat';
load(read_file);

type = 'Coef_S_P3';

% 01 - 'Coef_AoA'
% 02 - 'Coef_AoA_V'
% 03 - 'Coef_AoA_P'
% 04 - 'Coef_AoA_S'
% 05 - 'Coef_AoA_V3'
% 06 - 'Coef_AoA_P3'
% 07 - 'Coef_AoA_S3'

% 08 - 'Coef_Coef'

% 09 - 'Coef_S_V3'
% 10 - 'Coef_S_V3_P'
% 11 - 'Coef_S_P3'
% 12 - 'Coefmax_S_P3'

% Parametros
S = 0.8; S_pos = prm.S == S;
P_pos = 1;
V = 13.5; V_pos = prm.V == V;
AoA = 0; A_pos = prm.AoA == AoA;
rho = 1.1161;

switch type
    % =====================================================================
    case 'Coef_AoA'
        x = prm.AoA;
        
        y1 = dados(S_pos,P_pos).CL(V_pos,:);
        y2 = dados(S_pos,P_pos).CD(V_pos,:);
        y3 = y1./y2;
        y4 = dados(S_pos,P_pos).CM(V_pos,:);
        
        ptitulo = cellstr(prm.perfil(P_pos));
        titulo  = ['S = ' num2str(S) ' m^2 - Perfil: ' ptitulo{1} ' - V = ' num2str(V) ' m/s'];
        
        figure; plot(x,y4); grid on;
        xlabel('AoA'); ylabel('C_M');
        title(titulo); 
        
        figure; plot(x,y3); grid on;
        xlabel('AoA'); ylabel('C_L/C_D');
        title(titulo); 
        
        figure; plot(x,y2); grid on;
        xlabel('AoA'); ylabel('C_D');
        title(titulo); 
        
        figure; plot(x,y1); grid on;
        xlabel('AoA'); ylabel('C_L');
        title(titulo); 
    
    % =====================================================================
    case 'Coef_AoA_V'
        x = prm.AoA;
        
        y1 = dados(S_pos,P_pos).CL(:,:);
        y2 = dados(S_pos,P_pos).CD(:,:);
        y3 = y1./y2;
        y4 = dados(S_pos,P_pos).CM(:,:);
        
        for aux_V = 1:length(prm.V)
            legenda{aux_V} = ['V = ' num2str(prm.V(aux_V))];
        end
        ptitulo = cellstr(prm.perfil(P_pos));
        titulo  = ['S = ' num2str(S) ' m^2 - Perfil: ' ptitulo{1}];
        
        figure; plot(x,y4); grid on;
        xlabel('AoA'); ylabel('C_M');
        legend(legenda,'Location','south'); title(titulo);
        
        figure; plot(x,y3); grid on;
        xlabel('AoA'); ylabel('C_L/C_D');
        legend(legenda); title(titulo);
        
        figure; plot(x,y2); grid on;
        xlabel('AoA'); ylabel('C_D');
        legend(legenda,'Location','northwest'); title(titulo);
        
        figure; plot(x,y1); grid on;
        xlabel('AoA'); ylabel('C_L');
        legend(legenda,'Location','southeast'); title(titulo);  
        
    % =====================================================================      
    case 'Coef_AoA_P'
        x = prm.AoA;
        
        for aux_P = 1:length(prm.perfil)
            y1(aux_P,:) = dados(S_pos,aux_P).CL(V_pos,:);
            y2(aux_P,:) = dados(S_pos,aux_P).CD(V_pos,:);
            y3 = y1./y2;
            y4(aux_P,:) = dados(S_pos,aux_P).CM(V_pos,:);
        end
        legenda = prm.perfil;
        titulo  = ['S = ' num2str(S) ' m^2 - V = ' num2str(V) ' m/s'];
        
        figure; plot(x,y4); grid on;
        xlabel('AoA'); ylabel('C_M');
        legend(legenda,'Location','south'); title(titulo);
        
        figure; plot(x,y3); grid on;
        xlabel('AoA'); ylabel('C_L/C_D');
        legend(legenda); title(titulo);
        
        figure; plot(x,y2); grid on;
        xlabel('AoA'); ylabel('C_D');
        legend(legenda,'Location','northwest'); title(titulo);
        
        figure; plot(x,y1); grid on;
        xlabel('AoA'); ylabel('C_L');
        legend(legenda,'Location','southeast'); title(titulo);  
       
    % =====================================================================      
    case 'Coef_AoA_S'
        x = prm.AoA;
        
        for aux_S = 1:length(prm.S)
            y1(aux_S,:) = dados(aux_S,P_pos).CL(V_pos,:);
            y2(aux_S,:) = dados(aux_S,P_pos).CD(V_pos,:);
            y3 = y1./y2;
            y4(aux_S,:) = dados(aux_S,P_pos).CM(V_pos,:);
            legenda{aux_S} = ['S = ' num2str(prm.S(aux_S))];
        end
        ptitulo = cellstr(prm.perfil(P_pos));
        titulo  = ['V = ' num2str(V) ' m/s - Perfil: ' ptitulo{1}];
        
        figure; plot(x,y4); grid on;
        xlabel('AoA'); ylabel('C_M');
        legend(legenda,'Location','south'); title(titulo);
        
        figure; plot(x,y3); grid on;
        xlabel('AoA'); ylabel('C_L/C_D');
        legend(legenda); title(titulo);
        
        figure; plot(x,y2); grid on;
        xlabel('AoA'); ylabel('C_D');
        legend(legenda,'Location','northwest'); title(titulo);
        
        figure; plot(x,y1); grid on;
        xlabel('AoA'); ylabel('C_L');
        legend(legenda,'Location','southeast'); title(titulo);         

    % =====================================================================      
    case 'Coef_AoA_V3'
        x = prm.AoA;
        y = prm.V;
        
        z1 = dados(S_pos,P_pos).CL(:,:);
        z2 = dados(S_pos,P_pos).CD(:,:);
        z3 = z1./z2;
        z4 = dados(S_pos,P_pos).CM(:,:);
        for aux_V = 1:length(prm.V)
            qinf = 0.5.*rho.*prm.V(aux_V).^2;
            z5(aux_V,:) = z1(aux_V,:).*prm.S(S_pos).*qinf;
            z6(aux_V,:) = z2(aux_V,:).*prm.S(S_pos).*qinf;
        end
            
        ptitulo = cellstr(prm.perfil(P_pos));
        titulo  = ['S = ' num2str(S) ' m^2 - Perfil: ' ptitulo{1}];
        
        figure; surf(x,y,z6); grid on;
        xlabel('AoA'); ylabel('V'); zlabel('D');
        title(titulo);
        
        figure; surf(x,y,z5); grid on;
        xlabel('AoA'); ylabel('V'); zlabel('L');
        title(titulo);
        
        figure; surf(x,y,z4); grid on;
        xlabel('AoA'); ylabel('V'); zlabel('C_M');
        title(titulo);
        
        figure; surf(x,y,z3); grid on;
        xlabel('AoA'); ylabel('V'); zlabel('C_L/C_D');
        title(titulo);
        
        figure; surf(x,y,z2); grid on;
        xlabel('AoA'); ylabel('V'); zlabel('C_D');
        title(titulo);
        
        figure; surf(x,y,z1); grid on;
        xlabel('AoA'); ylabel('V'); zlabel('C_L');  
        title(titulo);
        
    % =====================================================================      
    case 'Coef_AoA_P3'
        x = prm.AoA;
        y = 1:length(prm.perfil);
        
        for aux_P = 1:length(prm.perfil)
            z1(aux_P,:) = dados(S_pos,aux_P).CL(V_pos,:);
            z2(aux_P,:) = dados(S_pos,aux_P).CD(V_pos,:);
            z3 = z1./z2;
            z4(aux_P,:) = dados(S_pos,aux_P).CM(V_pos,:);
        end
        titulo  = ['S = ' num2str(S) ' m^2 - V = ' num2str(V) ' m/s'];
        
        figure; surf(x,y,z4); grid on;
        text(x(1)*ones(1,length(y)),y,0*z4(:,1),cellstr(prm.perfil));
        xlabel('AoA'); ylabel('Perfil'); zlabel('C_M');
        title(titulo);
        
        figure; surf(x,y,z3); grid on;
        xlabel('AoA'); ylabel('Perfil'); zlabel('C_L/C_D');
        text(x(1)*ones(1,length(y)),y,0*z3(:,1),cellstr(prm.perfil));
        title(titulo);
        
        figure; surf(x,y,z2); grid on;
        xlabel('AoA'); ylabel('Perfil'); zlabel('C_D');
        text(x(1)*ones(1,length(y)),y,0*z2(:,1),cellstr(prm.perfil));
        title(titulo);
        
        figure; surf(x,y,z1); grid on;
        xlabel('AoA'); ylabel('Perfil'); zlabel('C_L');  
        text(x(1)*ones(1,length(y)),y,0*z1(:,1),cellstr(prm.perfil));
        title(titulo);
        
    % =====================================================================      
    case 'Coef_AoA_S3'
        x = prm.AoA;
        y = prm.S;
        
        for aux_S = 1:length(prm.S)
            z1(aux_S,:) = dados(aux_S,P_pos).CL(V_pos,:);
            z2(aux_S,:) = dados(aux_S,P_pos).CD(V_pos,:);
            z3 = z1./z2;
            z4(aux_S,:) = dados(aux_S,P_pos).CM(V_pos,:);
        end
        ptitulo = cellstr(prm.perfil(P_pos));
        titulo  = ['V = ' num2str(V) ' m/s - Perfil: ' ptitulo{1}];
        
        figure; surf(x,y,z4); grid on;
        xlabel('AoA'); ylabel('S'); zlabel('C_M');
        title(titulo);
        
        figure; surf(x,y,z3); grid on;
        xlabel('AoA'); ylabel('S'); zlabel('C_L/C_D');
        title(titulo);
        
        figure; surf(x,y,z2); grid on;
        xlabel('AoA'); ylabel('S'); zlabel('C_D');
        title(titulo);
        
        figure; surf(x,y,z1); grid on;
        xlabel('AoA'); ylabel('S'); zlabel('C_L');
        title(titulo);
    % =====================================================================
    case 'Coef_Coef'   
        y = dados(S_pos,P_pos).CL(V_pos,:);
        
        x1 = dados(S_pos,P_pos).CD(V_pos,:);
        x2 = y./x1;
        x3 = dados(S_pos,P_pos).CM(V_pos,:);
        
        ptitulo = cellstr(prm.perfil(P_pos));
        titulo  = ['S = ' num2str(S) ' m^2 - Perfil: ' ptitulo{1} ' - V = ' num2str(V) ' m/s'];
        
        figure; plot(x3,y); grid on;
        xlabel('C_M'); ylabel('C_L');
        title(titulo); 
        
        figure; plot(x2,y); grid on;
        xlabel('C_L/C_D'); ylabel('C_L');
        title(titulo); 
        
        figure; plot(x1,y); grid on;
        xlabel('C_D'); ylabel('C_L');
        title(titulo); 
        
    % =====================================================================      
    case 'Coef_S_V3'
        x = prm.S;
        y = prm.V;

        for aux_S = 1:length(prm.S)
            for aux_V = 1:length(prm.V)
                qinf = 0.5.*rho.*prm.V(aux_V).^2;
                z1(aux_V,aux_S) = dados(aux_S,P_pos).CL(aux_V,A_pos);
                z2(aux_V,aux_S) = dados(aux_S,P_pos).CD(aux_V,A_pos);
                z3 = z1./z2;
                z4(aux_V,aux_S) = dados(aux_S,P_pos).CM(aux_V,A_pos);
                z5(aux_V,aux_S) = z1(aux_V,aux_S).*prm.S(aux_S).*qinf;
                z6(aux_V,aux_S) = z2(aux_V,aux_S).*prm.S(aux_S).*qinf;
            end
        end
        ptitulo = cellstr(prm.perfil(P_pos));
        titulo  = ['Perfil: ' ptitulo{1} ' - AoA = ' num2str(AoA) '�'];
        
        figure(1); surf(x,y,z6); grid on;
        xlabel('S'); ylabel('V'); zlabel('D');
        title(titulo);
        
        figure(2); surf(x,y,z5); grid on;
        xlabel('S'); ylabel('V'); zlabel('L');
        title(titulo);
        
        figure(3); surf(x,y,z4); grid on;
        xlabel('S'); ylabel('V'); zlabel('C_M');
        title(titulo);
        
        figure(4); surf(x,y,z3); grid on;
        xlabel('S'); ylabel('V'); zlabel('C_L/C_D');
        title(titulo);
        
        figure(5); surf(x,y,z2); grid on;
        xlabel('S'); ylabel('V'); zlabel('C_D');
        title(titulo);
        
        figure(6); surf(x,y,z1); grid on;
        xlabel('S'); ylabel('V'); zlabel('C_L');  
        title(titulo);
        
    % =====================================================================      
    case 'Coef_S_V3_P'
        x = prm.S;
        y = prm.V;
        
        for aux_P = 1:length(prm.perfil)
            z1 = zeros(length(prm.V),length(prm.S));
            z2 = zeros(length(prm.V),length(prm.S));
            z3 = zeros(length(prm.V),length(prm.S));
            z4 = zeros(length(prm.V),length(prm.S));
            z5 = zeros(length(prm.V),length(prm.S));
            z6 = zeros(length(prm.V),length(prm.S));

        for aux_S = 1:length(prm.S)
            for aux_V = 1:length(prm.V)
                qinf = 0.5.*rho.*prm.V(aux_V).^2;
                z1(aux_V,aux_S) = dados(aux_S,aux_P).CL(aux_V,A_pos);
                z2(aux_V,aux_S) = dados(aux_S,aux_P).CD(aux_V,A_pos);
                z3 = z1./z2;
                z4(aux_V,aux_S) = dados(aux_S,aux_P).CM(aux_V,A_pos);
                z5(aux_V,aux_S) = z1(aux_V,aux_S).*prm.S(aux_S).*qinf;
                z6(aux_V,aux_S) = z2(aux_V,aux_S).*prm.S(aux_S).*qinf;
            end
        end
        ptitulo = cellstr(prm.perfil(P_pos));
        titulo  = ['AoA = ' num2str(AoA) '�'];
        
        figure(1); hold on; surf(x,y,z6); grid on;
        xlabel('S'); ylabel('V'); zlabel('D');
        title(titulo);
        
        figure(2); hold on; surf(x,y,z5); grid on;
        xlabel('S'); ylabel('V'); zlabel('L');
        title(titulo);
        
        figure(3); hold on; surf(x,y,z4); grid on;
        xlabel('S'); ylabel('V'); zlabel('C_M');
        title(titulo);
        
        figure(4); hold on; surf(x,y,z3); grid on;
        xlabel('S'); ylabel('V'); zlabel('C_L/C_D');
        title(titulo);
        
        figure(5); hold on; surf(x,y,z2); grid on;
        xlabel('S'); ylabel('V'); zlabel('C_D');
        title(titulo);
        
        figure(6); hold on; surf(x,y,z1); grid on;
        xlabel('S'); ylabel('V'); zlabel('C_L');  
        title(titulo);
        end
    % =====================================================================      
    case 'Coef_S_P3'
        x = prm.S;
        y = 1:length(prm.perfil);
        
        qinf = 0.5.*rho.*V.^2;
        for aux_P = 1:length(prm.perfil)
            for aux_S = 1:length(prm.S)
                z1(aux_P,aux_S) = dados(aux_S,aux_P).CL(V_pos,A_pos);
                z2(aux_P,aux_S) = dados(aux_S,aux_P).CD(V_pos,A_pos);
                z3 = z1./z2;
                z4(aux_P,aux_S) = dados(aux_S,aux_P).CM(V_pos,A_pos);
                z5(aux_P,aux_S) = z1(aux_P,aux_S).*prm.S(aux_S).*qinf;
                z6(aux_P,aux_S) = z2(aux_P,aux_S).*prm.S(aux_S).*qinf;
                z7(aux_P,aux_S) = dados(aux_S,aux_P).e(V_pos,A_pos);
            end
        end
        titulo  = ['V = ' num2str(V) ' m/s - AoA = ' num2str(AoA) '�'];
        
        figure; surf(x,y,z7); grid on;
        text(x(1)*ones(1,length(y)),y,0*z6(:,1),cellstr(prm.perfil));
        xlabel('S'); ylabel('Perfil'); zlabel('e');
        title(titulo);
        
        figure; surf(x,y,z6); grid on;
        text(x(1)*ones(1,length(y)),y,0*z6(:,1),cellstr(prm.perfil));
        xlabel('S'); ylabel('Perfil'); zlabel('D');
        title(titulo);
        
        figure; surf(x,y,z5); grid on;
        text(x(1)*ones(1,length(y)),y,0*z5(:,1),cellstr(prm.perfil));
        xlabel('S'); ylabel('Perfil'); zlabel('L');
        title(titulo);
        
        figure; surf(x,y,z4); grid on;
        text(x(1)*ones(1,length(y)),y,0*z4(:,1),cellstr(prm.perfil));
        xlabel('S'); ylabel('Perfil'); zlabel('C_M');
        title(titulo);
        
        figure; surf(x,y,z3); grid on;
        xlabel('S'); ylabel('Perfil'); zlabel('C_L/C_D');
        text(x(1)*ones(1,length(y)),y,0*z3(:,1),cellstr(prm.perfil));
        title(titulo);
        
        figure; surf(x,y,z2); grid on;
        xlabel('S'); ylabel('Perfil'); zlabel('C_D');
        text(x(1)*ones(1,length(y)),y,0*z2(:,1),cellstr(prm.perfil));
        title(titulo);
        
        figure; surf(x,y,z1); grid on;
        xlabel('S'); ylabel('Perfil'); zlabel('C_L');  
        text(x(1)*ones(1,length(y)),y,0*z1(:,1),cellstr(prm.perfil));
        title(titulo);
        
    % =====================================================================      
    case 'Coefmax_S_P3'
        x = prm.S;
        y = 1:length(prm.perfil);
        
        qinf = 0.5.*rho.*V.^2;
        for aux_P = 1:length(prm.perfil)
            for aux_S = 1:length(prm.S)
                [~,A_pos] = max(dados(aux_S,aux_P).CL(V_pos,:));
                z1(aux_P,aux_S) = dados(aux_S,aux_P).CL(V_pos,A_pos);
                z2(aux_P,aux_S) = dados(aux_S,aux_P).CD(V_pos,A_pos);
                z3 = z1./z2;
                z4(aux_P,aux_S) = dados(aux_S,aux_P).CM(V_pos,A_pos);
                z5(aux_P,aux_S) = z1(aux_P,aux_S).*prm.S(aux_S).*qinf;
                z6(aux_P,aux_S) = z2(aux_P,aux_S).*prm.S(aux_S).*qinf;
            end
        end
        titulo  = ['V = ' num2str(V) ' m/s - AoA = C_{L,max}'];
        
        figure; surf(x,y,z6); grid on;
        text(x(1)*ones(1,length(y)),y,0*z6(:,1),cellstr(prm.perfil));
        xlabel('S'); ylabel('Perfil'); zlabel('D');
        title(titulo);
        
        figure; surf(x,y,z5); grid on;
        text(x(1)*ones(1,length(y)),y,0*z5(:,1),cellstr(prm.perfil));
        xlabel('S'); ylabel('Perfil'); zlabel('L');
        title(titulo);
        
        figure; surf(x,y,z4); grid on;
        text(x(1)*ones(1,length(y)),y,0*z4(:,1),cellstr(prm.perfil));
        xlabel('S'); ylabel('Perfil'); zlabel('C_M');
        title(titulo);
        
        figure; surf(x,y,z3); grid on;
        xlabel('S'); ylabel('Perfil'); zlabel('C_L/C_D');
        text(x(1)*ones(1,length(y)),y,0*z3(:,1),cellstr(prm.perfil));
        title(titulo);
        
        figure; surf(x,y,z2); grid on;
        xlabel('S'); ylabel('Perfil'); zlabel('C_D');
        text(x(1)*ones(1,length(y)),y,0*z2(:,1),cellstr(prm.perfil));
        title(titulo);
        
        figure; surf(x,y,z1); grid on;
        xlabel('S'); ylabel('Perfil'); zlabel('C_L');  
        text(x(1)*ones(1,length(y)),y,0*z1(:,1),cellstr(prm.perfil));
        title(titulo);
end
