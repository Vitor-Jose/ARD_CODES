%% Plota as distribui��es nas asa
% Vitor Jos� - nov. 2021

%% LIMPEZA
clear; clc; close all;
SetPaths

%% PAR�METROS INICIAS
load('03-MACACO_Aeronaves/Regular2021.mat');    % Carrega a aeronave
aviao = obj;        % Realoca para avi�o

tipo = 'cf';        % cf ou cp
vista = 'extra';    % extra ou intra

plot_surf =  true;
plot_contour = true;

j = 1; p = 14;
for aoa = 0
% aviao.LiftingSurfaces.IsMain = 1;
% aviao.LiftingSurfaces.label = 'Asa principal';
% aviao.LiftingSurfaces.Geo.Yb = [0 0.8 1.6];
% aviao.LiftingSurfaces.Geo.C = [0.4 0.4 0.2];
% aviao.LiftingSurfaces.Geo.Dihedral = [0 0];
% aviao.LiftingSurfaces.Geo.Sweep = [0 0];
% aviao.LiftingSurfaces.Geo.TwistY = [0 0 0];
% aviao.LiftingSurfaces.Geo.Incidence = 0;
% aviao.LiftingSurfaces(1).Geo.pos.x = 0;
% aviao.LiftingSurfaces(1).Geo.pos.y = 0;
% aviao.LiftingSurfaces(1).Geo.pos.z = 0;
perfil(1) = ImportObj('L1_3c.mat');
perfil(2) = ImportObj('L2_L1_3c.mat');
aviao.LiftingSurfaces(1).Aero.Airfoils.Data = [perfil(1) perfil(1) perfil(2)];

% aviao.LiftingSurfaces(2) = LiftingSurface('Asa2'); %Obrigat�rio para novas superf�cies
% aviao.LiftingSurfaces(2).IsMain = 0;
% aviao.LiftingSurfaces(2).Geo.Yb = [0    0.70    1.1750];
% aviao.LiftingSurfaces(2).Geo.C = [0.3699    0.3693  0.22];
% aviao.LiftingSurfaces(2).Geo.Dihedral = [0 0 0];
% aviao.LiftingSurfaces(2).Geo.Sweep = [0 0 0];
% aviao.LiftingSurfaces(2).Geo.TwistY = [0 0 0 0];
% aviao.LiftingSurfaces(2).Geo.Incidence = 1;
% aviao.LiftingSurfaces(2).Geo.pos.x = -0.1;
% aviao.LiftingSurfaces(2).Geo.pos.y = 0;
% aviao.LiftingSurfaces(2).Geo.pos.z = 0.95;
perfil(3) = ImportObj('MH82.mat');
perfil(4) = ImportObj('MH82.mat');
perfil(5) = ImportObj('MH82.mat');
aviao.LiftingSurfaces(2).Aero.Airfoils.Data = [perfil(3) perfil(4) perfil(5)];
% 
% aviao.LiftingSurfaces(3) = LiftingSurface('EH'); %Obrigat�rio para novas superf�cies
% aviao.LiftingSurfaces(3).IsMain = 0;
% aviao.LiftingSurfaces(3).Geo.Yb = [0 0.9];
% aviao.LiftingSurfaces(3).Geo.C = [0.18 0.18];
% aviao.LiftingSurfaces(3).Geo.Dihedral = [0];
% aviao.LiftingSurfaces(3).Geo.Sweep = [0];
% aviao.LiftingSurfaces(3).Geo.TwistY = [0 0];
% aviao.LiftingSurfaces(3).Geo.Incidence = 0;
% aviao.LiftingSurfaces(3).Geo.pos.x = 0.4;
% aviao.LiftingSurfaces(3).Geo.pos.y = 0;
% aviao.LiftingSurfaces(3).Geo.pos.z = 1.35;
% perfil(3) = ImportObj('x2l_v3.mat');
% aviao.LiftingSurfaces(3).Aero.Airfoils.Data = [perfil(3) perfil(3)];

aviao.UpdateGeo;
% aviao.PlotGeo('-nimage',1); % descomente para rodar o plot
% view([-40,30])
% axis off
%% malha
% aviao.LiftingSurfaces(1).Aero.Mesh.Ny = [p(j) p(j)];
% aviao.LiftingSurfaces(1).Aero.Mesh.TypeY = [1 3];
% aviao.LiftingSurfaces(2).Aero.Mesh.Ny=[p(j) p(j)];
% aviao.LiftingSurfaces(2).Aero.Mesh.TypeY= [1 3];
% aviao.LiftingSurfaces(3).Aero.Mesh.Ny = [1.5*p(j)];
% aviao.LiftingSurfaces(3).Aero.Mesh.TypeY= 3;

% aviao.UpdateAeroMesh;
% aviao.PlotMesh; % descomente para rodar o plot

%% condicoes
condicao= FlightConditions;
condicao.alpha= aoa;
condicao.Voo= 11.5;
condicao.rho= 1.1161;


%% solver
NL_VLM(aviao, condicao);
CLplane = aviao.Aero.Coeffs.CL;
CDplane = aviao.Aero.Coeffs.CD;

% Se��o
f = figure(2); hold on; axis equal
LF = 2;
n_divisoes = 100; % N�mero de divis�es na corda

% for ii = 1:length(aviao.LiftingSurfaces(LF).Aero.Mesh.Verts.v1)
%     x = [aviao.LiftingSurfaces(LF).Aero.Mesh.Verts.v1(ii,1) aviao.LiftingSurfaces(LF).Aero.Mesh.Verts.v2(ii,1) aviao.LiftingSurfaces(LF).Aero.Mesh.Verts.v3(ii,1) aviao.LiftingSurfaces(LF).Aero.Mesh.Verts.v4(ii,1) aviao.LiftingSurfaces(LF).Aero.Mesh.Verts.v1(ii,1)];
%     y = [aviao.LiftingSurfaces(LF).Aero.Mesh.Verts.v1(ii,2) aviao.LiftingSurfaces(LF).Aero.Mesh.Verts.v2(ii,2) aviao.LiftingSurfaces(LF).Aero.Mesh.Verts.v3(ii,2) aviao.LiftingSurfaces(LF).Aero.Mesh.Verts.v4(ii,2) aviao.LiftingSurfaces(LF).Aero.Mesh.Verts.v1(ii,2)];
%     plot(x,y,'k','LineWidth',1.2); axis equal;
%     axis([-0.25*aviao.LiftingSurfaces(LF).Geo.C(1)-0.20 0.75*aviao.LiftingSurfaces(LF).Geo.C(1)+0.05 -aviao.LiftingSurfaces(LF).Geo.b2-0.05 aviao.LiftingSurfaces(LF).Geo.b2+0.05])
%     view([90,90])
% end

switch tipo
%% Plotar Cf
    case 'cf'
aviao.LiftingSurfaces(LF).Aero.Loads.alpha_eff = aviao.LiftingSurfaces(LF).Aero.Loads.alpha-aviao.LiftingSurfaces(LF).Aero.Loads.alpha_ind;
x_top = linspace(1,0,n_divisoes);

for ii = 1:aviao.LiftingSurfaces(LF).Aero.Mesh.Ny_tot
    % Definindo o perfil de dentro e o perfil de fora da se��o
    Yinner_ind = aviao.LiftingSurfaces(LF).Aero.Mesh.SpanInner_ind(ii);
    Youter_ind = aviao.LiftingSurfaces(LF).Aero.Mesh.SpanOuter_ind(ii);
    
    % Definindo o airfoil do momento
	AirfoilData_inner	= aviao.LiftingSurfaces(LF).Aero.Airfoils.Data(Yinner_ind);
	AirfoilData_outer	= aviao.LiftingSurfaces(LF).Aero.Airfoils.Data(Youter_ind);
    
    % Encontrando as posi��es do Reynolds nos dois casos
    Re_pan = aviao.LiftingSurfaces(LF).Aero.Loads.Re_pan(ii);
    [~,pos_Re_inner] = min(abs([AirfoilData_inner.pol.Re]-Re_pan));
    [~,pos_Re_outer] = min(abs([AirfoilData_outer.pol.Re]-Re_pan));
    
    
    % Encontrando o intervalo de alpha do perfil interno
    pos_a_inner = find(AirfoilData_inner.pol(pos_Re_inner).alpha <= aviao.LiftingSurfaces(LF).Aero.Loads.alpha_eff(ii),1,'last');
    i1 = AirfoilData_inner.pol(pos_Re_inner).alpha(pos_a_inner);
    i2 = AirfoilData_inner.pol(pos_Re_inner).alpha(pos_a_inner+1);
    
    % Interpola��o do interno
    [~,pos_inner_1] = min(AirfoilData_inner.pol(pos_Re_inner).x(:,pos_a_inner));
    switch vista
        case 'extra'
            x_inner_1 = AirfoilData_inner.pol(pos_Re_inner).x(1:pos_inner_1,pos_a_inner);
            Cf_inner_1 = AirfoilData_inner.pol(pos_Re_inner).Cf(1:pos_inner_1,pos_a_inner);
        case 'intra'
            x_inner_1 = AirfoilData_inner.pol(pos_Re_inner).x(pos_inner_1:end,pos_a_inner);
            Cf_inner_1 = AirfoilData_inner.pol(pos_Re_inner).Cf(pos_inner_1:end,pos_a_inner);
    end
    Cf_inner_1 = interp1(x_inner_1,Cf_inner_1,x_top,'linear','extrap');
    
    [~,pos_inner_2] = min(AirfoilData_inner.pol(pos_Re_inner).x(:,pos_a_inner+1));
    switch vista
        case 'extra'
            x_inner_2 = AirfoilData_inner.pol(pos_Re_inner).x(1:pos_inner_2,pos_a_inner+1);
            Cf_inner_2 = AirfoilData_inner.pol(pos_Re_inner).Cf(1:pos_inner_2,pos_a_inner+1);
        case 'intra'
            x_inner_2 = AirfoilData_inner.pol(pos_Re_inner).x(pos_inner_2:end,pos_a_inner+1);
            Cf_inner_2 = AirfoilData_inner.pol(pos_Re_inner).Cf(pos_inner_2:end,pos_a_inner+1);
    end
    Cf_inner_2 = interp1(x_inner_2,Cf_inner_2,x_top,'linear','extrap');
    
    Cf_inner = interp1([i1 i2],[Cf_inner_1;Cf_inner_2],aviao.LiftingSurfaces(LF).Aero.Loads.alpha_eff(ii));  
    
    % Encontrando o intervalo de alpha do perfil externo
    pos_a_outer = find(AirfoilData_outer.pol(pos_Re_outer).alpha <= aviao.LiftingSurfaces(LF).Aero.Loads.alpha_eff(ii),1,'last');
    o1 = AirfoilData_outer.pol(pos_Re_outer).alpha(pos_a_outer);
    o2 = AirfoilData_outer.pol(pos_Re_outer).alpha(pos_a_outer+1);
    
    % Interpola��o do externo
    [~,pos_outer_1] = min(AirfoilData_outer.pol(pos_Re_outer).x(:,pos_a_outer));
    switch vista
        case 'extra'
            x_outer_1 = AirfoilData_outer.pol(pos_Re_outer).x(1:pos_outer_1,pos_a_outer);
            Cf_outer_1 = AirfoilData_outer.pol(pos_Re_outer).Cf(1:pos_outer_1,pos_a_outer);
        case 'intra'
            x_outer_1 = AirfoilData_outer.pol(pos_Re_outer).x(pos_outer_1:end,pos_a_outer);
            Cf_outer_1 = AirfoilData_outer.pol(pos_Re_outer).Cf(pos_outer_1:end,pos_a_outer);
    end
    Cf_outer_1 = interp1(x_outer_1,Cf_outer_1,x_top,'linear','extrap');
    
    [~,pos_outer_2] = min(AirfoilData_outer.pol(pos_Re_outer).x(:,pos_a_outer+1));
    switch vista
        case 'extra'
            x_outer_2 = AirfoilData_outer.pol(pos_Re_outer).x(1:pos_outer_2,pos_a_outer+1);
            Cf_outer_2 = AirfoilData_outer.pol(pos_Re_outer).Cf(1:pos_outer_2,pos_a_outer+1);
        case 'intra'
            x_outer_2 = AirfoilData_outer.pol(pos_Re_outer).x(pos_outer_2:end,pos_a_outer+1);
            Cf_outer_2 = AirfoilData_outer.pol(pos_Re_outer).Cf(pos_outer_2:end,pos_a_outer+1);
    end
    Cf_outer_2 = interp1(x_outer_2,Cf_outer_2,x_top,'linear','extrap');
    
    Cf_outer = interp1([o1 o2],[Cf_outer_1;Cf_outer_2],aviao.LiftingSurfaces(LF).Aero.Loads.alpha_eff(ii));  

    corda = (aviao.LiftingSurfaces(LF).Aero.Mesh.Verts.v4(ii,1)-aviao.LiftingSurfaces(LF).Aero.Mesh.Verts.v1(ii,1));
    x_corda(:,ii) = aviao.LiftingSurfaces(LF).Aero.Mesh.Verts.v1(ii,1) + x_top*corda;
    y_corda(:,ii) = aviao.LiftingSurfaces(LF).Aero.Mesh.Verts.v1(ii,2)*ones(n_divisoes,1);
    perc   = aviao.LiftingSurfaces(LF).Aero.Mesh.SpanPercent(ii);
    z_corda(:,ii) = Cf_inner.*(1-perc) + Cf_outer.*perc;
    
    %z_corda(end-40:end,:) = z_corda(end-41,ii);
    
    % plot3(x_corda, aviao.LiftingSurfaces(LF).Aero.Mesh.Verts.v2(ii,2)*ones(pos3,1),50*aviao.LiftingSurfaces(LF).Aero.Airfoils.Data(1).pol(pos1).Cf(1:pos3,pos2),'b','LineWidth',1.2);
    % surf(x_corda, aviao.LiftingSurfaces(LF).Aero.Mesh.Verts.v2(ii,2)*ones(pos3,1),100*aviao.LiftingSurfaces(LF).Aero.Airfoils.Data(1).pol(pos1).Cf(1:pos3,pos2),'r','LineWidth',1.2);
    % figure(2); hold on;
    % plot(x_top,aviao.LiftingSurfaces(LF).Aero.Airfoils.Data(1).pol(pos1).Cf(1:pos3,pos2));
end

%% Plotar Cp
    case 'cp'
aviao.LiftingSurfaces(LF).Aero.Loads.alpha_eff = aviao.LiftingSurfaces(LF).Aero.Loads.alpha-aviao.LiftingSurfaces(LF).Aero.Loads.alpha_ind;
x_top = linspace(1,0,n_divisoes);

for ii = 1:aviao.LiftingSurfaces(LF).Aero.Mesh.Ny_tot
    % Definindo o perfil de dentro e o perfil de fora da se��o
    Yinner_ind = aviao.LiftingSurfaces(LF).Aero.Mesh.SpanInner_ind(ii);
    Youter_ind = aviao.LiftingSurfaces(LF).Aero.Mesh.SpanOuter_ind(ii);
    
    % Definindo o airfoil do momento
	AirfoilData_inner	= aviao.LiftingSurfaces(LF).Aero.Airfoils.Data(Yinner_ind);
	AirfoilData_outer	= aviao.LiftingSurfaces(LF).Aero.Airfoils.Data(Youter_ind);
    
    % Encontrando as posi��es do Reynolds nos dois casos
    Re_pan = aviao.LiftingSurfaces(LF).Aero.Loads.Re_pan(ii);
    [~,pos_Re_inner] = min(abs([AirfoilData_inner.pol.Re]-Re_pan));
    [~,pos_Re_outer] = min(abs([AirfoilData_outer.pol.Re]-Re_pan));
    
    
    % Encontrando o intervalo de alpha do perfil interno
    pos_a_inner = find(AirfoilData_inner.pol(pos_Re_inner).alpha <= aviao.LiftingSurfaces(LF).Aero.Loads.alpha_eff(ii),1,'last');
    i1 = AirfoilData_inner.pol(pos_Re_inner).alpha(pos_a_inner);
    i2 = AirfoilData_inner.pol(pos_Re_inner).alpha(pos_a_inner+1);
    
    % Interpola��o do interno
    [~,pos_inner_1] = min(AirfoilData_inner.pol(pos_Re_inner).x2(:,pos_a_inner));
    switch vista
        case 'extra'
            x_inner_1 = AirfoilData_inner.pol(pos_Re_inner).x2(1:pos_inner_1,pos_a_inner);
            Cp_inner_1 = AirfoilData_inner.pol(pos_Re_inner).Cp(1:pos_inner_1,pos_a_inner);
        case 'intra'
            x_inner_1 = AirfoilData_inner.pol(pos_Re_inner).x2(pos_inner_1:end,pos_a_inner);
            Cp_inner_1 = AirfoilData_inner.pol(pos_Re_inner).Cp(pos_inner_1:end,pos_a_inner);
    end
    Cp_inner_1 = interp1(x_inner_1,Cp_inner_1,x_top,'linear','extrap');
    
    [~,pos_inner_2] = min(AirfoilData_inner.pol(pos_Re_inner).x2(:,pos_a_inner+1));
    switch vista
        case 'extra'
            x_inner_2 = AirfoilData_inner.pol(pos_Re_inner).x2(1:pos_inner_2,pos_a_inner+1);
            Cp_inner_2 = AirfoilData_inner.pol(pos_Re_inner).Cp(1:pos_inner_2,pos_a_inner+1);
        case 'intra'
            x_inner_2 = AirfoilData_inner.pol(pos_Re_inner).x2(pos_inner_2:end,pos_a_inner+1);
            Cp_inner_2 = AirfoilData_inner.pol(pos_Re_inner).Cp(pos_inner_2:end,pos_a_inner+1);
    end
    Cp_inner_2 = interp1(x_inner_2,Cp_inner_2,x_top,'linear','extrap');
    
    Cp_inner = interp1([i1 i2],[Cp_inner_1;Cp_inner_2],aviao.LiftingSurfaces(LF).Aero.Loads.alpha_eff(ii));  
    
    % Encontrando o intervalo de alpha do perfil externo
    pos_a_outer = find(AirfoilData_outer.pol(pos_Re_outer).alpha <= aviao.LiftingSurfaces(LF).Aero.Loads.alpha_eff(ii),1,'last');
    o1 = AirfoilData_outer.pol(pos_Re_outer).alpha(pos_a_outer);
    o2 = AirfoilData_outer.pol(pos_Re_outer).alpha(pos_a_outer+1);
    
    % Interpola��o do externo
    [~,pos_outer_1] = min(AirfoilData_outer.pol(pos_Re_outer).x2(:,pos_a_outer));
    switch vista
        case 'extra'
            x_outer_1 = AirfoilData_outer.pol(pos_Re_outer).x2(1:pos_outer_1,pos_a_outer);
            Cp_outer_1 = AirfoilData_outer.pol(pos_Re_outer).Cp(1:pos_outer_1,pos_a_outer);
        case 'intra'
            x_outer_1 = AirfoilData_outer.pol(pos_Re_outer).x2(pos_outer_1:end,pos_a_outer);
            Cp_outer_1 = AirfoilData_outer.pol(pos_Re_outer).Cp(pos_outer_1:end,pos_a_outer);
    end
    Cp_outer_1 = interp1(x_outer_1,Cp_outer_1,x_top,'linear','extrap');
    
    [~,pos_outer_2] = min(AirfoilData_outer.pol(pos_Re_outer).x2(:,pos_a_outer+1));
    switch vista
        case 'extra'
            x_outer_2 = AirfoilData_outer.pol(pos_Re_outer).x2(1:pos_outer_2,pos_a_outer+1);
            Cp_outer_2 = AirfoilData_outer.pol(pos_Re_outer).Cp(1:pos_outer_2,pos_a_outer+1);
        case 'intra'
            x_outer_2 = AirfoilData_outer.pol(pos_Re_outer).x2(pos_outer_2:end,pos_a_outer+1);
            Cp_outer_2 = AirfoilData_outer.pol(pos_Re_outer).Cp(pos_outer_2:end,pos_a_outer+1);
    end
    Cp_outer_2 = interp1(x_outer_2,Cp_outer_2,x_top,'linear','extrap');
    
    Cp_outer = interp1([o1 o2],[Cp_outer_1;Cp_outer_2],aviao.LiftingSurfaces(LF).Aero.Loads.alpha_eff(ii));  

    corda = (aviao.LiftingSurfaces(LF).Aero.Mesh.Verts.v4(ii,1)-aviao.LiftingSurfaces(LF).Aero.Mesh.Verts.v1(ii,1));
    x_corda(:,ii) = aviao.LiftingSurfaces(LF).Aero.Mesh.Verts.v1(ii,1) + x_top*corda;
    y_corda(:,ii) = aviao.LiftingSurfaces(LF).Aero.Mesh.Verts.v1(ii,2)*ones(n_divisoes,1);
    perc   = aviao.LiftingSurfaces(LF).Aero.Mesh.SpanPercent(ii);
    z_corda(:,ii) = Cp_inner.*(1-perc) + Cp_outer.*perc;
    
    %z_corda(end-40:end,:) = z_corda(end-41,ii);
    
    % plot3(x_corda, aviao.LiftingSurfaces(LF).Aero.Mesh.Verts.v2(ii,2)*ones(pos3,1),50*aviao.LiftingSurfaces(LF).Aero.Airfoils.Data(1).pol(pos1).Cf(1:pos3,pos2),'b','LineWidth',1.2);
    % surf(x_corda, aviao.LiftingSurfaces(LF).Aero.Mesh.Verts.v2(ii,2)*ones(pos3,1),100*aviao.LiftingSurfaces(LF).Aero.Airfoils.Data(1).pol(pos1).Cf(1:pos3,pos2),'r','LineWidth',1.2);
    % figure(2); hold on;
    % plot(x_top,aviao.LiftingSurfaces(LF).Aero.Airfoils.Data(1).pol(pos1).Cf(1:pos3,pos2));
end

end
%% Contornos
j = 1;
intervalo_1 = 0.0;
intervalo_2 = 1.0; 
intervalo = floor((1-intervalo_2)*n_divisoes)+1:floor((1-intervalo_1)*n_divisoes);
s = surf(x_corda(intervalo,:),y_corda(intervalo,:),z_corda(intervalo,:));
s.EdgeColor = 'none';
view([90,90])
axis equal
colormap(hsv); colorbar('Location','south');
f.Position = [403 246 672 420];

for ii = 1:size(z_corda,2)
   for jj = floor(n_divisoes/2):-1:1
        x(ii) = x_corda(jj,ii);
        y(ii) = y_corda(jj,ii);
        z(ii) = z_corda(jj,ii);
        
        if z_corda(jj,ii)<0.0005             
            break
        end
    end
end
% plot3(x,y,z,'LineWidth',1);

for i=1:length(aviao.LiftingSurfaces(LF).Geo.Yb)-1
    x1 = aviao.LiftingSurfaces(LF).Geo.XYZ.LE(i  ,1);
    x2 = aviao.LiftingSurfaces(LF).Geo.XYZ.LE(i+1,1);
    x3 = aviao.LiftingSurfaces(LF).Geo.XYZ.TE(i+1,1);
    x4 = aviao.LiftingSurfaces(LF).Geo.XYZ.TE(i  ,1);

    y1 = aviao.LiftingSurfaces(LF).Geo.XYZ.LE(i  ,2);
    y2 = aviao.LiftingSurfaces(LF).Geo.XYZ.LE(i+1,2);
    y3 = aviao.LiftingSurfaces(LF).Geo.XYZ.TE(i+1,2);
    y4 = aviao.LiftingSurfaces(LF).Geo.XYZ.TE(i  ,2);

    z1 = aviao.LiftingSurfaces(LF).Geo.XYZ.LE(i,  3);
    z2 = aviao.LiftingSurfaces(LF).Geo.XYZ.LE(i+1,3);
    z3 = aviao.LiftingSurfaces(LF).Geo.XYZ.TE(i+1,3);
    z4 = aviao.LiftingSurfaces(LF).Geo.XYZ.TE(i,  3);
    
    plot3([x1 x2 x3 x4 x1],[y1 y2 y3 y4 y1],[z1 z2 z3 z4 z1]-0.9,'k','LineWidth',1.0)
    plot3([x1 x2 x3 x4 x1],-[y1 y2 y3 y4 y1],[z1 z2 z3 z4 z1]-0.9,'k','LineWidth',1.0)
end                
end
axis([min(min(x_corda(intervalo,:)))-0.1 max(max(x_corda(intervalo,:)))+0.1 -(y2+0.1) y2+0.1 min(min(z_corda(intervalo,:)))-0.2 max(max(z_corda(intervalo,:)))+0.2]);
axis off


