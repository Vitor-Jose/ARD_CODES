%% Plota as distribui��es nas asa
% Vitor Jos� - nov. 2021

%% LIMPEZA
clear; clc; close all;
SetPaths

%% PAR�METROS INICIAS
load('03-MACACO_Aeronaves/plane_2022_aileron.mat');    % Carrega a aeronave
aviao = obj;        % Realoca para avi�o

aviao.Aero.Settings.NLVLM.Cl_from_adjusted_2DCurvesEq = 0; 
% p2 = ImportObj('ETW_02_9_5.mat');
% p1 = ImportObj('BA7_top10_WTU_01.mat');
% p1 = ImportObj('E95_35_plus10.mat');
p2 = ImportObj('BT825_15_minus10.mat');
p1 = ImportObj('E95_35_plus10.mat');
aviao.LiftingSurfaces(1).Aero.Airfoils.Data(4) = p2;
aviao.LiftingSurfaces(1).Aero.Airfoils.Data(3) = p1;

tipo = 'cp';        % cf ou cp
vista = 'extra';    % extra ou intra

n_divisoes = 100;   % N�mero de divis�es na corda
intervalo_1 = 0.4;  % In�cio da representa��o da corda (0.0 - Bordo de ataque)
intervalo_2 = 1.0;  % Fim da representa��o da corda    (1.0 - Bordo de fuga)

superficies = [1];

% plot_surf       = 1;    % 0 - N�o plota superf�cie
%                         % 1 - Plota superf�cie

                        
plot_contour    = 1;    % 0 - N�o plota contorno
                        % 1 - Plota contorno
aux_contour = [0.0015 0.0015];                
AoA =  [20 22 23];

conta_fig = 1;
for conta_AoA = 1:length(AoA)
    %% MODIFICAR GEOMETRIA
    % aviao.UpdateGeo;
    % aviao.UpdateAeroMesh;

    %% CONDI��ES DE VOO
    condicao = FlightConditions;
    condicao.alpha = AoA(conta_AoA);
    condicao.Voo = 12;
    condicao.rho = 1.1161;


    %% SOLVER
    NL_VLM(aviao, condicao,'-dispstat','-itermax',30);
    CLplane = aviao.Aero.Coeffs.CL;
    CDplane = aviao.Aero.Coeffs.CD;

    
    for conta_LF = 1:length(superficies)
        f = figure(conta_fig); % Define figura 
        hold on; axis equal;   % Caracter�sticas da figura
        LF = superficies(conta_LF);

    switch tipo
    %% Plotar Cf
        case 'cf'
    aviao.LiftingSurfaces(LF).Aero.Loads.alpha_eff = aviao.LiftingSurfaces(LF).Aero.Loads.alpha-aviao.LiftingSurfaces(LF).Aero.Loads.alpha_ind;
    x_top = linspace(1,0,n_divisoes);

    for ii = 1:aviao.LiftingSurfaces(LF).Aero.Mesh.Ny_tot
        % Definindo o perfil de dentro e o perfil de fora da se��o
        Yinner_ind = aviao.LiftingSurfaces(LF).Aero.Mesh.SpanInner_ind(ii);
        Youter_ind = aviao.LiftingSurfaces(LF).Aero.Mesh.SpanOuter_ind(ii);

        % Definindo o airfoil do momento
        AirfoilData_inner	= aviao.LiftingSurfaces(LF).Aero.Airfoils.Data(Yinner_ind);
        AirfoilData_outer	= aviao.LiftingSurfaces(LF).Aero.Airfoils.Data(Youter_ind);

        % Encontrando as posi��es do Reynolds nos dois casos
        Re_pan = aviao.LiftingSurfaces(LF).Aero.Loads.Re_pan(ii);
        [~,pos_Re_inner] = min(abs([AirfoilData_inner.pol.Re]-Re_pan));
        [~,pos_Re_outer] = min(abs([AirfoilData_outer.pol.Re]-Re_pan));


        % Encontrando o intervalo de alpha do perfil interno
        pos_a_inner = find(AirfoilData_inner.pol(pos_Re_inner).alpha <= aviao.LiftingSurfaces(LF).Aero.Loads.alpha_eff(ii),1,'last');
        if isempty(pos_a_inner)
            pos_a_inner = 1;
        end
        i1 = AirfoilData_inner.pol(pos_Re_inner).alpha(pos_a_inner);
        i2 = AirfoilData_inner.pol(pos_Re_inner).alpha(pos_a_inner+1);

        % Interpola��o do interno
        [~,pos_inner_1] = min(AirfoilData_inner.pol(pos_Re_inner).x(:,pos_a_inner));
        switch vista
            case 'extra'
                x_inner_1 = AirfoilData_inner.pol(pos_Re_inner).x(1:pos_inner_1,pos_a_inner);
                Cf_inner_1 = AirfoilData_inner.pol(pos_Re_inner).Cf(1:pos_inner_1,pos_a_inner);
            case 'intra'
                x_inner_1 = AirfoilData_inner.pol(pos_Re_inner).x(pos_inner_1:end,pos_a_inner);
                Cf_inner_1 = AirfoilData_inner.pol(pos_Re_inner).Cf(pos_inner_1:end,pos_a_inner);
        end
        Cf_inner_1 = interp1(x_inner_1,Cf_inner_1,x_top,'linear','extrap');

        [~,pos_inner_2] = min(AirfoilData_inner.pol(pos_Re_inner).x(:,pos_a_inner+1));
        switch vista
            case 'extra'
                x_inner_2 = AirfoilData_inner.pol(pos_Re_inner).x(1:pos_inner_2,pos_a_inner+1);
                Cf_inner_2 = AirfoilData_inner.pol(pos_Re_inner).Cf(1:pos_inner_2,pos_a_inner+1);
            case 'intra'
                x_inner_2 = AirfoilData_inner.pol(pos_Re_inner).x(pos_inner_2:end,pos_a_inner+1);
                Cf_inner_2 = AirfoilData_inner.pol(pos_Re_inner).Cf(pos_inner_2:end,pos_a_inner+1);
        end
        Cf_inner_2 = interp1(x_inner_2,Cf_inner_2,x_top,'linear','extrap');

        Cf_inner = interp1([i1 i2],[Cf_inner_1;Cf_inner_2],aviao.LiftingSurfaces(LF).Aero.Loads.alpha_eff(ii));  

        % Encontrando o intervalo de alpha do perfil externo
        pos_a_outer = find(AirfoilData_outer.pol(pos_Re_outer).alpha <= aviao.LiftingSurfaces(LF).Aero.Loads.alpha_eff(ii),1,'last');
        if isempty(pos_a_outer)
            pos_a_outer = 1;
        end       
        o1 = AirfoilData_outer.pol(pos_Re_outer).alpha(pos_a_outer);
        o2 = AirfoilData_outer.pol(pos_Re_outer).alpha(pos_a_outer+1);

        % Interpola��o do externo
        [~,pos_outer_1] = min(AirfoilData_outer.pol(pos_Re_outer).x(:,pos_a_outer));
        switch vista
            case 'extra'
                x_outer_1 = AirfoilData_outer.pol(pos_Re_outer).x(1:pos_outer_1,pos_a_outer);
                Cf_outer_1 = AirfoilData_outer.pol(pos_Re_outer).Cf(1:pos_outer_1,pos_a_outer);
            case 'intra'
                x_outer_1 = AirfoilData_outer.pol(pos_Re_outer).x(pos_outer_1:end,pos_a_outer);
                Cf_outer_1 = AirfoilData_outer.pol(pos_Re_outer).Cf(pos_outer_1:end,pos_a_outer);
        end
        Cf_outer_1 = interp1(x_outer_1,Cf_outer_1,x_top,'linear','extrap');

        [~,pos_outer_2] = min(AirfoilData_outer.pol(pos_Re_outer).x(:,pos_a_outer+1));
        switch vista
            case 'extra'
                x_outer_2 = AirfoilData_outer.pol(pos_Re_outer).x(1:pos_outer_2,pos_a_outer+1);
                Cf_outer_2 = AirfoilData_outer.pol(pos_Re_outer).Cf(1:pos_outer_2,pos_a_outer+1);
            case 'intra'
                x_outer_2 = AirfoilData_outer.pol(pos_Re_outer).x(pos_outer_2:end,pos_a_outer+1);
                Cf_outer_2 = AirfoilData_outer.pol(pos_Re_outer).Cf(pos_outer_2:end,pos_a_outer+1);
        end
        Cf_outer_2 = interp1(x_outer_2,Cf_outer_2,x_top,'linear','extrap');

        Cf_outer = interp1([o1 o2],[Cf_outer_1;Cf_outer_2],aviao.LiftingSurfaces(LF).Aero.Loads.alpha_eff(ii));  

        corda = (aviao.LiftingSurfaces(LF).Aero.Mesh.Verts.v4(ii,1)-aviao.LiftingSurfaces(LF).Aero.Mesh.Verts.v1(ii,1));
        x_corda(:,ii) = aviao.LiftingSurfaces(LF).Aero.Mesh.Verts.v1(ii,1) + x_top*corda;
        y_corda(:,ii) = aviao.LiftingSurfaces(LF).Aero.Mesh.Verts.v1(ii,2)*ones(n_divisoes,1);
        perc   = aviao.LiftingSurfaces(LF).Aero.Mesh.SpanPercent(ii);
        z_corda(:,ii) = Cf_inner.*(1-perc) + Cf_outer.*perc;
    end

    %% Plotar Cp
        case 'cp'
    aviao.LiftingSurfaces(LF).Aero.Loads.alpha_eff = aviao.LiftingSurfaces(LF).Aero.Loads.alpha-aviao.LiftingSurfaces(LF).Aero.Loads.alpha_ind;
    x_top = linspace(1,0,n_divisoes);

    for ii = 1:aviao.LiftingSurfaces(LF).Aero.Mesh.Ny_tot
        % Definindo o perfil de dentro e o perfil de fora da se��o
        Yinner_ind = aviao.LiftingSurfaces(LF).Aero.Mesh.SpanInner_ind(ii);
        Youter_ind = aviao.LiftingSurfaces(LF).Aero.Mesh.SpanOuter_ind(ii);

        % Definindo o airfoil do momento
        AirfoilData_inner	= aviao.LiftingSurfaces(LF).Aero.Airfoils.Data(Yinner_ind);
        AirfoilData_outer	= aviao.LiftingSurfaces(LF).Aero.Airfoils.Data(Youter_ind);

        % Encontrando as posi��es do Reynolds nos dois casos
        Re_pan = aviao.LiftingSurfaces(LF).Aero.Loads.Re_pan(ii);
        [~,pos_Re_inner] = min(abs([AirfoilData_inner.pol.Re]-Re_pan));
        [~,pos_Re_outer] = min(abs([AirfoilData_outer.pol.Re]-Re_pan));


        % Encontrando o intervalo de alpha do perfil interno
        pos_a_inner = find(AirfoilData_inner.pol(pos_Re_inner).alpha <= aviao.LiftingSurfaces(LF).Aero.Loads.alpha_eff(ii),1,'last');
        if isempty(pos_a_inner)
            pos_a_inner = 1;
        end
        i1 = AirfoilData_inner.pol(pos_Re_inner).alpha(pos_a_inner);
        i2 = AirfoilData_inner.pol(pos_Re_inner).alpha(pos_a_inner+1);

        % Interpola��o do interno
        [~,pos_inner_1] = min(AirfoilData_inner.pol(pos_Re_inner).x2(:,pos_a_inner));
        switch vista
            case 'extra'
                x_inner_1 = AirfoilData_inner.pol(pos_Re_inner).x2(1:pos_inner_1,pos_a_inner);
                Cp_inner_1 = AirfoilData_inner.pol(pos_Re_inner).Cp(1:pos_inner_1,pos_a_inner);
            case 'intra'
                x_inner_1 = AirfoilData_inner.pol(pos_Re_inner).x2(pos_inner_1:end,pos_a_inner);
                Cp_inner_1 = AirfoilData_inner.pol(pos_Re_inner).Cp(pos_inner_1:end,pos_a_inner);
        end
        Cp_inner_1 = interp1(x_inner_1,Cp_inner_1,x_top,'linear','extrap');

        [~,pos_inner_2] = min(AirfoilData_inner.pol(pos_Re_inner).x2(:,pos_a_inner+1));
        switch vista
            case 'extra'
                x_inner_2 = AirfoilData_inner.pol(pos_Re_inner).x2(1:pos_inner_2,pos_a_inner+1);
                Cp_inner_2 = AirfoilData_inner.pol(pos_Re_inner).Cp(1:pos_inner_2,pos_a_inner+1);
            case 'intra'
                x_inner_2 = AirfoilData_inner.pol(pos_Re_inner).x2(pos_inner_2:end,pos_a_inner+1);
                Cp_inner_2 = AirfoilData_inner.pol(pos_Re_inner).Cp(pos_inner_2:end,pos_a_inner+1);
        end
        Cp_inner_2 = interp1(x_inner_2,Cp_inner_2,x_top,'linear','extrap');

        Cp_inner = interp1([i1 i2],[Cp_inner_1;Cp_inner_2],aviao.LiftingSurfaces(LF).Aero.Loads.alpha_eff(ii));  

        % Encontrando o intervalo de alpha do perfil externo
        pos_a_outer = find(AirfoilData_outer.pol(pos_Re_outer).alpha <= aviao.LiftingSurfaces(LF).Aero.Loads.alpha_eff(ii),1,'last');
        if isempty(pos_a_outer)
            pos_a_outer = 1;
        end   
        o1 = AirfoilData_outer.pol(pos_Re_outer).alpha(pos_a_outer);
        o2 = AirfoilData_outer.pol(pos_Re_outer).alpha(pos_a_outer+1);

        % Interpola��o do externo
        [~,pos_outer_1] = min(AirfoilData_outer.pol(pos_Re_outer).x2(:,pos_a_outer));
        switch vista
            case 'extra'
                x_outer_1 = AirfoilData_outer.pol(pos_Re_outer).x2(1:pos_outer_1,pos_a_outer);
                Cp_outer_1 = AirfoilData_outer.pol(pos_Re_outer).Cp(1:pos_outer_1,pos_a_outer);
            case 'intra'
                x_outer_1 = AirfoilData_outer.pol(pos_Re_outer).x2(pos_outer_1:end,pos_a_outer);
                Cp_outer_1 = AirfoilData_outer.pol(pos_Re_outer).Cp(pos_outer_1:end,pos_a_outer);
        end
        Cp_outer_1 = interp1(x_outer_1,Cp_outer_1,x_top,'linear','extrap');

        [~,pos_outer_2] = min(AirfoilData_outer.pol(pos_Re_outer).x2(:,pos_a_outer+1));
        switch vista
            case 'extra'
                x_outer_2 = AirfoilData_outer.pol(pos_Re_outer).x2(1:pos_outer_2,pos_a_outer+1);
                Cp_outer_2 = AirfoilData_outer.pol(pos_Re_outer).Cp(1:pos_outer_2,pos_a_outer+1);
            case 'intra'
                x_outer_2 = AirfoilData_outer.pol(pos_Re_outer).x2(pos_outer_2:end,pos_a_outer+1);
                Cp_outer_2 = AirfoilData_outer.pol(pos_Re_outer).Cp(pos_outer_2:end,pos_a_outer+1);
        end
        Cp_outer_2 = interp1(x_outer_2,Cp_outer_2,x_top,'linear','extrap');

        Cp_outer = interp1([o1 o2],[Cp_outer_1;Cp_outer_2],aviao.LiftingSurfaces(LF).Aero.Loads.alpha_eff(ii));  

        corda = (aviao.LiftingSurfaces(LF).Aero.Mesh.Verts.v4(ii,1)-aviao.LiftingSurfaces(LF).Aero.Mesh.Verts.v1(ii,1));
        x_corda(:,ii) = aviao.LiftingSurfaces(LF).Aero.Mesh.Verts.v1(ii,1) + x_top*corda;
        y_corda(:,ii) = aviao.LiftingSurfaces(LF).Aero.Mesh.Verts.v1(ii,2)*ones(n_divisoes,1);
        perc   = aviao.LiftingSurfaces(LF).Aero.Mesh.SpanPercent(ii);
        z_corda(:,ii) = Cp_inner.*(1-perc) + Cp_outer.*perc;
    end

    end
    %% Contornos
    
    intervalo = floor((1-intervalo_2)*n_divisoes)+1:floor((1-intervalo_1)*n_divisoes);
    s = surf(x_corda(intervalo,:),y_corda(intervalo,:),z_corda(intervalo,:));
    s.EdgeColor = 'none';
    view([90,90])
    axis equal
    colormap(jet(50)); colorbar('Location','south');
    f.Position = [403 246 672 420];

    for i=1:length(aviao.LiftingSurfaces(LF).Geo.Yb)-1
        x1 = aviao.LiftingSurfaces(LF).Geo.XYZ.LE(i  ,1);
        x2 = aviao.LiftingSurfaces(LF).Geo.XYZ.LE(i+1,1);
        x3 = aviao.LiftingSurfaces(LF).Geo.XYZ.TE(i+1,1);
        x4 = aviao.LiftingSurfaces(LF).Geo.XYZ.TE(i  ,1);

        y1 = aviao.LiftingSurfaces(LF).Geo.XYZ.LE(i  ,2);
        y2 = aviao.LiftingSurfaces(LF).Geo.XYZ.LE(i+1,2);
        y3 = aviao.LiftingSurfaces(LF).Geo.XYZ.TE(i+1,2);
        y4 = aviao.LiftingSurfaces(LF).Geo.XYZ.TE(i  ,2);

        z1 = aviao.LiftingSurfaces(LF).Geo.XYZ.LE(i,  3);
        z2 = aviao.LiftingSurfaces(LF).Geo.XYZ.LE(i+1,3);
        z3 = aviao.LiftingSurfaces(LF).Geo.XYZ.TE(i+1,3);
        z4 = aviao.LiftingSurfaces(LF).Geo.XYZ.TE(i,  3);
        
        altura = -aviao.LiftingSurfaces(LF).Geo.pos.z+0.5;
        plot3([x1 x2 x3 x4 x1],[y1 y2 y3 y4 y1],[z1 z2 z3 z4 z1]+altura,'k','LineWidth',1.0)
        plot3([x1 x2 x3 x4 x1],-[y1 y2 y3 y4 y1],[z1 z2 z3 z4 z1]+altura,'k','LineWidth',1.0)
    end
    axis([min(min(x_corda(intervalo,:)))-0.25 max(max(x_corda(intervalo,:)))+0.1 -(y2+0.1) y2+0.1 min(min(z_corda(intervalo,:)))-1 max(max(z_corda(intervalo,:)))+1]);
    axis off
    
    titulo = ['Asa ' num2str(superficies(conta_LF)) ' - AoA = ' num2str(AoA(conta_AoA)) '� - C_L = ' num2str(aviao.LiftingSurfaces(LF).Aero.Coeffs.CL)];
    title(titulo)
 
    if plot_contour
 %       figure(conta_fig+1);
        figure(10);
        hold on; axis equal;   % Caracter�sticas da figura
        
        for i=1:length(aviao.LiftingSurfaces(LF).Geo.Yb)-1
        x1 = aviao.LiftingSurfaces(LF).Geo.XYZ.LE(i  ,1);
        x2 = aviao.LiftingSurfaces(LF).Geo.XYZ.LE(i+1,1);
        x3 = aviao.LiftingSurfaces(LF).Geo.XYZ.TE(i+1,1);
        x4 = aviao.LiftingSurfaces(LF).Geo.XYZ.TE(i  ,1);

        y1 = aviao.LiftingSurfaces(LF).Geo.XYZ.LE(i  ,2);
        y2 = aviao.LiftingSurfaces(LF).Geo.XYZ.LE(i+1,2);
        y3 = aviao.LiftingSurfaces(LF).Geo.XYZ.TE(i+1,2);
        y4 = aviao.LiftingSurfaces(LF).Geo.XYZ.TE(i  ,2);

        z1 = aviao.LiftingSurfaces(LF).Geo.XYZ.LE(i,  3);
        z2 = aviao.LiftingSurfaces(LF).Geo.XYZ.LE(i+1,3);
        z3 = aviao.LiftingSurfaces(LF).Geo.XYZ.TE(i+1,3);
        z4 = aviao.LiftingSurfaces(LF).Geo.XYZ.TE(i,  3);
        
        altura = -aviao.LiftingSurfaces(LF).Geo.pos.z-0.5;
        plot3([x1 x2 x3 x4 x1],[y1 y2 y3 y4 y1],[z1 z2 z3 z4 z1]+altura,'k','LineWidth',1.0)
        plot3([x1 x2 x3 x4 x1],-[y1 y2 y3 y4 y1],[z1 z2 z3 z4 z1]+altura,'k','LineWidth',1.0)
        end
        
        contour(x_corda(intervalo,1:end),y_corda(intervalo,1:end),z_corda(intervalo,1:end),aux_contour,'LineWidth',2);
        view([90,90])
        f.Position = [403 246 672 420];
        conta_fig = conta_fig+2;
    else
        conta_fig = conta_fig+1;
    end

    end
end


% Plotar paineis
% for ii = 1:length(aviao.LiftingSurfaces(LF).Aero.Mesh.Verts.v1)
%     x = [aviao.LiftingSurfaces(LF).Aero.Mesh.Verts.v1(ii,1) aviao.LiftingSurfaces(LF).Aero.Mesh.Verts.v2(ii,1) aviao.LiftingSurfaces(LF).Aero.Mesh.Verts.v3(ii,1) aviao.LiftingSurfaces(LF).Aero.Mesh.Verts.v4(ii,1) aviao.LiftingSurfaces(LF).Aero.Mesh.Verts.v1(ii,1)];
%     y = [aviao.LiftingSurfaces(LF).Aero.Mesh.Verts.v1(ii,2) aviao.LiftingSurfaces(LF).Aero.Mesh.Verts.v2(ii,2) aviao.LiftingSurfaces(LF).Aero.Mesh.Verts.v3(ii,2) aviao.LiftingSurfaces(LF).Aero.Mesh.Verts.v4(ii,2) aviao.LiftingSurfaces(LF).Aero.Mesh.Verts.v1(ii,2)];
%     plot(x,y,'k','LineWidth',1.2); axis equal;
%     axis([-0.25*aviao.LiftingSurfaces(LF).Geo.C(1)-0.20 0.75*aviao.LiftingSurfaces(LF).Geo.C(1)+0.05 -aviao.LiftingSurfaces(LF).Geo.b2-0.05 aviao.LiftingSurfaces(LF).Geo.b2+0.05])
%     view([90,90])
% end