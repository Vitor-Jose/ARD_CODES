clear,clc,close all
% Codigo para gerar os vetores de velocidade para diferentes angulos de ataque em cfd
%% =========INPUT=========

Voo = 12; %Velocidade desejada para a an�lise
alpha = [10]; %Angulo de ataque desejado para a an�lise em graus (�)

%% ========C�LCULOS=======
Vx = Voo*cosd(alpha);
Vz = Voo*sind(alpha);

Lx = -sind(alpha);
Lz = cosd(alpha);

Dx = cosd(alpha);
Dz = sind(alpha);
%% ========Display========

fprintf('Ux: %f\nUz: %f\nDire��o da sustenta��o em x: %f\nDire��o da sustenta��o em z: %f\nDire��o do arrasto em x: %f\nDire��o do arrasto em z: %f\n',Vx,Vz,Lx,Lz,Dx,Dz)
