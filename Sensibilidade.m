clear; close all; clc

vet = [-0.015 -0.01 0 0.01 0.015];
aux_r = 0;
for r = vet
    aux_r = aux_r+1;
    aux_p =0;
for p = vet
% close all
cd 03-MACACO_Aeronaves
load('plane_2022.mat') 
cd ..
aviao = obj; 

% aviao.LiftingSurfaces.IsMain = 1;
% aviao.LiftingSurfaces.label = 'Asa Principal';
% aviao.LiftingSurfaces.Geo.Yb = geo.LiftingSurface(1).b(1,:);
aviao.LiftingSurfaces(1).Geo.C = aviao.LiftingSurfaces(1).Geo.C + [r r p];
% aviao.LiftingSurfaces.Geo.C = geo.LiftingSurface.c(1,:);
perfil(1) = ImportObj('BT.mat');
perfil(2) = ImportObj('E95.mat');
% % perfil(3) = ImportObj('MHL90.mat');
% % perfil(4) = ImportObj('mh84.mat');
aviao.LiftingSurfaces(1).Aero.Airfoils.Data = [perfil(2) perfil(2) perfil(1)];

% aviao.LiftingSurfaces(2) = [];
% aviao.LiftingSurfaces(2) = LiftingSurface('EH');
% aviao.LiftingSurfaces(2).IsMain = 0;
% aviao.LiftingSurfaces(2).Geo.Yb = geo.LiftingSurface.b(2,:);
% aviao.LiftingSurfaces(2).Geo.C = geo.LiftingSurface.c(2,:);
% aviao.LiftingSurfaces(2).Geo.Incidence = -c;
% aviao.LiftingSurfaces(2).Geo.pos.z = -h;
% aviao.LiftingSurfaces(2).Geo.pos.x = geo.LiftingSurface.pos(2,1);
% aviao.LiftingSurfaces(2).Geo.pos.y = geo.LiftingSurface.pos(2,2);
% aviao.LiftingSurfaces(2).Geo.pos.z = geo.LiftingSurface.pos(2,3);
% perfil(5) = ImportObj('prof2010.mat');
% aviao.LiftingSurfaces(2).Aero.Airfoils.Data = [perfil(5) perfil(5)];

aviao.UpdateGeo;
% aviao.PlotGeo('-airfoils');
% pause(0.001)
% aviao.PlotWake('-airfoils');

%% Malha

aviao.LiftingSurfaces(1).Aero.Mesh.Ny = [18 16];
aviao.LiftingSurfaces(1).Aero.Mesh.TypeY = [1 3]; % uniforme na se��o mais interior.
                                                    % cosseno nas se��es intermedi�rias para prever os efeitos da jun��o de se��es com perfis diferentes
                                                    % e direita na se��o mais externas em busca de dados mais detalhados por conta dos v�rtices de ponta de asa

% aviao.LiftingSurfaces(2).Aero.Mesh.Ny = [10 8];
% aviao.LiftingSurfaces(2).Aero.Mesh.TypeY = [1 3];

aviao.UpdateAeroMesh;
% aviao.PlotMesh;

%% Condi��es de Voo

condicao = FlightConditions;
condicao.Voo = 12;
condicao.rho = 1.116;
aoa = 15;

%% Solver
    aux_p = aux_p+1;
    condicao.alpha = aoa;
    NL_VLM(aviao, condicao,'-append','-dispstat','-itermax',30);

    CLplane(aux_p,aux_r) = aviao.Aero.Coeffs.CL;
    CDplane(aux_p,aux_r) = aviao.Aero.Coeffs.CD;

    CDsurf1(aux_p,aux_r) = aviao.LiftingSurfaces(1).Aero.Coeffs.CD;
    CLsurf1(aux_p,aux_r) = aviao.LiftingSurfaces(1).Aero.Coeffs.CL;
    Cmsurf1(aux_p,aux_r) = aviao.LiftingSurfaces(1).Aero.Coeffs.Cm25;
    
    Sref(aux_p,aux_r) = aviao.Aero.Coeffs.Sref;
    
%     CDsurf2(aux_p,aux_r) = aviao.LiftingSurfaces(2).Aero.Coeffs.CD;
%     CLsurf2(aux_p,aux_r) = aviao.LiftingSurfaces(2).Aero.Coeffs.CL;
     
end 
end

subplot(1,2,1)
L = CLsurf1.*Sref.*0.5*144*1.116;
contourf(vet*1000,vet*1000,L); colorbar;
ylabel('Varia��o - Ponta [mm]')
xlabel('Varia��o - Raiz [mm]')
title('Sustenta��o na subida [N]')

subplot(1,2,2)
D = CDsurf1.*Sref.*0.5*144*1.116;
contourf(vet*1000,vet*1000,D); colorbar;
ylabel('Varia��o - Ponta [mm]')
xlabel('Varia��o - Raiz [mm]')
title('Arrasto na subida [N]')

 



