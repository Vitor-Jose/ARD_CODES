%% Programa para analisar diferentes n�meros e configura��es de se��es
clear; close all; clc;

% 3 Se��es 
% Vari�veis: 
% c3    - Corda da ponta
% l2    - Corda relativa da segunda se��o
% x3    - Envergadura da terceira se��o/Envergadura total
% x2    - (Envergadura da segunda se��o)/Envergadura total

perfises = {'ETW_02b.mat'};
i = 1;

aoa = 15;
envergadura = 2.0;
S = 0.95;
c_min = 0.15;

lim_l2 = 1:-0.25:0;
lim_c3 = 0.475:-0.1:0.175;
lim_x3 = 0.1:0.15:0.4;
lim_x2 = 0.1:0.2:0.5;

CL = zeros(length(lim_c3),length(lim_l2),length(lim_x2),length(lim_x3),length(aoa));
CD = zeros(length(lim_c3),length(lim_l2),length(lim_x2),length(lim_x3),length(aoa));
CDi = zeros(length(lim_c3),length(lim_l2),length(lim_x2),length(lim_x3),length(aoa));
CM = zeros(length(lim_c3),length(lim_l2),length(lim_x2),length(lim_x3),length(aoa));
penal = zeros(length(lim_c3),length(lim_l2),length(lim_x2),length(lim_x3),length(aoa));

for x3 = lim_x3
    pos.x3 = find(x3 == lim_x3);
    for x2 = lim_x2
        pos.x2 = find(x2 == lim_x2);
        for c3 = lim_c3
            pos.c3 = find(c3 == lim_c3);
            for l2 = lim_l2
                pos.l2 = find(l2 == lim_l2);
                %% C�lculos a partir da parametriza��o
                x1 = 1 - (x2+x3);
                
                c_ret = S/envergadura;
                
                c_max =(c_ret-0.5*c3*(x2+x3))/(x1+0.5*(x2+x3));
                l2min =(c_max-c3)/((x2+x3)*envergadura*0.5);
                
                l2real = (1-l2)*l2min;
                acompanha_c2 = 0.5*envergadura*x2*l2real;
                
                b1 =  x1*envergadura/2;
                b2 = (x2*envergadura*0.5) + b1;
                b3 = (x3*envergadura*0.5) + b2;
                
                c1 = (c_ret+acompanha_c2*0.5*(x2+x3)-0.5*x3*c3)/(x1+x2+0.5*x3);
                c2 = c1-acompanha_c2;
                
                S1 = c1*x1*envergadura;
                S2 = 0.5*(c1+c2)*x2*envergadura;
                S3 = 0.5*(c2+c3)*x3*envergadura;
                Sw = S1+S2+S3;
                
                if c3 >= c_min
                %% Definindo a asa
                penal(pos.c3,pos.l2,pos.x2,pos.x3) = 0;
                aviao = Airplane;

                aviao.LiftingSurfaces.IsMain = 1;
                aviao.LiftingSurfaces.label = 'Asa principal';
                aviao.LiftingSurfaces.Geo.Yb = [0 b1 b2 b3];
                aviao.LiftingSurfaces.Geo.C = [c1 c1 c2 c3];
                aviao.LiftingSurfaces.Geo.Dihedral = [0 0 0];
                aviao.LiftingSurfaces.Geo.Sweep = [0 0 0];
                aviao.LiftingSurfaces.Geo.TwistY = [0 0 0 0];
                aviao.LiftingSurfaces.Geo.Incidence = 0;
                aviao.LiftingSurfaces(1).Geo.pos.x = 0;
                aviao.LiftingSurfaces(1).Geo.pos.y = 0;
                aviao.LiftingSurfaces(1).Geo.pos.z = 0;
                perfil(1) = ImportObj(perfises{i});
                aviao.LiftingSurfaces(1).Aero.Airfoils.Data = [perfil(1) perfil(1) perfil(1) perfil(1)];

                 aviao.UpdateGeo;
%                 f = figure(pos.l2);
%                 f.Position = [300 150 600 300];
%                 title(['Tamanho relativo da segunda corda: 0,0'])
%                 aviao.PlotGeo('-nimage',pos.l2);
%                 view([90 -90])
%                 axis off

                
                %% - Malha
                % aviao.LiftingSurfaces(1).Aero.Mesh.Ny = [2+floor(x1*envergadura*14) 2+floor(x2*envergadura*20) 2+floor(x3*envergadura*25)];
                aviao.LiftingSurfaces(1).Aero.Mesh.Ny = [2+floor((x1^1.1)*25) 2+floor((x2^0.73)*33) 2+floor((x3^0.63)*32)];
                aviao.LiftingSurfaces(1).Aero.Mesh.TypeY = [1 2 3];
%                 disp(aviao.LiftingSurfaces(1).Aero.Mesh.Ny)
                
                aviao.UpdateAeroMesh;
                %aviao.PlotMesh;

                %% - Condi��es de voo
                condicao = FlightConditions;
                condicao.Voo = 15;
                condicao.rho = 1.225;
                
                %% - Solver
                for j = 1:length(aoa)
                    condicao.alpha = aoa(j);
                    aviao.Aero.Settings.NLVLM.Cl_from_adjusted_2DCurvesEq = 1;
                    NL_VLM(aviao, condicao,'-append','-itermax',30,'-dispstat');

                    CL(pos.c3,pos.l2,pos.x2,pos.x3,j) = aviao.Aero.Coeffs(j).CL;
                    CD(pos.c3,pos.l2,pos.x2,pos.x3,j) = aviao.Aero.Coeffs(j).CD;  
                    CDi(pos.c3,pos.l2,pos.x2,pos.x3,j) = aviao.Aero.Coeffs(j).CDi;
                    CM(pos.c3,pos.l2,pos.x2,pos.x3,j) = aviao.Aero.Coeffs(j).Cm25; 
                end
                clc
                disp(CL(:,:,pos.x2,pos.x3,1))
                else
                    penal(pos.c3,pos.l2,pos.x2,pos.x3) = 1;
                end
            end
        end
    end
end
save('Resultados_perfil_ETW.mat','CL','CD','CM')

%% Salvando para excel
for camelo = 1:length(perfises)
tam = size(CL); %1-afilamento terceira, 2-afilamento segunda, 3-tamanho segunda, 4-tamanho terceira
for k = 1:length(aoa)
    for i = 1:tam(3)
        for j = 1:tam(4)
            M1(((i-1)*tam(1))+1:i*tam(1),((j-1)*tam(2))+1:j*tam(2)) = CL(:,:,i,j,k);
            M2(((i-1)*tam(1))+1:i*tam(1),((j-1)*tam(2))+1:j*tam(2)) = CD(:,:,i,j,k);
            M4(((i-1)*tam(1))+1:i*tam(1),((j-1)*tam(2))+1:j*tam(2)) = CM(:,:,i,j,k);
            M3(((i-1)*tam(1))+1:i*tam(1),((j-1)*tam(2))+1:j*tam(2)) = CDi(:,:,i,j,k);
        end
    end
    M = M1./M2;
    Me = (M1.^2)./(pi.*((envergadura^2)/S).*M3);
    cd Tabelas
    nome1 = [erase(perfises{camelo},'.mat') ' - CL' num2str(aoa(k))  '.xls'];
    nome2 = [erase(perfises{camelo},'.mat') ' - CD' num2str(aoa(k)) '.xls'];
    nome3 = [erase(perfises{camelo},'.mat') ' - r' num2str(aoa(k)) '.xls'];
    nome4 = [erase(perfises{camelo},'.mat') ' - CM' num2str(aoa(k)) '.xls'];
    writetable(table(M1),nome1,'Sheet',1,'Range','C3')
    writetable(table(M2),nome1,'Sheet',2,'Range','C3')
    writetable(table(M3),nome1,'Sheet',3,'Range','C3')
    writetable(table(M4),nome1,'Sheet',4,'Range','C3')
    writetable(table(M),nome1,'Sheet',5,'Range','C3')
    writetable(table(Me),nome1,'Sheet',6,'Range','C3')

    cd ..
end
end
                         %terceira se��o pequena    %terceira se��o grande 
%segunda se��o pequena           --------                --------

%segunda se��o grande            --------                --------