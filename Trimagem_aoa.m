function Trimagem_aoa(geo,flc)
%% ==================== Parametros de entrada =============================
eff = 0.5;                                                                  % Efetividade do profundor
aoamax = 17;                                                                % Maximo angulo de ataque que o codigo buscara CLtrim
aoamin = -5;
deflexaomax = -15;                                                          % Deflexao maxima do profundor
ID_prof = 3;                                                                % Superficie de controle
V = 10;
%% ======================= Prelocando variaveis ===========================
CL = zeros(geo.surfacenum,1);
Cm = zeros(geo.surfacenum,1);
%% ========================== Chama VLM ===================================
flc.Voo = V;                                                                % Declarando a velocidade para o VLM
flc.case(1).aoa = (aoamin:aoamax);                                             % Range de angulos que serao calculados para as asas
flc.case(2).aoa = (10:-1:deflexaomax);                                      % Range de angulos que serao calculados para a empenagem
coef = coeffs(geo,flc,0,[1 2 3],1);                                       % Chamando VLM
for j=1:geo.surfacenum
    coeff.fit(j).CL = fit(coef(j).aoa',coef(j).CL','linearinterp');    % Nesse loop e feito o fit dos coeficientes (apenas para simplificar o acesso aos coeficientes)
    coeff.fit(j).CD = fit(coef(j).aoa',coef(j).CD','linearinterp');
    coeff.fit(j).Cm25 = fit(coef(j).aoa',coef(j).Cm25','linearinterp');
end
%% =================== CL por deflexao do prof ============================
flc.case(2).aoa = [0 -5];
coeffsEH = coeffs(geo,flc,[0;3],3,3);
CL_deltae = (coeffsEH(3).CL(1) - coeffsEH(3).CL(2))/(coeffsEH(3).aoa(1) - coeffsEH(3).aoa(2))*eff*coeffsEH(3).Sref/coef(1).Sref;
i = 0;
for aoa=aoamin:0.1:aoamax
    i = i + 1;
    xo = -2;
    [deflexao,errotrim] = fminsearch(@trimagem,xo);
    trim.aoa(i) = aoa;
    trim.deflexao(i) = deflexao;
    trim.erro(i) = errotrim;
end
%% ===================== Plot dos resultados ==============================
figure
plot(trim.aoa,trim.deflexao)
grid on
xlabel('Aoa [graus]')
ylabel('Deflexao do profundor [graus]')
title('Trimagem por aoa')
figure
plot(trim.aoa,trim.erro)
grid on
xlabel('Aoa [graus]')
ylabel('Erro do codigo [ ]')
title('Erro do codigo')
%% =================== Funcao de otimizacao trimagem ======================
function Errotrim = trimagem(x)                                            % Funcao para otimizar
deflexao = x;                                                              % A funcao recebe o vetor x a deflexao
for j=1:geo.surfacenum
    M = geo.s.neta(j)*[0 -geo.s.pos(j,3) geo.s.pos(j,2); geo.s.pos(j,3) 0 -geo.s.pos(j,1); -geo.s.pos(j,2) geo.s.pos(j,1) 0]*([cosd(aoa) 0 -sind(aoa);0 1 0;sind(aoa) 0 cosd(aoa)]*[-coeff.fit(j).CD(aoa-geo.s.deda(j)*aoa-geo.s.eo(j));0;-coeff.fit(j).CL(aoa-geo.s.deda(j)*aoa-geo.s.eo(j))]);
    Cm(j) = M(2)/coef(1).Cref + geo.s.neta(j)*coeff.fit(j).Cm25(aoa - geo.s.deda(j)*aoa - geo.s.eo(j));
end
Cm_aeronave = sum(Cm);
Mprof = geo.s.neta(ID_prof)*[0 -geo.s.pos(ID_prof,3) geo.s.pos(ID_prof,2); geo.s.pos(ID_prof,3) 0 -geo.s.pos(ID_prof,1); -geo.s.pos(ID_prof,2) geo.s.pos(ID_prof,1) 0]*([cosd(aoa) 0 -sind(aoa);0 1 0;sind(aoa) 0 cosd(aoa)]*[0;0;-CL_deltae*deflexao]);
Cm_prof = Mprof(2)/coef(1).Cref;
Errotrim = abs(Cm_aeronave + Cm_prof)*100;                                 % Erro de Cm (soma do Cm da aeronave + Cm do profundor)
end
end