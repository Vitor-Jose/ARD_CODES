% Calculadora de inflation layer para o cfd
% Baseado em https://www.fluidmechanics101.com/pages/tools.html
% Autor: José Passos

%% LIMPA
clc;clear;close all;
%% INPUTS

% V : velocidade [m/s]
% C : comprimento característico, usualmente CMA [m]
% mi : viscosidade [Pa s] 1.81e-5
% rho : densidade [kg/m^3] 1.225
% yplus : y+ desejado (usualmente menor que 1)
% N : número de camadas

V = 25;
C = 1;
mi = 1.81e-5;
rho = 1.225;
yplus = 1;
N = 20;

%% CALCULOS

% Altura de primeira camada (yh) ~ first layer thickness
Re = (rho*V*C)/mi;
cf = (2*log10(Re) - .65)^(-2.3);  % Coeficiente de fricção. correlação empírica para escoamento completamente desenvolvido sobre uma placa plana. valido para Re < 10e9
tauw = rho*V^2*cf*.5;           % Calculo da tensão de cisalhamento (Wall shear stress)
ut = sqrt(tauw/rho);            % velocidade de fricção
yp = (yplus*mi)/(ut*rho);       % altura do centroide do primeiro elemento, rearranjo da equação do y+
yh = 2*yp;                      % altura da primeira camada

fprintf('A altura estimada da primeira camada e %.10f m \n',yh)

% O resultado deste código é apenas uma estimativa, portanto, se o y+ for
% maior do que o desejado, deve-se reduzir o valor de de yh de um fator
% desejado para alcançar o y+
% Exemplo:
% Você utilizou este código querendo um y+ de 1, e quando rodou o cfd seu
% y+ foi de 3, você deve dividir o seu valor de yh por 3 e refazer sua
% simulação.

% Razão de cresimento (growth) ~ growth ratio
if Re < 5e5
    deltagg = (4.91*C)/sqrt(Re); % (cengel)
else
    deltagg = (.38*C)/(Re^(1/5));% (cengel)
end

% r^N - r*(deltagg/yh) + ((deltagg/yh)-1) = 0

% Newton-raphson
r0 = 5;
i = 1;
f = @(r) r^N - r*(deltagg/yh) + ((deltagg/yh)-1);
dfdr = @(r) N*r^(N-1) - (deltagg/yh);
erro = 1000;
while erro > 1e-10
    fi = f(r0);
    flinhai = dfdr(r0);
    r = r0 - fi/flinhai;
    erro = abs(r-r0)/abs(r);
    r0 = r;
end

fprintf('A growth ratio e %f \n',r)

% Final layer thickness (yfinal)
yfinal = yh*r^(N-1);

fprintf('A altura estimada da ultima camada e %.10f m \n',yfinal)
