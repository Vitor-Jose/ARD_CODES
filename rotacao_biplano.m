function[dt] = rotacao_biplano (geo,flc)
std.m=17;
deflexao = -10;
Iyy = 1.5;
t=0;
geo.arf = 15;
%%%% inic�o - vari�veis
theta = 0;  %�ngulo entre referenciais
dt = 0.001; % infinitesimal de tempo
thetaum = 0; %Vel. angular
a= 0.1;

for j = 1:geo.surfacenum
    CA(:,j) = [geo.s.pos(j,1) - geo.tp.pos(1);0;geo.s.pos(j,3) - geo.tp.pos(3)];
end 

Xcg = -geo.tp.pos(1);
Zcg = -geo.tp.pos(3);
Zmotor = geo.motor.pos(3) - geo.tp.pos(3);
i=1;
T = [empuxo(flc.Voo,1,flc.rho); 0 ;0];

flc.case(1).aoa = 0:15;
flc.case(2).aoa = -12:10;
coef = coeffs(geo,flc,0,[1 2; 3 0],1);
    for j=1:geo.surfacenum
        coeff.fit(j).CL = fit(coef(j).aoa',coef(j).CL','linearinterp');
        coeff.fit(j).CD = fit(coef(j).aoa',coef(j).CD','linearinterp');
        coeff.fit(j).Cm25 = fit(coef(j).aoa',coef(j).Cm25','linearinterp');
    end
    
    CG = [Xcg;0;Zcg];
    ENG = [0;0;Zmotor];
    W = [0;0;std.m*flc.g];
    Finercia = [-a*std.m;0;0];
    CAt = CA(:,3);

while theta < geo.arf
    E1 = [cosd(theta) 0 -sind(theta); 0 1 0 ; sind(theta) 0 cosd(theta)];
    for j = 1:geo.surfacenum
        alpha = theta - geo.s.deda(j)*theta - geo.s.eo(j);
        Lw(:,j) = [0;0;-geo.s.neta(j)*0.5*flc.rho*(flc.Voo^2)*coef(1).Sref*coeff.fit(j).CL(alpha)];
        Dw(:,j) = [-geo.s.neta(j)*0.5*flc.rho*(flc.Voo^2)*coef(1).Sref*coeff.fit(j).CD(alpha);0;0];
        Mw(:,j) = [0; 0.5*geo.s.neta(j)*flc.rho*(flc.Voo^2)*coef(1).Sref*coeff.fit(j).Cm25(alpha)...
        *coef(1).Cref;0];
    
       M(:,j) = cross(CA(:,j),(E1*(Lw(:,j)+Dw(:,j))));
    end
    
    Lt = [0;0;-0.5*geo.s.neta(3)*flc.rho*(flc.Voo^2)*coef(1).Sref*coeff.fit(3).CL(deflexao)];
    Dt = [-0.5*geo.s.neta(3)*flc.rho*(flc.Voo^2)*coef(1).Sref*coeff.fit(3).CD(deflexao);0;0];
    Mt = [0; 0.5*flc.rho*(flc.Voo^2)*coef(1).Sref*coeff.fit(3).Cm25(deflexao)...
        *coef(1).Cref*geo.s.neta(3);0];
    Mmotor = cross(ENG,T);
    Mcg = cross(CG,(E1*(Finercia+W)));
    Mprof = cross(CAt,(E1*(Lt+Dt)))+Mt;

    Mtotal = sum(M(2,:))+Mmotor(2)+Mcg(2)+sum(Mw(2,:))+Mprof(2);
    Mtotal_plot(i) = Mtotal; 
    if  Mtotal < 0 && theta == 0
        Mtotal = 0;
    end
    
    
    theta=theta*pi/180;
    thetadois=Mtotal/Iyy;

    thetaum = thetaum + thetadois*dt;
    theta=theta + thetaum*dt + thetadois*dt^2/2;
    
    t = t + dt;
    theta = theta*180/pi;
    theta_plot(i) = theta;
    tempo_plot(i) = t;
    Mcg = cross(CG,(E1*W))+cross(CG,(E1*Finercia));
    Mcg_plot(i) = Mcg(2);
    M_prof_plot(i) = Mprof(2);
    M_motor_plot(i) = Mmotor(2);
    
for j = 1:geo.surfacenum
    M_plot(i,j) = M(2,j);
end
    i = i+1;
    
    if t > 2
        theta = geo.arf + 2;
        disp('Aeronave n�o rotaciona.');
    end 
end
plot(tempo_plot,M_plot(:,1),tempo_plot,M_plot(:,2),tempo_plot,Mtotal_plot,tempo_plot,Mcg_plot,tempo_plot,M_prof_plot,tempo_plot,M_motor_plot);
legend('W1','W2','Momento total','CG','Profundor','Motor')
xlabel('Tempo (s)')
ylabel('Momento (N*m)')
grid on
figure
plot(tempo_plot, theta_plot)
xlabel ('Tempo (s)')
ylabel ('AoA (graus)')
grid on

end